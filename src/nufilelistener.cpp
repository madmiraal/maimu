// (c) 2018: Marcel Admiraal

#include "nufilelistener.h"

namespace nu
{
    void FileListener::dataReceived(ma::Data* data)
    {
        switch (data->getDataType())
        {
        case DataType::NU_CUSTOM_START:
            onSignalDataReceived(*data);
            break;
        default:
            Listener::dataReceived(data);
            break;
        }
    }
}
