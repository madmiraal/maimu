// (c) 2016 - 2018: Marcel Admiraal

#include "nuconfig.h"

#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/button.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>

namespace nu
{
    enum
    {
        ID_ID,
        ID_FREQUENCY,
        ID_SKIP,
        ID_ADCFILTER1,
        ID_ADCFILTER2,
        ID_ADCFILTER3,
        ID_ADCFILTER4,
        ID_ADCFILTER5,
        ID_ADCFILTER6,
        ID_ADCFILTER7,
        ID_ADCFILTER8,
        ID_ACCELEROMETER_SCALE,
        ID_ACCELEROMETER_RATE,
        ID_GYROSCOPE_SCALE,
        ID_GYROSCOPE_RATE,
        ID_MAGNETOMETER_SCALE,
        ID_MAGNETOMETER_RATE,
        ID_ACCELEROMETER_X_OFFSET,
        ID_ACCELEROMETER_Y_OFFSET,
        ID_ACCELEROMETER_Z_OFFSET,
        ID_GYROSCOPE_X_OFFSET,
        ID_GYROSCOPE_Y_OFFSET,
        ID_GYROSCOPE_Z_OFFSET,
        ID_MAGNETOMETER_X_OFFSET,
        ID_MAGNETOMETER_Y_OFFSET,
        ID_MAGNETOMETER_Z_OFFSET
    };

    Config::Config(wxWindow* parent, RegisterData* registerData) :
        wxDialog(parent, wxID_ANY, wxT("Configure nu Registers")),
        registerData(registerData)
    {
        // Create Dialog
        wxBoxSizer* configSizer = new wxBoxSizer(wxVERTICAL);
        wxBoxSizer* registerSizer = new wxBoxSizer(wxHORIZONTAL);

        // Sample Data
        wxBoxSizer* sampleBoxSizer = new wxBoxSizer(wxVERTICAL);
        sampleBoxSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Sample Settings: ")), 0, wxALIGN_LEFT | wxALL, 5);
        wxGridSizer* sampleSizer = new wxGridSizer(2);
        // Firmware Version
        sampleSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Version: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        firmwareVersionText = new wxStaticText(this, wxID_ANY, wxEmptyString);
        sampleSizer->Add(firmwareVersionText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Id
        sampleSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Id: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        idText = new wxTextCtrl(this, ID_ID);
        idText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        sampleSizer->Add(idText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Sample Frequency
        sampleSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Sample Frequency (Hz): ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        sampleFrequencyText = new wxTextCtrl(this, ID_FREQUENCY);
        sampleFrequencyText->Bind(wxEVT_TEXT, &Config::frequencyChanged, this);
        sampleSizer->Add(sampleFrequencyText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Period
        sampleSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Period: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        periodText = new wxStaticText(this, wxID_ANY, wxEmptyString);
        sampleSizer->Add(periodText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Pre-Scaler
        sampleSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Pre-Scaler: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        prescalerText = new wxStaticText(this, wxID_ANY, wxEmptyString);
        sampleSizer->Add(prescalerText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Skip
        sampleSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("IMU / Quaternion send skip: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        skipText = new wxTextCtrl(this, ID_SKIP);
        skipText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        sampleSizer->Add(skipText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // ADC Channel Bit Filter
        sampleSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("ADC Bit Filter: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        adcFilterText = new wxStaticText(this, wxID_ANY, wxEmptyString);
        sampleSizer->Add(adcFilterText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        sampleBoxSizer->Add(sampleSizer, 0, wxEXPAND);
        // Bits
        wxBoxSizer* bitSelectionSizer = new wxBoxSizer(wxHORIZONTAL);
        adcCheckboxArray = new wxCheckBox*[8];
        for (unsigned int i = 0; i < 8; ++i)
        {
            wxString label; label << i+1;
            adcCheckboxArray[i] = new wxCheckBox(this, wxID_ANY, label);
            adcCheckboxArray[i]->Bind(
                    wxEVT_CHECKBOX, &Config::adcCheckboxChanged, this);
            bitSelectionSizer->Add(adcCheckboxArray[i], 1, wxEXPAND);
        }
        sampleBoxSizer->Add(bitSelectionSizer, 0, wxEXPAND);
        registerSizer->Add(sampleBoxSizer, 1, wxEXPAND);

        // IMU Data Settings
        wxBoxSizer* scaleRateBoxSizer = new wxBoxSizer(wxVERTICAL);
        scaleRateBoxSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("IMU Scales and Rates: ")), 0, wxALIGN_LEFT | wxALL, 5);
        wxGridSizer* scaleRateSizer = new wxGridSizer(2);
        // Accelerometer Scale
        scaleRateSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Accelerometer Scale: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        wxString accelerometerScaleChoices[] = {
                wxT("±2 g"),
                wxT("±4 g"),
                wxT("±8 g"),
                wxT("±16 g")};
        accelerometerScaleCombo = new wxComboBox(
                this, ID_ACCELEROMETER_SCALE, accelerometerScaleChoices[0],
                wxDefaultPosition, wxDefaultSize, 4, accelerometerScaleChoices);
        accelerometerScaleCombo->Bind(wxEVT_COMBOBOX, &Config::comboChanged,
                this);
        scaleRateSizer->Add(accelerometerScaleCombo, 0, wxEXPAND | wxRIGHT, 5);
        // Accelerometer Rate
        scaleRateSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Accelerometer Rate: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        wxString accelerometerRateChoices[] = {
                wxT("Power-down"),
                wxT("3.125 Hz"),
                wxT("6.25 Hz"),
                wxT("12.5 Hz"),
                wxT("25 Hz"),
                wxT("50 Hz"),
                wxT("100 Hz"),
                wxT("200 Hz"),
                wxT("400 Hz"),
                wxT("800 Hz"),
                wxT("1600 Hz")};
        accelerometerRateCombo = new wxComboBox(
                this, ID_ACCELEROMETER_RATE, accelerometerRateChoices[0],
                wxDefaultPosition, wxDefaultSize, 11, accelerometerRateChoices);
        accelerometerRateCombo->Bind(wxEVT_COMBOBOX, &Config::comboChanged,
                this);
        scaleRateSizer->Add(accelerometerRateCombo, 0, wxEXPAND | wxRIGHT, 5);
        // Gyroscope Scale
        scaleRateSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Gyroscope Scale: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        wxString gyroscopeScaleChoices[] = {
                wxT("±245 dps"),
                wxT("±500 dps"),
                wxT("±2000 dps")};
        gyroscopeScaleCombo = new wxComboBox(
                this, ID_GYROSCOPE_SCALE, gyroscopeScaleChoices[0],
                wxDefaultPosition, wxDefaultSize, 3, gyroscopeScaleChoices);
        gyroscopeScaleCombo->Bind(wxEVT_COMBOBOX, &Config::comboChanged,
                this);
        scaleRateSizer->Add(gyroscopeScaleCombo, 0, wxEXPAND | wxRIGHT, 5);
        // Gyroscope Rate
        scaleRateSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Gyroscope Rate: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        wxString gyroscopeRateChoices[] = {
                wxT("95 Hz (12.5)"),
                wxT("95 Hz (25)"),
                wxT("95 Hz (25)"),
                wxT("95 Hz (25)"),
                wxT("190 Hz (12.5)"),
                wxT("190 Hz (25)"),
                wxT("190 Hz (50)"),
                wxT("190 Hz (70)"),
                wxT("380 Hz (20)"),
                wxT("380 Hz (25)"),
                wxT("380 Hz (50)"),
                wxT("380 Hz (100)"),
                wxT("760 Hz (30)"),
                wxT("760 Hz (35)"),
                wxT("760 Hz (50)"),
                wxT("760 Hz (100)")};
        gyroscopeRateCombo = new wxComboBox(
                this, ID_GYROSCOPE_RATE, gyroscopeRateChoices[0],
                wxDefaultPosition, wxDefaultSize, 16, gyroscopeRateChoices);
        gyroscopeRateCombo->Bind(wxEVT_COMBOBOX, &Config::comboChanged,
                this);
        scaleRateSizer->Add(gyroscopeRateCombo, 0, wxEXPAND | wxRIGHT, 5);
        // Magnetometer Scale
        scaleRateSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Magnetometer Scale: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        wxString magnetometerScaleChoices[] = {
                wxT("±2 guass"),
                wxT("±4 guass"),
                wxT("±8 guass"),
                wxT("±12 guass")};
        magnetometerScaleCombo = new wxComboBox(
                this, ID_MAGNETOMETER_SCALE, magnetometerScaleChoices[0],
                wxDefaultPosition, wxDefaultSize, 4, magnetometerScaleChoices);
        magnetometerScaleCombo->Bind(wxEVT_COMBOBOX, &Config::comboChanged,
                this);
        scaleRateSizer->Add(magnetometerScaleCombo, 0, wxEXPAND | wxRIGHT, 5);
        // Magnetometer Rate
        scaleRateSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Magnetometer Rate: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        wxString magnetometerRateChoices[] = {
                wxT("3.125 Hz"),
                wxT("6.25 Hz"),
                wxT("12.5 Hz"),
                wxT("25 Hz"),
                wxT("50 Hz"),
                wxT("100 Hz")};
        magnetometerRateCombo = new wxComboBox(
                this, ID_MAGNETOMETER_RATE, magnetometerRateChoices[0],
                wxDefaultPosition, wxDefaultSize, 6, magnetometerRateChoices);
        magnetometerRateCombo->Bind(wxEVT_COMBOBOX, &Config::comboChanged,
                this);
        scaleRateSizer->Add(magnetometerRateCombo, 0, wxEXPAND | wxRIGHT, 5);
        scaleRateBoxSizer->Add(scaleRateSizer, 0, wxEXPAND);
        registerSizer->Add(scaleRateBoxSizer, 1, wxEXPAND);

        // Sample Data
        wxBoxSizer* offsetBoxSizer = new wxBoxSizer(wxVERTICAL);
        offsetBoxSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Offsets: ")), 0, wxALIGN_LEFT | wxALL, 5);
        wxGridSizer* offsetSizer = new wxGridSizer(2);
        // Accelerometer X Offset
        offsetSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Accelerometer X Offset: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        accelerometerXOffsetText = new wxTextCtrl(this, ID_ACCELEROMETER_X_OFFSET);
        accelerometerXOffsetText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        offsetSizer->Add(accelerometerXOffsetText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Accelerometer Y Offset
        offsetSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Accelerometer Y Offset: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        accelerometerYOffsetText = new wxTextCtrl(this, ID_ACCELEROMETER_Y_OFFSET);
        accelerometerYOffsetText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        offsetSizer->Add(accelerometerYOffsetText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Accelerometer Z Offset
        offsetSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Accelerometer Z Offset: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        accelerometerZOffsetText = new wxTextCtrl(this, ID_ACCELEROMETER_Z_OFFSET);
        accelerometerZOffsetText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        offsetSizer->Add(accelerometerZOffsetText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Gyroscope X Offset
        offsetSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Gyroscope X Offset: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        gyroscopeXOffsetText = new wxTextCtrl(this, ID_GYROSCOPE_X_OFFSET);
        gyroscopeXOffsetText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        offsetSizer->Add(gyroscopeXOffsetText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Gyroscope Y Offset
        offsetSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Gyroscope Y Offset: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        gyroscopeYOffsetText = new wxTextCtrl(this, ID_GYROSCOPE_Y_OFFSET);
        gyroscopeYOffsetText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        offsetSizer->Add(gyroscopeYOffsetText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Gyroscope Z Offset
        offsetSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Gyroscope Z Offset: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        gyroscopeZOffsetText = new wxTextCtrl(this, ID_GYROSCOPE_Z_OFFSET);
        gyroscopeZOffsetText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        offsetSizer->Add(gyroscopeZOffsetText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Magnetometer X Offset
        offsetSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Magnetometer X Offset: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        magnetometerXOffsetText = new wxTextCtrl(this, ID_MAGNETOMETER_X_OFFSET);
        magnetometerXOffsetText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        offsetSizer->Add(magnetometerXOffsetText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Magnetometer Y Offset
        offsetSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Magnetometer Y Offset: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        magnetometerYOffsetText = new wxTextCtrl(this, ID_MAGNETOMETER_Y_OFFSET);
        magnetometerYOffsetText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        offsetSizer->Add(magnetometerYOffsetText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        // Magnetometer Z Offset
        offsetSizer->Add(new wxStaticText(this, wxID_ANY,
                wxT("Magnetometer Z Offset: ")), 0, wxALIGN_LEFT | wxLEFT, 5);
        magnetometerZOffsetText = new wxTextCtrl(this, ID_MAGNETOMETER_Z_OFFSET);
        magnetometerZOffsetText->Bind(wxEVT_TEXT, &Config::textEntered, this);
        offsetSizer->Add(magnetometerZOffsetText, 0, wxALIGN_LEFT | wxRIGHT, 5);
        offsetBoxSizer->Add(offsetSizer, 0, wxEXPAND);
        registerSizer->Add(offsetBoxSizer, 1, wxEXPAND);

        configSizer->Add(registerSizer, 1, wxEXPAND);

        // Buttons.
        wxBoxSizer* buttonSizer = new wxBoxSizer(wxHORIZONTAL);
        wxButton* okButton = new wxButton(this, wxID_OK, wxT("OK"));
        wxButton* saveFlashButton = new wxButton(this, wxID_ANY, wxT("Save to Flash"));
        wxButton* loadButton = new wxButton(this, wxID_ANY, wxT("Load from File"));
        wxButton* saveButton = new wxButton(this, wxID_ANY, wxT("Save to File"));
        wxButton* cancelButton = new wxButton(this, wxID_CANCEL, wxT("Cancel"));
        saveFlashButton->Bind(wxEVT_BUTTON, &Config::saveRegistersToFlash, this);
        loadButton->Bind(wxEVT_BUTTON, &Config::loadRegisterSettings, this);
        saveButton->Bind(wxEVT_BUTTON, &Config::saveRegisterSettings, this);
        buttonSizer->AddStretchSpacer(1);
        buttonSizer->Add(okButton, 0, wxALL, 5);
        buttonSizer->Add(saveFlashButton, 0, wxALL, 5);
        buttonSizer->Add(loadButton, 0, wxALL, 5);
        buttonSizer->Add(saveButton, 0, wxALL, 5);
        buttonSizer->Add(cancelButton, 0, wxALL, 5);
        buttonSizer->AddStretchSpacer(1);
        configSizer->Add(buttonSizer, 0, wxEXPAND);

        populateValues();
        SetSizerAndFit(configSizer);
    }

    Config::~Config()
    {
        delete[] adcCheckboxArray;
    }

    void Config::populateValues()
    {
        wxString firmwareString;
        firmwareString << registerData->getFirmwareVersion();
        firmwareVersionText->SetLabel(firmwareString);

        wxString idString;
        idString << registerData->getID();
        idText->SetValue(idString);

        wxString sampleFrequencyString;
        sampleFrequencyString << registerData->getSampleFrequency();
		sampleFrequencyText->SetValue(sampleFrequencyString);

        wxString periodString;
        periodString << registerData->getPeriod();
		periodText->SetLabel(periodString);

		prescalerText->SetLabel(
                getPreScalerString(registerData->getPreScaler()));

        wxString skipString;
        skipString << registerData->getSkip();
		skipText->SetValue(skipString);

        wxString adcChannelFilterString;
        short adcFilterValue = registerData->getADCChannelFilter();
        adcChannelFilterString << adcFilterValue;
        adcFilterText->SetLabel(adcChannelFilterString);
        populateByteCheckboxes(adcCheckboxArray, adcFilterValue);

        accelerometerScaleCombo->SetSelection(
                registerData->getAccelerometerScale());
        accelerometerRateCombo->SetSelection(
                registerData->getAccelerometerRate());
        gyroscopeScaleCombo->SetSelection(
                registerData->getGyroscopeScale());
        gyroscopeRateCombo->SetSelection(
                registerData->getGyroscopeRate());
        magnetometerScaleCombo->SetSelection(
                registerData->getMagnetometerScale());
        magnetometerRateCombo->SetSelection(
                registerData->getMagnetometerRate());

        wxString accelerometerXOffsetString;
        accelerometerXOffsetString << registerData->getAccelerometerXOffset();
        accelerometerXOffsetText->SetValue(accelerometerXOffsetString);

        wxString accelerometerYOffsetString;
        accelerometerYOffsetString << registerData->getAccelerometerYOffset();
        accelerometerYOffsetText->SetValue(accelerometerYOffsetString);

        wxString accelerometerZOffsetString;
        accelerometerZOffsetString << registerData->getAccelerometerZOffset();
        accelerometerZOffsetText->SetValue(accelerometerZOffsetString);

        wxString gyroscopeXOffsetString;
        gyroscopeXOffsetString << registerData->getGyroscopeXOffset();
        gyroscopeXOffsetText->SetValue(gyroscopeXOffsetString);

        wxString gyroscopeYOffsetString;
        gyroscopeYOffsetString << registerData->getGyroscopeYOffset();
        gyroscopeYOffsetText->SetValue(gyroscopeYOffsetString);

        wxString gyroscopeZOffsetString;
        gyroscopeZOffsetString << registerData->getGyroscopeZOffset();
        gyroscopeZOffsetText->SetValue(gyroscopeZOffsetString);

        wxString magnetometerXOffsetString;
        magnetometerXOffsetString << registerData->getMagnetometerXOffset();
        magnetometerXOffsetText->SetValue(magnetometerXOffsetString);

        wxString magnetometerYOffsetString;
        magnetometerYOffsetString << registerData->getMagnetometerYOffset();
        magnetometerYOffsetText->SetValue(magnetometerYOffsetString);

        wxString magnetometerZOffsetString;
        magnetometerZOffsetString << registerData->getMagnetometerZOffset();
        magnetometerZOffsetText->SetValue(magnetometerZOffsetString);
    }

    void Config::textEntered(wxCommandEvent& event)
    {
        wxString text = event.GetString();

        // Ignore if blank.
        if (text.length() == 0) return;

        // Check for negative sign.
        int id = event.GetId();
        if (text == "-")
        {
            if (id == ID_ID || id == ID_SKIP)
            {
                wxMessageDialog message(this, wxT("Must be positive"));
                message.ShowModal();
            }
            return;
        }

        long value = 0;
        if (!text.ToLong(&value))
        {
            wxMessageDialog message(this, wxT("Must be an integer"));
            message.ShowModal();
            return;
        }
        switch (event.GetId())
        {
        case ID_ID:
            registerData->setID(value);
            break;
        case ID_SKIP:
            registerData->setSkip(value);
            break;
        case ID_ACCELEROMETER_X_OFFSET:
            registerData->setAccelerometerXOffset(value);
            break;
        case ID_ACCELEROMETER_Y_OFFSET:
            registerData->setAccelerometerYOffset(value);
            break;
        case ID_ACCELEROMETER_Z_OFFSET:
            registerData->setAccelerometerZOffset(value);
            break;
        case ID_GYROSCOPE_X_OFFSET:
            registerData->setGyroscopeXOffset(value);
            break;
        case ID_GYROSCOPE_Y_OFFSET:
            registerData->setGyroscopeYOffset(value);
            break;
        case ID_GYROSCOPE_Z_OFFSET:
            registerData->setGyroscopeZOffset(value);
            break;
        case ID_MAGNETOMETER_X_OFFSET:
            registerData->setMagnetometerXOffset(value);
            break;
        case ID_MAGNETOMETER_Y_OFFSET:
            registerData->setMagnetometerYOffset(value);
            break;
        case ID_MAGNETOMETER_Z_OFFSET:
            registerData->setMagnetometerZOffset(value);
            break;
        }
    }

    void Config::frequencyChanged(wxCommandEvent& event)
    {
        wxString text = event.GetString();
        unsigned long value = 0;
        if (text.length() > 0 && !text.ToULong(&value))
        {
            wxMessageDialog message(this, wxT("Must be a positive integer"));
            message.ShowModal();
            return;
        }
        if (value > 100000)
        {
            wxMessageDialog message(this, wxT("Frequency too high."));
            message.ShowModal();
            return;
        }
        if (value == 0) return;
        registerData->setSampleFrequency((int) value);
        wxString periodString;
        periodString << registerData->getPeriod();
        periodText->SetLabel(periodString);
        prescalerText->SetLabel(
                getPreScalerString(registerData->getPreScaler()));
    }

    void Config::adcCheckboxChanged(wxCommandEvent& event)
    {
        short adcFilterValue = getByteValue(adcCheckboxArray);
        wxString adcChannelFilterString;
        adcChannelFilterString << adcFilterValue;
        adcFilterText->SetLabel(adcChannelFilterString);
        registerData->setADCChannelFilter(adcFilterValue);
    }

    void Config::comboChanged(wxCommandEvent& event)
    {
        int value = event.GetSelection();
        switch (event.GetId())
        {
        case ID_ACCELEROMETER_SCALE:
            registerData->setAccelerometerScale(value); break;
        case ID_ACCELEROMETER_RATE:
            registerData->setAccelerometerRate(value);
            if (value <= 5 && value != 0 &&
                registerData->getMagnetometerRate() == 5)
            {
                wxMessageDialog message(this, wxT(
                    "Magnetometer rate of 100 Hz requires accelerometer rate > 50Hz"));
                message.ShowModal();
            }
            break;
        case ID_GYROSCOPE_SCALE:
            registerData->setGyroscopeScale(value); break;
        case ID_GYROSCOPE_RATE:
            registerData->setGyroscopeRate(value); break;
        case ID_MAGNETOMETER_SCALE:
            registerData->setMagnetometerScale(value); break;
        case ID_MAGNETOMETER_RATE:
            registerData->setMagnetometerRate(value);
            if (value == 5 && (
                registerData->getAccelerometerRate() <= 5 &&
                registerData->getAccelerometerRate() != 0))
            {
                wxMessageDialog message(this, wxT(
                    "Requires accelerometer rate > 50Hz or in power-down mode"));
                message.ShowModal();
            }
            break;
        }
    }

    void Config::saveRegistersToFlash(wxCommandEvent& event)
    {
        EndModal(wxID_APPLY);
    }

    void Config::loadRegisterSettings(wxCommandEvent& event)
    {
        bool success = false;
        while (!success)
        {
            wxFileDialog openFileDialog(this, wxT("Load Registers"),
                    wxEmptyString, wxT("registers.dat"),
                    wxFileSelectorDefaultWildcardStr,
                    wxFD_OPEN|wxFD_FILE_MUST_EXIST);
            if (openFileDialog.ShowModal() == wxID_CANCEL) return;
            success = registerData->loadData(
                    ma::Str(openFileDialog.GetPath().c_str()));
            if (success)
            {
                populateValues();
            }
            else
            {
                wxMessageBox(wxT("Load Failed"));
            }
        }
    }

    void Config::saveRegisterSettings(wxCommandEvent& event)
    {
        bool success = false;
        while (!success)
        {
            wxFileDialog saveFileDialog(this, wxT("Save Registers"),
                    wxEmptyString, wxT("registers.dat"),
                    wxFileSelectorDefaultWildcardStr,
                    wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
            if (saveFileDialog.ShowModal() == wxID_CANCEL) return;
            success = registerData->saveData(
                    ma::Str(saveFileDialog.GetPath().c_str()));
            if (!success)
            {
                wxMessageBox(wxT("Save Failed"));
            }
        }
    }

    void populateByteCheckboxes(wxCheckBox** checkBoxArray, short value)
    {
        for (unsigned int i = 0; i < 8; ++i)
        {
            short mask = 1 << i;
            checkBoxArray[i]->SetValue(value & mask);
        }
    }

    short getByteValue(wxCheckBox** checkBoxArray)
    {
        short value = 0;
        for (unsigned int i = 0; i < 8; ++i)
        {
            if (checkBoxArray[i]->IsChecked())
            {
                value += 1 << i;
            }
        }
        return value;
    }

    wxString getPreScalerString(unsigned int index)
    {
        switch (index)
        {
        case 0: return wxT("1:1");
        case 1: return wxT("1:8");
        case 2: return wxT("1:64");
        case 3: return wxT("1:256");
        }
        return wxT("");
    }
}
