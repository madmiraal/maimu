// (c) 2016: Marcel Admiraal

// (c) 2013 - 2016: Marcel Admiraal

#include "ximulistener.h"

namespace xIMU
{
    void Listener::dataReceived(ma::Data* data)
    {
        switch (data->getDataType())
        {
            case (unsigned int)PacketHeader::Error:
            {
                ErrorData* errorData = (ErrorData*)data;
                onErrorDataReceived(*errorData);
                // Inform IMU listeners.
                data->setDataType(ma::DataType::MessageData);
                data->setStringData(errorData->getMessage().c_str());
                IMUListener::onMessageDataReceived(*data);
                break;
            }
            case (unsigned int)PacketHeader::Command:
            {
                CommandData* commandData = (CommandData*)data;
                onCommandDataReceived(*commandData);
                // Inform IMU listeners.
                data->setDataType(ma::DataType::MessageData);
                data->setStringData(commandData->getMessage().c_str());
                IMUListener::onMessageDataReceived(*data);
                break;
            }
            case (unsigned int)PacketHeader::WriteRegister:
                onRegisterDataReceived(*(RegisterData*)data);
                break;
            case (unsigned int)PacketHeader::WriteDateTime:
                onDateTimeDataReceived(*(DateTimeData*)data);
                break;
            case (unsigned char)PacketHeader::RawBatteryAndTemperatureData:
                onRawBatteryAndTemperatureDataReceived(
                        *(RawBatteryAndTemperatureData*)data);
                break;
            case (unsigned char)PacketHeader::CalBatteryAndTemperatureData:
                onCalBatteryAndTemperatureDataReceived(
                        *(CalBatteryAndTemperatureData*)data);
                break;
            case (unsigned char)PacketHeader::RawInertialAndMagneticData:
                onRawInertialAndMagneticDataReceived(
                        *(RawInertialAndMagneticData*)data);
                break;
            case (unsigned char)PacketHeader::CalInertialAndMagneticData:
            {
                CalInertialAndMagneticData* calIMUData =
                        (CalInertialAndMagneticData*)data;
                onCalInertialAndMagneticDataReceived(*calIMUData);
                // Inform IMU listeners.
                float floatData[3];
                // Inform IMU gyroscope listeners.
                calIMUData->getGyroscope(floatData);
                data->setVectorData(ma::Vector(floatData, 3));
                onGyroscopeDataReceived(*data);
                // Inform IMU accelerometer listeners.
                calIMUData->getAccelerometer(floatData);
                data->setVectorData(ma::Vector(floatData, 3));
                onAccelerometerDataReceived(*data);
                // Inform IMU magnetometer listeners.
                calIMUData->getMagnetometer(floatData);
                data->setVectorData(ma::Vector(floatData, 3));
                onMagnetometerDataReceived(*data);
                break;
            }
            case (unsigned char)PacketHeader::QuaternionData:
            {
                QuaternionData* quaternionData = (QuaternionData*)data;
                onQuaternionDataReceived(*quaternionData);
                // Inform IMU listeners.
                float floatData[4];
                quaternionData->getQuaternion(floatData);
                data->setDataType(ma::DataType::QuaternionData);
                data->setVectorData(ma::Vector(floatData, 4));
                onQuaternionReceived(*data);
                break;
            }
            case (unsigned char)PacketHeader::DigitalIOdata:
                onDigitalIODataReceived(*(DigitalIOData*)data);
                break;
            case (unsigned char)PacketHeader::RawAnalogueInputData:
                onRawAnalogueInputDataReceived(*(RawAnalogueInputData*)data);
                break;
            case (unsigned char)PacketHeader::CalAnalogueInputData:
            {
                CalAnalogueInputData* calAnalougeData =
                        (CalAnalogueInputData*)data;
                onCalAnalogueInputDataReceived(*calAnalougeData);
                // Inform IMU listeners.
                float floatData[8];
                calAnalougeData->getAX(floatData);
                data->setDataType(ma::DataType::AnalogueData);
                data->setVectorData(ma::Vector(floatData, 8));
                onAnalogueDataReceived(*data);
                break;
            }
            case (unsigned char)PacketHeader::PWMOutputData:
                onPWMOutputDataReceived(*(PWMOutputData*)data);
                break;
            case (unsigned char)PacketHeader::RawADXL345BusData:
                onRawADXL345BusDataReceived(*(RawADXL345BusData*)data);
                break;
            case (unsigned char)PacketHeader::CalADXL345BusData:
                onCalADXL345BusDataReceived(*(CalADXL345BusData*)data);
                break;
            default:
                ma::IMUListener::dataReceived(data);
                break;
        }
    }
}
