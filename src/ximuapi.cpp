// (c) 2013 - 2016: Marcel Admiraal

#include "ximuapi.h"

#include "ximudata.h"

namespace xIMU
{
    API::API() : ma::IMUAPI()
    {
    }

    API::~API()
    {
    }

    bool API::getRegisters()
    {
        // Todo.
        return false;
    }

    bool API::setRegisters(ma::Data* registerData)
    {
        // Todo.
        return false;
    }

    bool API::sendCommand(const CommandCode commandCode)
    {
        unsigned int decodedLength = 4;
        unsigned int encodedLength = calcEncodedPacketLength(decodedLength);
        unsigned char packet[encodedLength];
        packet[0] = (unsigned char) PacketHeader::Command;
        packet[1] = (unsigned char) ((short)commandCode >> 8);
        packet[2] = (unsigned char) commandCode;
        packet[3] = calcChecksum(packet, decodedLength);
        encodePacket(packet, decodedLength);
        return sendPacket(packet, encodedLength);
    }

    bool API::sendReadRegister(const RegisterAddress registerAddress)
    {
        unsigned int decodedLength = 4;
        unsigned int encodedLength = calcEncodedPacketLength(decodedLength);
        unsigned char packet[encodedLength];
        packet[0] = (unsigned char) PacketHeader::ReadRegister;
        packet[1] = (unsigned char) ((short)registerAddress >> 8);
        packet[2] = (unsigned char) registerAddress;
        packet[3] = calcChecksum(packet, decodedLength);
        encodePacket(packet, decodedLength);
        return sendPacket(packet, encodedLength);
    }

    bool API::sendWriteRegister(
        const RegisterAddress registerAddress,
        const unsigned short value
        )
    {
        unsigned int decodedLength = 6;
        unsigned int encodedLength = calcEncodedPacketLength(decodedLength);
        unsigned char packet[encodedLength];
        packet[0] = (unsigned char) PacketHeader::WriteRegister;
        packet[1] = (unsigned char) ((unsigned short)registerAddress >> 8);
        packet[2] = (unsigned char) registerAddress;
        packet[3] = (unsigned char) (value >> 8);
        packet[4] = (unsigned char) value;
        packet[5] = calcChecksum(packet, decodedLength);
        encodePacket(packet, decodedLength);
        return sendPacket(packet, encodedLength);
    }

    bool API::sendReadDateTime()
    {
        unsigned int decodedLength = 2;
        unsigned int encodedLength = calcEncodedPacketLength(decodedLength);
        unsigned char packet[encodedLength];
        packet[0] = (unsigned char) PacketHeader::ReadDateTime;
        packet[1] = calcChecksum(packet, decodedLength);
        encodePacket(packet, decodedLength);
        return sendPacket(packet, encodedLength);
    }

    bool API::sendWriteDateTime(
        const int year,
        const int month,
        const int day,
        const int hour,
        const int minute,
        const int second
        )
    {
        // Ensure data values are valid.
        int y = year % 100;                 // Year value between 0 and 99.
        int mo = ((month - 1) % 12) + 1;    // Month value between 1 and 12.
        int d = ((day - 1) % 31) + 1;       // Day value between 1 and 31.
        int h = hour % 24;                  // Hour value between 0 and 23.
        int mi = minute % 60;               // Minute value between 0 and 59.
        int s = second % 60;                // Second value between 0 and 59.
        unsigned int decodedLength = 8;
        unsigned int encodedLength = calcEncodedPacketLength(decodedLength);
        unsigned char packet[encodedLength];
        packet[0] = (unsigned char) PacketHeader::WriteDateTime;
        // Store values in four bit decimal format.
        packet[1] = (unsigned char) (((y / 10) << 4)|(y % 10));
        packet[2] = (unsigned char) (((mo / 10) << 4)|(mo % 10));
        packet[3] = (unsigned char) (((d / 10) << 4)|(d % 10));
        packet[4] = (unsigned char) (((h / 10) << 4)|(h % 10));
        packet[5] = (unsigned char) (((mi / 10) << 4)|(mi % 10));
        packet[6] = (unsigned char) (((s / 10) << 4)|(s % 10));
        packet[7] = calcChecksum(packet, decodedLength);
        encodePacket(packet, decodedLength);
        return sendPacket(packet, encodedLength);
    }

    bool API::sendDigitalIO(
        const unsigned char directionByte,
        const unsigned char valueByte
        )
    {
        unsigned int decodedLength = 4;
        unsigned int encodedLength = calcEncodedPacketLength(decodedLength);
        unsigned char packet[encodedLength];
        packet[0] = (unsigned char) PacketHeader::DigitalIOdata;
        packet[1] = directionByte;
        packet[2] = valueByte;
        packet[3] = calcChecksum(packet, decodedLength);
        encodePacket(packet, decodedLength);
        return sendPacket(packet, encodedLength);
    }

    bool API::sendPWMOutput(
        unsigned short channel1,
        unsigned short channel2,
        unsigned short channel3,
        unsigned short channel4
        )
    {
        unsigned int decodedLength = 10;
        unsigned int encodedLength = calcEncodedPacketLength(decodedLength);
        unsigned char packet[encodedLength];
        packet[0] = (unsigned char) PacketHeader::PWMOutputData;
        packet[1] = (unsigned char) (channel1 >> 8);
        packet[2] = (unsigned char) channel1;
        packet[3] = (unsigned char) (channel2 >> 8);
        packet[4] = (unsigned char) channel2;
        packet[5] = (unsigned char) (channel3 >> 8);
        packet[6] = (unsigned char) channel3;
        packet[7] = (unsigned char) (channel4 >> 8);
        packet[8] = (unsigned char) channel4;
        packet[9] = calcChecksum(packet, decodedLength);
        for (unsigned int i = 0; i < decodedLength - 1; i++)
        {
            packet[decodedLength - 1] += packet[i];
        }
        encodePacket(packet, decodedLength);
        return sendPacket(packet, encodedLength);
    }

    ma::Data* API::getNextData()
    {
        unsigned int packetLength = 256;
        unsigned char packet[packetLength];
        getPacket(packet, packetLength);
        if (packetLength > 0)
            return extractData(packet, packetLength);
        else
            return 0;
    }

    void API::getPacket(unsigned char* packet, unsigned int& packetLength)
    {
        unsigned int bytesRead = 0;
        // Get bytes until framing packet found or maximum bytes read.
        do
        {
            unsigned char byte;
            if (readBytes(&byte, 1) != 1)
            {
                packetLength = 0;
                return;
            }
            packet[bytesRead] = byte;
            ++bytesRead;
        } while (bytesRead <= packetLength &&
                (packet[bytesRead-1] & 0x80) != 0x80);

        packetLength = bytesRead;
        if (packetLength > 0)
        {
            decodePacket(packet, packetLength);
            packetLength = calcDecodedPacketLength(packetLength);
        }
        if (packetLength > 0 && !checkChecksum(packet, packetLength))
        {
            packetLength = 0;
        }
    }

    ma::Data* API::extractData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        switch (packet[0])
        {
            case ((unsigned char)PacketHeader::Error):
                return extractErrorData(packet, packetLength);
            case ((unsigned char)PacketHeader::Command):
                return extractCommandData(packet, packetLength);
            case ((unsigned char)PacketHeader::WriteRegister):
                return extractRegisterData(packet, packetLength);
            case ((unsigned char)PacketHeader::WriteDateTime):
                return extractDateTimeData(packet, packetLength);
            case ((unsigned char)PacketHeader::RawBatteryAndTemperatureData):
                return extractRawBatteryAndTemperatureData(packet, packetLength);
            case ((unsigned char)PacketHeader::CalBatteryAndTemperatureData):
                return extractCalBatteryAndTemperatureData(packet, packetLength);
            case ((unsigned char)PacketHeader::RawInertialAndMagneticData):
                return extractRawInertialAndMagneticData(packet, packetLength);
            case ((unsigned char)PacketHeader::CalInertialAndMagneticData):
                return extractCalInertialAndMagneticData(packet, packetLength);
            case ((unsigned char)PacketHeader::QuaternionData):
                return extractQuaternionData(packet, packetLength);
            case ((unsigned char)PacketHeader::DigitalIOdata):
                return extractDigitalIOData(packet, packetLength);
            case ((unsigned char)PacketHeader::RawAnalogueInputData):
                return extractRawAnalogueInputData(packet, packetLength);
            case ((unsigned char)PacketHeader::CalAnalogueInputData):
                return extractCalAnalogueInputData(packet, packetLength);
            case ((unsigned char)PacketHeader::PWMOutputData):
                return extractPWMOutputData(packet, packetLength);
            case ((unsigned char)PacketHeader::RawADXL345BusData):
                return extractRawADXL345BusData(packet, packetLength);
            case ((unsigned char)PacketHeader::CalADXL345BusData):
                return extractCalADXL345BusData(packet, packetLength);
            default:
                return new ma::Data();
        }
    }

    unsigned int API::calcEncodedPacketLength(const unsigned int decodedLength)
    {
        unsigned int encodedLength = decodedLength * 8 / 7;
        if ((decodedLength * 8) % 7 != 0)
        {
            ++encodedLength;
        }
        return encodedLength;
    }

    unsigned int API::calcDecodedPacketLength(const unsigned int encodedLength)
    {
        return encodedLength * 7 / 8;
    }

    void API::encodePacket(unsigned char* packet,
            const unsigned int decodedLength)
    {
        // Zero the first extra byte to ensure unused bits are zero.
        packet[decodedLength] = '\0';

        // Get encoded packet length.
        unsigned int encodedLength = calcEncodedPacketLength(decodedLength);

        // Encode packet by shifting each byte right and wrapping the bit.
        for (unsigned int j = 0; j < encodedLength; j++)
        {
            for (unsigned int i = encodedLength - 1; i > j; --i)
            {
                packet[i] >>= 1;
                if ((packet[i - 1] & 0x01) == 0x01) packet[i] |= 0x80;
            }
            packet[j] >>= 1;
        }

        // Set most significant bit of last byte to 1.
        packet[encodedLength - 1] |= 0x80;
    }

    void API::decodePacket(unsigned char* packet,
            const unsigned int encodedLength)
    {
        // Decode packet by dropping the most significant bits and shifting.
        // Note: We increase j by one to prevent the underflow.
        for (unsigned int j = encodedLength; j > 0; --j)
        {
            packet[j-1] <<= 1;
            for (unsigned int i = j; i < encodedLength; ++i)
            {
                if ((packet[i] & 0x80) == 0x80) packet[i-1] |= 0x01;
                packet[i] <<= 1;
            }
        }
    }

    unsigned char API::calcChecksum(const unsigned char* packet,
            const unsigned int packetLength)
    {
        unsigned char checksum = 0;
        for (unsigned int i = 0; i < (packetLength - 1); ++i)
        {
            checksum += packet[i];
        }
        return checksum;
    }

    bool API::checkChecksum(const unsigned char* packet,
            const unsigned int packetLength)
    {
        // Confirm checksum
        unsigned char checksum = calcChecksum(packet, packetLength);
        return  checksum == packet[packetLength - 1];
    }

    ma::Data* API::extractErrorData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 4)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        ErrorData* data = new ErrorData (concat(packet[1], packet[2]));
        data->setDataType((unsigned int)PacketHeader::Error);
        return data;
    }

    ma::Data* API::extractCommandData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 4)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        CommandData* data = new CommandData(concat(packet[1], packet[2]));
        data->setDataType((unsigned int)PacketHeader::Command);
        return data;
    }

    ma::Data* API::extractRegisterData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 6)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        ma::Data* data = new RegisterData(
            concat(packet[1], packet[2]),
            concat(packet[3], packet[4])
        );
        data->setDataType((unsigned int)PacketHeader::WriteRegister);
        return data;
    }

    ma::Data* API::extractDateTimeData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 8)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        ma::Data* data = new DateTimeData(
            (int)(10 * ((packet[1] & 0xF0) >> 4) + (packet[1] & 0x0F)),
            (int)(10 * ((packet[2] & 0xF0) >> 4) + (packet[2] & 0x0F)),
            (int)(10 * ((packet[3] & 0xF0) >> 4) + (packet[3] & 0x0F)),
            (int)(10 * ((packet[4] & 0xF0) >> 4) + (packet[4] & 0x0F)),
            (int)(10 * ((packet[5] & 0xF0) >> 4) + (packet[5] & 0x0F)),
            (int)(10 * ((packet[6] & 0xF0) >> 4) + (packet[6] & 0x0F))
        );
        data->setDataType((unsigned int)PacketHeader::WriteDateTime);
        return data;
    }

    ma::Data* API::extractRawBatteryAndTemperatureData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 6)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        ma::Data* data = new RawBatteryAndTemperatureData(
            (short)concat(packet[1], packet[2]),
            (short)concat(packet[3], packet[4])
        );
        data->setDataType((unsigned int)PacketHeader::RawBatteryAndTemperatureData);
        return data;
    }

    ma::Data* API::extractCalBatteryAndTemperatureData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 6)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        ma::Data* data = new CalBatteryAndTemperatureData(
            shortToFloat(concat(packet[1], packet[2]),
                QValue::CalibratedBattery),
            shortToFloat(concat(packet[3], packet[4]),
                QValue::CalibratedThermometer)
        );
        data->setDataType((unsigned int)PacketHeader::CalBatteryAndTemperatureData);
        return data;
    }

    ma::Data* API::extractRawInertialAndMagneticData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 20)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        short gyroscope[] = {
            (short)concat(packet[1], packet[2]),
            (short)concat(packet[3], packet[4]),
            (short)concat(packet[5], packet[6])
        };
        short accelerometer[] = {
            (short)concat(packet[7], packet[8]),
            (short)concat(packet[9], packet[10]),
            (short)concat(packet[11], packet[12])
        };
        short magnetometer[] = {
            (short)concat(packet[13], packet[14]),
            (short)concat(packet[15], packet[16]),
            (short)concat(packet[17], packet[18])
        };
        ma::Data* data = new RawInertialAndMagneticData(
                gyroscope, accelerometer, magnetometer);
        data->setDataType((unsigned int)PacketHeader::RawInertialAndMagneticData);
        return data;
    }

    ma::Data* API::extractCalInertialAndMagneticData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 20)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        float gyroscope[] = {
            shortToFloat(concat(packet[1], packet[2]),
                QValue::CalibratedGyroscope),
            shortToFloat(concat(packet[3], packet[4]),
                QValue::CalibratedGyroscope),
            shortToFloat(concat(packet[5], packet[6]),
                QValue::CalibratedGyroscope)
        };
        float accelerometer[] = {
            shortToFloat(concat(packet[7], packet[8]),
                QValue::CalibratedAccelerometer),
            shortToFloat(concat(packet[9], packet[10]),
                QValue::CalibratedAccelerometer),
            shortToFloat(concat(packet[11], packet[12]),
                QValue::CalibratedAccelerometer)
        };
        float magnetometer[] = {
            shortToFloat(concat(packet[13], packet[14]),
                QValue::CalibratedMagnetometer),
            shortToFloat(concat(packet[15], packet[16]),
                QValue::CalibratedMagnetometer),
            shortToFloat(concat(packet[17], packet[18]),
                QValue::CalibratedMagnetometer)
        };
        ma::Data* data = new CalInertialAndMagneticData(
                gyroscope, accelerometer, magnetometer);
        data->setDataType((unsigned int)PacketHeader::CalInertialAndMagneticData);
        return data;
    }

    ma::Data* API::extractQuaternionData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 10)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        float quaternion[] = {
            shortToFloat(concat(packet[1], packet[2]),
                QValue::Quaternion),
            shortToFloat(concat(packet[3], packet[4]),
                QValue::Quaternion),
            shortToFloat(concat(packet[5], packet[6]),
                QValue::Quaternion),
            shortToFloat(concat(packet[7], packet[8]),
                QValue::Quaternion)
        };
        ma::Data* data = new QuaternionData(quaternion);
        data->setDataType((unsigned int)PacketHeader::QuaternionData);
        return data;
    }

    ma::Data* API::extractDigitalIOData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 4)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        ma::Data* data = new DigitalIOData(packet[1], packet[2]);
        data->setDataType((unsigned int)PacketHeader::DigitalIOdata);
        return data;
    }

    ma::Data* API::extractRawAnalogueInputData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 18)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        ma::Data* data = new RawAnalogueInputData(
            (short)concat(packet[1], packet[2]),
            (short)concat(packet[3], packet[4]),
            (short)concat(packet[5], packet[6]),
            (short)concat(packet[7], packet[8]),
            (short)concat(packet[9], packet[10]),
            (short)concat(packet[11], packet[12]),
            (short)concat(packet[13], packet[14]),
            (short)concat(packet[15], packet[16])
        );
        data->setDataType((unsigned int)PacketHeader::RawAnalogueInputData);
        return data;
    }

    ma::Data* API::extractCalAnalogueInputData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 18)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        ma::Data* data = new CalAnalogueInputData(
            shortToFloat(concat(packet[1], packet[2]),
                QValue::CalibratedAnalogueInput),
            shortToFloat(concat(packet[3], packet[4]),
                QValue::CalibratedAnalogueInput),
            shortToFloat(concat(packet[5], packet[6]),
                QValue::CalibratedAnalogueInput),
            shortToFloat(concat(packet[7], packet[8]),
                QValue::CalibratedAnalogueInput),
            shortToFloat(concat(packet[9], packet[10]),
                QValue::CalibratedAnalogueInput),
            shortToFloat(concat(packet[11], packet[12]),
                QValue::CalibratedAnalogueInput),
            shortToFloat(concat(packet[13], packet[14]),
                QValue::CalibratedAnalogueInput),
            shortToFloat(concat(packet[15], packet[16]),
                QValue::CalibratedAnalogueInput)
        );
        data->setDataType((unsigned int)PacketHeader::CalAnalogueInputData);
        return data;
    }

    ma::Data* API::extractPWMOutputData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 10)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        ma::Data* data = new PWMOutputData(
            concat(packet[1], packet[2]),
            concat(packet[3], packet[4]),
            concat(packet[5], packet[6]),
            concat(packet[7], packet[8])
        );
        data->setDataType((unsigned int)PacketHeader::PWMOutputData);
        return data;
    }

    ma::Data* API::extractRawADXL345BusData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 26)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        short ADXL345A[] = {
            (short)concat(packet[1], packet[2]),
            (short)concat(packet[3], packet[4]),
            (short)concat(packet[5], packet[6])
        };
        short ADXL345B[] = {
            (short)concat(packet[7], packet[8]),
            (short)concat(packet[9], packet[10]),
            (short)concat(packet[11], packet[12])
        };
        short ADXL345C[] = {
            (short)concat(packet[13], packet[14]),
            (short)concat(packet[15], packet[16]),
            (short)concat(packet[17], packet[18])
        };
        short ADXL345D[] = {
            (short)concat(packet[19], packet[20]),
            (short)concat(packet[21], packet[22]),
            (short)concat(packet[23], packet[24])
        };
        ma::Data* data = new RawADXL345BusData(
            ADXL345A, ADXL345B, ADXL345C, ADXL345D);
        data->setDataType((unsigned int)PacketHeader::RawADXL345BusData);
        return data;
    }

    ma::Data* API::extractCalADXL345BusData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 26)
        {
            // Invalid number of unsigned chars for packet header.
            return new ma::Data();
        }
        float ADXL345A[] = {
            shortToFloat(concat(packet[1], packet[2]),
                QValue::CalibratedADXL345),
            shortToFloat(concat(packet[3], packet[4]),
                QValue::CalibratedADXL345),
            shortToFloat(concat(packet[5], packet[6]),
                QValue::CalibratedADXL345)
        };
        float ADXL345B[] = {
            shortToFloat(concat(packet[7], packet[8]),
                QValue::CalibratedADXL345),
            shortToFloat(concat(packet[9], packet[10]),
                QValue::CalibratedADXL345),
            shortToFloat(concat(packet[11], packet[12]),
                QValue::CalibratedADXL345)
        };
        float ADXL345C[] = {
            shortToFloat(concat(packet[13], packet[14]),
                QValue::CalibratedADXL345),
            shortToFloat(concat(packet[15], packet[16]),
                QValue::CalibratedADXL345),
            shortToFloat(concat(packet[17], packet[18]),
                QValue::CalibratedADXL345)
        };
        float ADXL345D[] = {
            shortToFloat(concat(packet[19], packet[20]),
                QValue::CalibratedADXL345),
            shortToFloat(concat(packet[21], packet[22]),
                QValue::CalibratedADXL345),
            shortToFloat(concat(packet[23], packet[24]),
                QValue::CalibratedADXL345)
        };
        ma::Data* data = new CalADXL345BusData(
            ADXL345A, ADXL345B, ADXL345C, ADXL345D);
        data->setDataType((unsigned int)PacketHeader::CalADXL345BusData);
        return data;
    }

    unsigned short concat(const unsigned char MSB, const unsigned char LSB)
    {
        return ((unsigned short)MSB << 8) | (unsigned short)LSB;
    }
}
