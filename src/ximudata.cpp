// (c) 2013 - 2016: Marcel Admiraal

#include "ximudata.h"

#include <cmath>

namespace xIMU
{
    ErrorData::ErrorData() :
            ma::Data(), errorCode(ErrorCode::NoError)
    {
    }

    ErrorData::ErrorData(const ErrorData& source) :
            ma::Data(source), errorCode(source.errorCode)
    {
    }

    ErrorData::ErrorData(const ErrorCode errorCode) :
            ma::Data(), errorCode(errorCode)
    {
    }

    ErrorData::ErrorData(const unsigned short errorCode) :
            ma::Data(), errorCode((ErrorCode)errorCode)
    {
    }

    ErrorData::~ErrorData()
    {
    }

    void swap(ErrorData& first, ErrorData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.errorCode, second.errorCode);
    }

    ErrorData& ErrorData::operator=(ErrorData source)
    {
        swap(*this, source);
        return *this;
    }

    ErrorCode ErrorData::getErrorCode() const
    {
        return errorCode;
    }

    void ErrorData::setErrorCode(const ErrorCode errorCode)
    {
        this->errorCode = errorCode;
    }

    void ErrorData::setErrorCode(const unsigned short errorCode)
    {
        this->errorCode = (ErrorCode)errorCode;
    }

    std::string ErrorData::getMessage() const
    {
        switch (errorCode)
        {
            case (ErrorCode::NoError):
                return "No error.";
            case (ErrorCode::FactoryResetFailed):
                return "Factory reset failed.";
            case (ErrorCode::LowBattery):
                return "Low battery.";
            case (ErrorCode::USBreceiveBufferOverrun):
                return "USB receive buffer overrun.";
            case (ErrorCode::USBtransmitBufferOverrun):
                return "USB transmit buffer overrun.";
            case (ErrorCode::BluetoothReceiveBufferOverrun):
                return "Bluetooth receive buffer overrun.";
            case (ErrorCode::BluetoothTransmitBufferOverrun):
                return "Bluetooth transmit buffer overrun.";
            case (ErrorCode::SDcardWriteBufferOverrun):
                return "SD card write buffer overrun.";
            case (ErrorCode::TooFewBytesInPacket):
                return "Too few bytes in packet.";
            case (ErrorCode::TooManyBytesInPacket):
                return "Too many bytes in packet.";
            case (ErrorCode::InvalidChecksum):
                return "Invalid checksum.";
            case (ErrorCode::UnknownHeader):
                return "Unknown packet header.";
            case (ErrorCode::InvalidNumBytesForPacketHeader):
                return "Invalid number of bytes for packet header.";
            case (ErrorCode::InvalidRegisterAddress):
                return "Invalid register address.";
            case (ErrorCode::RegisterReadOnly):
                return "Cannot write to read-only register.";
            case (ErrorCode::InvalidRegisterValue):
                return "Invalid register value.";
            case (ErrorCode::InvalidCommand):
                return "Invalid command.";
            case (ErrorCode::GyroscopeAxisNotAt200dps):
                return "Gyroscope axis not at 200 ˚/s. Operation aborted.";
            case (ErrorCode::GyroscopeNotStationary):
                return "Gyroscope not stationary. Operation aborted.";
            case (ErrorCode::AcceleroemterAxisNotAt1g):
                return "Acceleroemter axis not at ±1 g. Operation aborted.";
            case (ErrorCode::MagnetometerSaturation):
                return "Magnetometer saturation occurred. Operation aborted.";
            case (ErrorCode::IncorrectAuxillaryPortMode):
                return "Auxiliary port in incorrect mode.";
            case (ErrorCode::UARTreceiveBufferOverrun):
                return "UART receive buffer overrun.";
            case (ErrorCode::UARTtransmitBufferOverrun):
                return "UART transmit buffer overrun.";
            default:
                return "";
        }
    }

    CommandData::CommandData() :
            ma::Data(), commandCode(CommandCode::NullCommand)
    {
    }

    CommandData::CommandData(const CommandData& source) :
            ma::Data(source), commandCode(source.commandCode)
    {
    }

    CommandData::CommandData(const CommandCode commandCode) :
            ma::Data(), commandCode(commandCode)
    {
    }

    CommandData::CommandData(const unsigned short commandCode) :
            ma::Data(), commandCode((CommandCode)commandCode)
    {
    }

    CommandData::~CommandData()
    {
    }

    void swap(CommandData& first, CommandData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.commandCode, second.commandCode);
    }

    CommandData& CommandData::operator=(CommandData source)
    {
        swap(*this, source);
        return *this;
    }

    CommandCode CommandData::getCommandCode() const
    {
        return commandCode;
    }

    void CommandData::setCommandCode(const CommandCode commandCode)
    {
        this->commandCode = commandCode;
    }

    void CommandData::setCommandCode(const unsigned short commandCode)
    {
        this->commandCode = (CommandCode)commandCode;
    }

    std::string CommandData::getMessage() const
    {
        switch (commandCode)
        {
            case (CommandCode::NullCommand):
                return "Null command.";
            case (CommandCode::FactoryReset):
                return "Factory reset.";
            case (CommandCode::Reset):
                return "Reset.";
            case (CommandCode::Sleep):
                return "Sleep.";
            case (CommandCode::ResetSleepTimer):
                return "Reset sleep timer.";
            case (CommandCode::SampleGyroscopeAxisAt200dps):
                return "Sample gyroscope axis at ±200 dps.";
            case (CommandCode::CalculateGyroscopeSensitivity):
                return "Calculate gyroscope sensitivity.";
            case (CommandCode::SampleGyroscopeBiasTemp1):
                return "Sample gyroscope bias at temperature 1.";
            case (CommandCode::SampleGyroscopeBiasTemp2):
                return "Sample gyroscope bias at temperature 2.";
            case (CommandCode::CalculateGyroscopeBiasParameters):
                return "Calculate gyroscope bias parameters.";
            case (CommandCode::SampleAccelerometerAxisAt1g):
                return "Sample accelerometer axis at ±1 g.";
            case (CommandCode::CalculateAccelerometerBiasAndSensitivity):
                return "Calculate accelerometer bias and sensitivity";
            case (CommandCode::MeasureMagnetometerBiasAndSensitivity):
                return "Measure magnetometer bias and sensitivity.";
            case (CommandCode::AlgorithmInitialise):
                return "Algorithm initialise.";
            case (CommandCode::AlgorithmTare):
                return "Algorithm tare.";
            case (CommandCode::AlgorithmClearTare):
                return "Algorithm clear tare.";
            case (CommandCode::AlgorithmInitialiseThenTare):
                return "Algorithm initialise then tare.";
            default:
                return "";
        }
    }

    RegisterData::RegisterData(const RegisterData& source) :
            ma::Data(source), address(source.address), value(source.value)
    {
    }

    RegisterData::RegisterData(const RegisterAddress address) :
            ma::Data(), address(address), value(0)
    {
    }

    RegisterData::RegisterData(const RegisterAddress address,
            const float value) :
            ma::Data(), address(address)
    {
        setFloatValue(value);
    }

    RegisterData::RegisterData(const unsigned short address,
            const unsigned short value) :
            ma::Data(), address((RegisterAddress)address), value(value)
    {
    }

    RegisterData::~RegisterData()
    {
    }

    void swap(RegisterData& first, RegisterData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.address, second.address);
        std::swap(first.value, second.value);
    }

    RegisterData& RegisterData::operator=(RegisterData source)
    {
        swap(*this, source);
        return *this;
    }

    RegisterAddress RegisterData::getAddress() const
    {
        return address;
    }

    unsigned short RegisterData::getValue() const
    {
        return value;
    }

    float RegisterData::getFloatValue() const
    {
        return shortToFloat(value, getQValue(address));
    }

    void RegisterData::setAddress(const RegisterAddress registerAddress)
    {
        this->address = address;
    }

    void RegisterData::setValue(const unsigned short value)
    {
        this->value = value;
    }

    void RegisterData::setFloatValue(const float floatValue)
    {
        this->value = floatToShort(floatValue, getQValue(address));
    }

    void RegisterData::setData(const unsigned short address,
            const unsigned short value)
    {
        this->address = (RegisterAddress)address;
        this->value = value;
    }

    QValue RegisterData::getQValue(const RegisterAddress address) const
    {
        switch (address)
        {
            case (RegisterAddress::BatterySensitivity):
                return QValue::BatterySensitivity;
            case (RegisterAddress::BatteryBias):
                return QValue::BatteryBias;
            case (RegisterAddress::ThermometerSensitivity):
                return QValue::ThermometerSensitivity;
            case (RegisterAddress::ThermometerBias):
                return QValue::ThermometerBias;
            case (RegisterAddress::GyroscopeSensitivityX):
                return QValue::GyroscopeSensitivity;
            case (RegisterAddress::GyroscopeSensitivityY):
                return QValue::GyroscopeSensitivity;
            case (RegisterAddress::GyroscopeSensitivityZ):
                return QValue::GyroscopeSensitivity;
            case (RegisterAddress::GyroscopeSampledPlus200dpsX):
                return QValue::GyroscopeSampled200dps;
            case (RegisterAddress::GyroscopeSampledPlus200dpsY):
                return QValue::GyroscopeSampled200dps;
            case (RegisterAddress::GyroscopeSampledPlus200dpsZ):
                return QValue::GyroscopeSampled200dps;
            case (RegisterAddress::GyroscopeSampledMinus200dpsX):
                return QValue::GyroscopeSampled200dps;
            case (RegisterAddress::GyroscopeSampledMinus200dpsY):
                return QValue::GyroscopeSampled200dps;
            case (RegisterAddress::GyroscopeSampledMinus200dpsZ):
                return QValue::GyroscopeSampled200dps;
            case (RegisterAddress::GyroscopeBiasAt25degCX):
                return QValue::GyroscopeBiasAt25degC;
            case (RegisterAddress::GyroscopeBiasAt25degCY):
                return QValue::GyroscopeBiasAt25degC;
            case (RegisterAddress::GyroscopeBiasAt25degCZ):
                return QValue::GyroscopeBiasAt25degC;
            case (RegisterAddress::GyroscopeBiasTempSensitivityX):
                return QValue::GyroscopeBiasTempSensitivity;
            case (RegisterAddress::GyroscopeBiasTempSensitivityY):
                return QValue::GyroscopeBiasTempSensitivity;
            case (RegisterAddress::GyroscopeBiasTempSensitivityZ):
                return QValue::GyroscopeBiasTempSensitivity;
            case (RegisterAddress::GyroscopeSample1Temp):
                return QValue::CalibratedThermometer;
            case (RegisterAddress::GyroscopeSample1BiasX):
                return QValue::GyroscopeSampledBias;
            case (RegisterAddress::GyroscopeSample1BiasY):
                return QValue::GyroscopeSampledBias;
            case (RegisterAddress::GyroscopeSample1BiasZ):
                return QValue::GyroscopeSampledBias;
            case (RegisterAddress::GyroscopeSample2Temp):
                return QValue::CalibratedThermometer;
            case (RegisterAddress::GyroscopeSample2BiasX):
                return QValue::GyroscopeSampledBias;
            case (RegisterAddress::GyroscopeSample2BiasY):
                return QValue::GyroscopeSampledBias;
            case (RegisterAddress::GyroscopeSample2BiasZ):
                return QValue::GyroscopeSampledBias;
            case (RegisterAddress::AccelerometerSensitivityX):
                return QValue::AccelerometerSensitivity;
            case (RegisterAddress::AccelerometerSensitivityY):
                return QValue::AccelerometerSensitivity;
            case (RegisterAddress::AccelerometerSensitivityZ):
                return QValue::AccelerometerSensitivity;
            case (RegisterAddress::AccelerometerBiasX):
                return QValue::AccelerometerBias;
            case (RegisterAddress::AccelerometerBiasY):
                return QValue::AccelerometerBias;
            case (RegisterAddress::AccelerometerBiasZ):
                return QValue::AccelerometerBias;
            case (RegisterAddress::AccelerometerSampledPlus1gX):
                return QValue::AccelerometerSampled1g;
            case (RegisterAddress::AccelerometerSampledPlus1gY):
                return QValue::AccelerometerSampled1g;
            case (RegisterAddress::AccelerometerSampledPlus1gZ):
                return QValue::AccelerometerSampled1g;
            case (RegisterAddress::AccelerometerSampledMinus1gX):
                return QValue::AccelerometerSampled1g;
            case (RegisterAddress::AccelerometerSampledMinus1gY):
                return QValue::AccelerometerSampled1g;
            case (RegisterAddress::AccelerometerSampledMinus1gZ):
                return QValue::AccelerometerSampled1g;
            case (RegisterAddress::MagnetometerSensitivityX):
                return QValue::MagnetometerSensitivity;
            case (RegisterAddress::MagnetometerSensitivityY):
                return QValue::MagnetometerSensitivity;
            case (RegisterAddress::MagnetometerSensitivityZ):
                return QValue::MagnetometerSensitivity;
            case (RegisterAddress::MagnetometerBiasX):
                return QValue::MagnetometerBias;
            case (RegisterAddress::MagnetometerBiasY):
                return QValue::MagnetometerBias;
            case (RegisterAddress::MagnetometerBiasZ):
                return QValue::MagnetometerBias;
            case (RegisterAddress::MagnetometerHardIronBiasX):
                return QValue::MagnetometerHardIronBias;
            case (RegisterAddress::MagnetometerHardIronBiasY):
                return QValue::MagnetometerHardIronBias;
            case (RegisterAddress::MagnetometerHardIronBiasZ):
                return QValue::MagnetometerHardIronBias;
            case (RegisterAddress::AlgorithmKp):
                return QValue::AlgorithmKp;
            case (RegisterAddress::AlgorithmKi):
                return QValue::AlgorithmKi;
            case (RegisterAddress::AlgorithmInitKp):
                return QValue::AlgorithmInitKp;
            case (RegisterAddress::AlgorithmInitPeriod):
                return QValue::AlgorithmInitPeriod;
            case (RegisterAddress::AlgorithmMinValidMag):
                return QValue::CalibratedMagnetometer;
            case (RegisterAddress::AlgorithmMaxValidMag):
                return QValue::CalibratedMagnetometer;
            case (RegisterAddress::AlgorithmTareQuat0):
                return QValue::Quaternion;
            case (RegisterAddress::AlgorithmTareQuat1):
                return QValue::Quaternion;
            case (RegisterAddress::AlgorithmTareQuat2):
                return QValue::Quaternion;
            case (RegisterAddress::AlgorithmTareQuat3):
                return QValue::Quaternion;
            case (RegisterAddress::BatteryShutdownVoltage):
                return QValue::CalibratedBattery;
            case (RegisterAddress::AnalogueInputSensitivity):
                return QValue::AnalogueInputSensitivity;
            case (RegisterAddress::AnalogueInputBias):
                return QValue::AnalogueInputBias;
            case (RegisterAddress::ADXL345AsensitivityX):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345AsensitivityY):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345AsensitivityZ):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345AbiasX):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345AbiasY):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345AbiasZ):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345BsensitivityX):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345BsensitivityY):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345BsensitivityZ):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345BbiasX):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345BbiasY):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345BbiasZ):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345CsensitivityX):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345CsensitivityY):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345CsensitivityZ):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345CbiasX):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345CbiasY):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345CbiasZ):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345DsensitivityX):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345DsensitivityY):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345DsensitivityZ):
                return QValue::ADXL345busSensitivity;
            case (RegisterAddress::ADXL345DbiasX):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345DbiasY):
                return QValue::ADXL345busBias;
            case (RegisterAddress::ADXL345DbiasZ):
                return QValue::ADXL345busBias;
            default:
                return (QValue)0;
        }
    }

    DateTimeData::DateTimeData() :
            ma::Data(), year(2000), month(1), day(1),
            hour(0), minute(0), second(0)
    {
    }

    DateTimeData::DateTimeData(const DateTimeData& source) :
            ma::Data(source),
            year(source.year), month(source.month), day(source.day),
            hour(source.hour), minute(source.minute), second(source.second)
    {
    }

    DateTimeData::DateTimeData(const int year, const int month, const int day,
            const int hour, const int minute, const int second) : ma::Data()
    {
        setDateTime(year, month, day, hour, minute, second);
    }

    DateTimeData::~DateTimeData()
    {
    }

    void swap(DateTimeData& first, DateTimeData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.year, second.year);
        std::swap(first.month, second.month);
        std::swap(first.day, second.day);
        std::swap(first.hour, second.hour);
        std::swap(first.minute, second.minute);
        std::swap(first.second, second.second);
    }

    DateTimeData& DateTimeData::operator=(DateTimeData source)
    {
        swap(*this, source);
        return *this;
    }

    int DateTimeData::getYear() const
    {
        return year;
    }

    int DateTimeData::getMonth() const
    {
        return month;
    }

    int DateTimeData::getDay() const
    {
        return day;
    }

    int DateTimeData::getHour() const
    {
        return hour;
    }

    int DateTimeData::getMinute() const
    {
        return minute;
    }

    int DateTimeData::getSecond() const
    {
        return second;
    }

    void DateTimeData::setYear(const int year)
    {
        // Ensure data value is valid.
        this->year = year % 100;                // Year value between 0 and 99.
    }

    void DateTimeData::setMonth(const int month)
    {
        this->month = ((month - 1) % 12) + 1;   // Month value between 1 and 12.
    }

    void DateTimeData::setDay(const int day)
    {
        this->day = ((day - 1) % 31) + 1;       // Day value between 1 and 31.
    }

    void DateTimeData::setHour(const int hour)
    {
        this->hour = hour % 24;                 // Hour value between 0 and 23.
    }

    void DateTimeData::setMinute(const int minute)
    {
        this->minute = minute % 60;             // Minute value between 0 and 59.
    }

    void DateTimeData::setSecond(const int second)
    {
        this->second = second % 60;             // Second value between 0 and 59.
    }

    void DateTimeData::setDateTime(const int year, const int month,
            const int day, const int hour, const int minute, const int second)
    {
        setYear(year);
        setMonth(month);
        setDay(day);
        setHour(hour);
        setMinute(minute);
        setSecond(second);
    }

    RawBatteryAndTemperatureData::RawBatteryAndTemperatureData() :
            ma::Data(), batteryVoltage(0), temperature(0)
    {
    }

    RawBatteryAndTemperatureData::RawBatteryAndTemperatureData(
            const RawBatteryAndTemperatureData& source) :
            ma::Data(source), batteryVoltage(source.batteryVoltage),
            temperature(source.temperature)
    {
    }

    RawBatteryAndTemperatureData::RawBatteryAndTemperatureData(
            const short batteryVoltage, const short temperature) :
            ma::Data(), batteryVoltage(batteryVoltage), temperature(temperature)
    {
    }

    RawBatteryAndTemperatureData::~RawBatteryAndTemperatureData()
    {
    }

    void swap(RawBatteryAndTemperatureData& first,
            RawBatteryAndTemperatureData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.batteryVoltage, second.batteryVoltage);
        std::swap(first.temperature, second.temperature);
    }

    RawBatteryAndTemperatureData& RawBatteryAndTemperatureData::operator=(
            RawBatteryAndTemperatureData source)
    {
        swap(*this, source);
        return *this;
    }

    short RawBatteryAndTemperatureData::getBatteryVoltage() const
    {
        return batteryVoltage;
    }

    short RawBatteryAndTemperatureData::getTemperature() const
    {
        return temperature;
    }

    void RawBatteryAndTemperatureData::setBatteryVoltage(
            const short batteryVoltage)
    {
        this->batteryVoltage = batteryVoltage;
    }

    void RawBatteryAndTemperatureData::setTemperature(
            const short temperature)
    {
        this->temperature = temperature;
    }

    CalBatteryAndTemperatureData::CalBatteryAndTemperatureData() :
            ma::Data(), batteryVoltage(0.0f), temperature(0.0f)
    {
    }

    CalBatteryAndTemperatureData::CalBatteryAndTemperatureData(
            const CalBatteryAndTemperatureData& source) :
            ma::Data(source), batteryVoltage(source.batteryVoltage),
            temperature(source.temperature)
    {
    }

    CalBatteryAndTemperatureData::CalBatteryAndTemperatureData(
            float batteryVoltage, float temperature) :
            ma::Data(), batteryVoltage(batteryVoltage),
            temperature(temperature)
    {
    }

    CalBatteryAndTemperatureData::~CalBatteryAndTemperatureData()
    {
    }

    void swap(CalBatteryAndTemperatureData& first,
            CalBatteryAndTemperatureData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.batteryVoltage, second.batteryVoltage);
        std::swap(first.temperature, second.temperature);
    }

    CalBatteryAndTemperatureData& CalBatteryAndTemperatureData::operator=(
            CalBatteryAndTemperatureData source)
    {
        swap(*this, source);
        return *this;
    }

    float CalBatteryAndTemperatureData::getBatteryVoltage() const
    {
        return batteryVoltage;
    }

    float CalBatteryAndTemperatureData::getTemperature() const
    {
        return temperature;
    }

    void CalBatteryAndTemperatureData::setBatteryVoltage(
            const float batteryVoltage)
    {
        this->batteryVoltage = batteryVoltage;
    }

    void CalBatteryAndTemperatureData::setTemperature(
            const float temperature)
    {
        this->temperature = temperature;
    }

    RawInertialAndMagneticData::RawInertialAndMagneticData() : ma::Data()
    {
        short zeroArray[3] = {0, 0, 0};
        setGyroscope(zeroArray);
        setAccelerometer(zeroArray);
        setMagnetometer(zeroArray);
    }

    RawInertialAndMagneticData::RawInertialAndMagneticData(
            const RawInertialAndMagneticData& source) : ma::Data(source)
    {
        setGyroscope(source.gyroscope);
        setAccelerometer(source.accelerometer);
        setMagnetometer(source.magnetometer);
    }

    RawInertialAndMagneticData::RawInertialAndMagneticData(
            const short gyroscope[],
            const short accelerometer[],
            const short magnetometer[]
            ) : ma::Data()
    {
        setGyroscope(gyroscope);
        setAccelerometer(accelerometer);
        setMagnetometer(magnetometer);
    }

    RawInertialAndMagneticData::~RawInertialAndMagneticData()
    {
    }

    void swap(RawInertialAndMagneticData& first,
            RawInertialAndMagneticData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.gyroscope, second.gyroscope);
        std::swap(first.accelerometer, second.accelerometer);
        std::swap(first.magnetometer, second.magnetometer);
    }

    RawInertialAndMagneticData& RawInertialAndMagneticData::operator=(
            RawInertialAndMagneticData source)
    {
        swap(*this, source);
        return *this;
    }

    void RawInertialAndMagneticData::getGyroscope(short gyroscope[]) const
    {
        for (int i = 0; i < 3; i++) gyroscope[i] = this->gyroscope[i];
    }

    void RawInertialAndMagneticData::getAccelerometer(short accelerometer[]) const
    {
        for (int i = 0; i < 3; i++) accelerometer[i] = this->accelerometer[i];
    }

    void RawInertialAndMagneticData::getMagnetometer(short magnetometer[]) const
    {
        for (int i = 0; i < 3; i++) magnetometer[i] = this->magnetometer[i];
    }

    void RawInertialAndMagneticData::setGyroscope(const short gyroscope[])
    {
        for (int i = 0; i < 3; i++) this->gyroscope[i] = gyroscope[i];
    }

    void RawInertialAndMagneticData::setAccelerometer(const short accelerometer[])
    {
        for (int i = 0; i < 3; i++) this->accelerometer[i] = accelerometer[i];
    }

    void RawInertialAndMagneticData::setMagnetometer(const short magnetometer[])
    {
        for (int i = 0; i < 3; i++) this->magnetometer[i] = magnetometer[i];
    }

    CalInertialAndMagneticData::CalInertialAndMagneticData() : ma::Data()
    {
        float zeroArray[3] = {0.0f, 0.0f, 0.0f};
        setGyroscope(zeroArray);
        setAccelerometer(zeroArray);
        setMagnetometer(zeroArray);
    }

    CalInertialAndMagneticData::CalInertialAndMagneticData(
            const CalInertialAndMagneticData& source) : ma::Data(source)
    {
        setGyroscope(source.gyroscope);
        setAccelerometer(source.accelerometer);
        setMagnetometer(source.magnetometer);
    }

    CalInertialAndMagneticData::CalInertialAndMagneticData(
            const float gyroscope[],
            const float accelerometer[],
            const float magnetometer[]
            ) : ma::Data()
    {
        setGyroscope(gyroscope);
        setAccelerometer(accelerometer);
        setMagnetometer(magnetometer);
    }

    CalInertialAndMagneticData::~CalInertialAndMagneticData()
    {
    }

    void swap(CalInertialAndMagneticData& first,
        CalInertialAndMagneticData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.gyroscope, second.gyroscope);
        std::swap(first.accelerometer, second.accelerometer);
        std::swap(first.magnetometer, second.magnetometer);
    }

    CalInertialAndMagneticData& CalInertialAndMagneticData::operator=(
            CalInertialAndMagneticData source)
    {
        swap(*this, source);
        return *this;
    }

    void CalInertialAndMagneticData::getGyroscope(float gyroscope[]) const
    {
        for (int i = 0; i < 3; i++) gyroscope[i] = this->gyroscope[i];
    }

    void CalInertialAndMagneticData::getAccelerometer(float accelerometer[]) const
    {
        for (int i = 0; i < 3; i++) accelerometer[i] = this->accelerometer[i];
    }

    void CalInertialAndMagneticData::getMagnetometer(float magnetometer[]) const
    {
        for (int i = 0; i < 3; i++) magnetometer[i] = this->magnetometer[i];
    }

    void CalInertialAndMagneticData::setGyroscope(const float gyroscope[])
    {
        for (int i = 0; i < 3; i++) this->gyroscope[i] = gyroscope[i];
    }

    void CalInertialAndMagneticData::setAccelerometer(const float accelerometer[])
    {
        for (int i = 0; i < 3; i++) this->accelerometer[i] = accelerometer[i];
    }

    void CalInertialAndMagneticData::setMagnetometer(const float magnetometer[])
    {
        for (int i = 0; i < 3; i++) this->magnetometer[i] = magnetometer[i];
    }

    QuaternionData::QuaternionData() : ma::Data()
    {
        float oneArray[4] = {1.0f, 0.0f, 0.0f, 0.0f};
        setQuaternion(oneArray);
    }

    QuaternionData::QuaternionData(const QuaternionData& source) :
            ma::Data(source)
    {
        setQuaternion(source.quaternion);
    }

    QuaternionData::QuaternionData(const float quaternion[]) : ma::Data()
    {
        setQuaternion(quaternion);
    }

    QuaternionData::~QuaternionData()
    {
    }

    void swap(QuaternionData& first, QuaternionData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.quaternion, second.quaternion);
    }

    QuaternionData& QuaternionData::operator=(QuaternionData source)
    {
        swap(*this, source);
        return *this;
    }

    void QuaternionData::getQuaternion(float quaternion[]) const
    {
        for (int i = 0; i < 4; i++) quaternion[i] = this->quaternion[i];
    }

    void QuaternionData::setQuaternion(const float quaternion[])
    {
        float norm = 0.0f;
        for (int i = 0; i < 4; i++) norm += quaternion[i] * quaternion[i];
        norm = sqrt(norm);
        for (int i = 0; i < 4; i++) this->quaternion[i] = quaternion[i]/norm;
    }

    DigitalIOData::DigitalIOData() : ma::Data()
    {
        setDirection(0);
        setState(0);
    }

    DigitalIOData::DigitalIOData(const DigitalIOData& source) : ma::Data(source)
    {
        direction = source.direction;
        state = source.state;
    }

    DigitalIOData::DigitalIOData(const unsigned char direction,
            const unsigned char state) :
            ma::Data(), direction(direction), state(state)
    {
    }

    DigitalIOData::~DigitalIOData()
    {
    }

    void swap(DigitalIOData& first, DigitalIOData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.direction, second.direction);
        std::swap(first.state, second.state);
    }

    DigitalIOData& DigitalIOData::operator=(DigitalIOData source)
    {
        swap(*this, source);
        return *this;
    }

    unsigned char DigitalIOData::getDirection() const
    {
        return direction;
    }

    unsigned char DigitalIOData::getState() const
    {
        return state;
    }

    void DigitalIOData::setDirection(const unsigned char direction)
    {
        this->direction = direction;
    }

    void DigitalIOData::setState(const unsigned char state)
    {
        this->state = state;
    }

    RawAnalogueInputData::RawAnalogueInputData() : ma::Data(),
            AX0(0), AX1(0), AX2(0), AX3(0), AX4(0), AX5(0), AX6(0), AX7(0)
    {
    }

    RawAnalogueInputData::RawAnalogueInputData(
            const RawAnalogueInputData& source) : ma::Data(source),
            AX0(source.AX0), AX1(source.AX1), AX2(source.AX2), AX3(source.AX3),
            AX4(source.AX4), AX5(source.AX5), AX6(source.AX6), AX7(source.AX7)
    {
    }

    RawAnalogueInputData::RawAnalogueInputData(
            const short AX0, const short AX1, const short AX2, const short AX3,
            const short AX4, const short AX5, const short AX6, const short AX7)
            : ma::Data(),
            AX0(AX0), AX1(AX1), AX2(AX2), AX3(AX3),
            AX4(AX4), AX5(AX5), AX6(AX6), AX7(AX7)
    {
    }

    RawAnalogueInputData::~RawAnalogueInputData()
    {
    }

    void swap(RawAnalogueInputData& first, RawAnalogueInputData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.AX0, second.AX0);
        std::swap(first.AX1, second.AX1);
        std::swap(first.AX2, second.AX2);
        std::swap(first.AX3, second.AX3);
        std::swap(first.AX4, second.AX4);
        std::swap(first.AX5, second.AX5);
        std::swap(first.AX6, second.AX6);
        std::swap(first.AX7, second.AX7);
    }

    RawAnalogueInputData& RawAnalogueInputData::operator=(
            RawAnalogueInputData source)
    {
        swap(*this, source);
        return *this;
    }

    short RawAnalogueInputData::getAX0() const
    {
        return AX0;
    }

    short RawAnalogueInputData::getAX1() const
    {
        return AX1;
    }

    short RawAnalogueInputData::getAX2() const
    {
        return AX2;
    }

    short RawAnalogueInputData::getAX3() const
    {
        return AX3;
    }

    short RawAnalogueInputData::getAX4() const
    {
        return AX4;
    }

    short RawAnalogueInputData::getAX5() const
    {
        return AX5;
    }

    short RawAnalogueInputData::getAX6() const
    {
        return AX6;
    }

    short RawAnalogueInputData::getAX7() const
    {
        return AX7;
    }

    void RawAnalogueInputData::getAX(short axData[]) const
    {
        axData[0] = AX0;
        axData[1] = AX1;
        axData[2] = AX2;
        axData[3] = AX3;
        axData[4] = AX4;
        axData[5] = AX5;
        axData[6] = AX6;
        axData[7] = AX7;
    }

    void RawAnalogueInputData::setAX0(const short AX0)
    {
        this->AX0 = AX0;
    }

    void RawAnalogueInputData::setAX1(const short AX1)
    {
        this->AX1 = AX1;
    }

    void RawAnalogueInputData::setAX2(const short AX2)
    {
        this->AX2 = AX2;
    }

    void RawAnalogueInputData::setAX3(const short AX3)
    {
        this->AX3 = AX3;
    }

    void RawAnalogueInputData::setAX4(const short AX4)
    {
        this->AX4 = AX4;
    }

    void RawAnalogueInputData::setAX5(const short AX5)
    {
        this->AX5 = AX5;
    }

    void RawAnalogueInputData::setAX6(const short AX6)
    {
        this->AX6 = AX6;
    }

    void RawAnalogueInputData::setAX7(const short AX7)
    {
        this->AX7 = AX7;
    }

    void RawAnalogueInputData::setAX(
            const short AX0, const short AX1, const short AX2, const short AX3,
            const short AX4, const short AX5, const short AX6, const short AX7)
    {
        setAX0(AX0);
        setAX1(AX1);
        setAX2(AX2);
        setAX3(AX3);
        setAX4(AX4);
        setAX5(AX5);
        setAX6(AX6);
        setAX7(AX7);
    }

    CalAnalogueInputData::CalAnalogueInputData() : ma::Data(),
            AX0(0), AX1(0), AX2(0), AX3(0), AX4(0), AX5(0), AX6(0), AX7(0)
    {
    }

    CalAnalogueInputData::CalAnalogueInputData(
            const CalAnalogueInputData& source) : ma::Data(source),
            AX0(source.AX0), AX1(source.AX1), AX2(source.AX2), AX3(source.AX3),
            AX4(source.AX4), AX5(source.AX5), AX6(source.AX6), AX7(source.AX7)
    {
    }

    CalAnalogueInputData::CalAnalogueInputData(
            const float AX0, const float AX1, const float AX2, const float AX3,
            const float AX4, const float AX5, const float AX6, const float AX7)
            : ma::Data(),
            AX0(AX0), AX1(AX1), AX2(AX2), AX3(AX3),
            AX4(AX4), AX5(AX5), AX6(AX6), AX7(AX7)
    {
    }

    CalAnalogueInputData::~CalAnalogueInputData()
    {
    }

    void swap(CalAnalogueInputData& first, CalAnalogueInputData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.AX0, second.AX0);
        std::swap(first.AX1, second.AX1);
        std::swap(first.AX2, second.AX2);
        std::swap(first.AX3, second.AX3);
        std::swap(first.AX4, second.AX4);
        std::swap(first.AX5, second.AX5);
        std::swap(first.AX6, second.AX6);
        std::swap(first.AX7, second.AX7);
    }

    CalAnalogueInputData& CalAnalogueInputData::operator=(
            CalAnalogueInputData source)
    {
        swap(*this, source);
        return *this;
    }

    float CalAnalogueInputData::getAX0() const
    {
        return AX0;
    }

    float CalAnalogueInputData::getAX1() const
    {
        return AX1;
    }

    float CalAnalogueInputData::getAX2() const
    {
        return AX2;
    }

    float CalAnalogueInputData::getAX3() const
    {
        return AX3;
    }

    float CalAnalogueInputData::getAX4() const
    {
        return AX4;
    }

    float CalAnalogueInputData::getAX5() const
    {
        return AX5;
    }

    float CalAnalogueInputData::getAX6() const
    {
        return AX6;
    }

    float CalAnalogueInputData::getAX7() const
    {
        return AX7;
    }

    void CalAnalogueInputData::getAX(float axData[]) const
    {
        axData[0] = AX0;
        axData[1] = AX1;
        axData[2] = AX2;
        axData[3] = AX3;
        axData[4] = AX4;
        axData[5] = AX5;
        axData[6] = AX6;
        axData[7] = AX7;
    }

    void CalAnalogueInputData::setAX0(const float AX0)
    {
        this->AX0 = AX0;
    }

    void CalAnalogueInputData::setAX1(const float AX1)
    {
        this->AX1 = AX1;
    }

    void CalAnalogueInputData::setAX2(const float AX2)
    {
        this->AX2 = AX2;
    }

    void CalAnalogueInputData::setAX3(const float AX3)
    {
        this->AX3 = AX3;
    }

    void CalAnalogueInputData::setAX4(const float AX4)
    {
        this->AX4 = AX4;
    }

    void CalAnalogueInputData::setAX5(const float AX5)
    {
        this->AX5 = AX5;
    }

    void CalAnalogueInputData::setAX6(const float AX6)
    {
        this->AX6 = AX6;
    }

    void CalAnalogueInputData::setAX7(const float AX7)
    {
        this->AX7 = AX7;
    }

    void CalAnalogueInputData::setAX(
            const float AX0, const float AX1, const float AX2, const float AX3,
            const float AX4, const float AX5, const float AX6, const float AX7)
    {
        setAX0(AX0);
        setAX1(AX1);
        setAX2(AX2);
        setAX3(AX3);
        setAX4(AX4);
        setAX5(AX5);
        setAX6(AX6);
        setAX7(AX7);
    }

    PWMOutputData::PWMOutputData() : ma::Data(), AX0(0), AX2(0), AX4(0), AX6(0)
    {
    }

    PWMOutputData::PWMOutputData(const PWMOutputData& source) :
            ma::Data(source),
            AX0(source.AX0), AX2(source.AX2), AX4(source.AX4), AX6(source.AX6)
    {
    }

    PWMOutputData::PWMOutputData(
            const unsigned short AX0, const unsigned short AX2,
            const unsigned short AX4, const unsigned short AX6) : ma::Data(),
            AX0(AX0), AX2(AX2), AX4(AX4), AX6(AX6)
    {
    }

    PWMOutputData::~PWMOutputData()
    {
    }

    void swap(PWMOutputData& first, PWMOutputData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.AX0, second.AX0);
        std::swap(first.AX2, second.AX2);
        std::swap(first.AX4, second.AX4);
        std::swap(first.AX6, second.AX6);
    }

    PWMOutputData& PWMOutputData::operator=(PWMOutputData source)
    {
        swap(*this, source);
        return *this;
    }

    unsigned short PWMOutputData::getAX0() const
    {
        return AX0;
    }

    unsigned short PWMOutputData::getAX2() const
    {
        return AX2;
    }

    unsigned short PWMOutputData::getAX4() const
    {
        return AX4;
    }

    unsigned short PWMOutputData::getAX6() const
    {
        return AX6;
    }

    void PWMOutputData::setAX0(const unsigned short AX0)
    {
        this->AX0 = AX0;
    }

    void PWMOutputData::setAX2(const unsigned short AX2)
    {
        this->AX2 = AX2;
    }

    void PWMOutputData::setAX4(const unsigned short AX4)
    {
        this->AX4 = AX4;
    }

    void PWMOutputData::setAX6(const unsigned short AX6)
    {
        this->AX6 = AX6;
    }

    void PWMOutputData::setAX(
            const unsigned short AX0, const unsigned short AX2,
            const unsigned short AX4, const unsigned short AX6)
    {
        setAX0(AX0);
        setAX2(AX2);
        setAX4(AX4);
        setAX6(AX6);
    }

    RawADXL345BusData::RawADXL345BusData() : ma::Data()
    {
        short ADXL345A[3] = {0, 0, 0};
        short ADXL345B[3] = {0, 0, 0};
        short ADXL345C[3] = {0, 0, 0};
        short ADXL345D[3] = {0, 0, 0};
        setADXL345(ADXL345A, ADXL345B, ADXL345C, ADXL345D);
    }

    RawADXL345BusData::RawADXL345BusData(
            const RawADXL345BusData& source) : ma::Data(source)
    {
        setADXL345A(source.ADXL345A);
        setADXL345B(source.ADXL345B);
        setADXL345C(source.ADXL345C);
        setADXL345D(source.ADXL345D);
    }

    RawADXL345BusData::RawADXL345BusData(
            const short ADXL345A[], const short ADXL345B[],
            const short ADXL345C[], const short ADXL345D[]) : ma::Data()
    {
        setADXL345(ADXL345A, ADXL345B, ADXL345C, ADXL345D);
    }

    RawADXL345BusData::~RawADXL345BusData()
    {
    }

    void swap(RawADXL345BusData& first, RawADXL345BusData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.ADXL345A, second.ADXL345A);
        std::swap(first.ADXL345B, second.ADXL345B);
        std::swap(first.ADXL345C, second.ADXL345C);
        std::swap(first.ADXL345D, second.ADXL345D);
    }

    RawADXL345BusData& RawADXL345BusData::operator=(
            RawADXL345BusData source)
    {
        swap(*this, source);
        return *this;
    }

    void RawADXL345BusData::getADXL345A(short ADXL345A[]) const
    {
        for (int i = 0; i < 3; i++) ADXL345A[i] = this->ADXL345A[i];
    }

    void RawADXL345BusData::getADXL345B(short ADXL345B[]) const
    {
        for (int i = 0; i < 3; i++) ADXL345B[i] = this->ADXL345B[i];
    }

    void RawADXL345BusData::getADXL345C(short ADXL345C[]) const
    {
        for (int i = 0; i < 3; i++) ADXL345C[i] = this->ADXL345C[i];
    }

    void RawADXL345BusData::getADXL345D(short ADXL345D[]) const
    {
        for (int i = 0; i < 3; i++) ADXL345D[i] = this->ADXL345D[i];
    }

    void RawADXL345BusData::setADXL345A(const short ADXL345A[])
    {
        for (int i = 0; i < 3; i++) this->ADXL345A[i] = ADXL345A[i];
    }

    void RawADXL345BusData::setADXL345B(const short ADXL345B[])
    {
        for (int i = 0; i < 3; i++) this->ADXL345A[i] = ADXL345A[i];
    }

    void RawADXL345BusData::setADXL345C(const short ADXL345C[])
    {
        for (int i = 0; i < 3; i++) this->ADXL345A[i] = ADXL345A[i];
    }

    void RawADXL345BusData::setADXL345D(const short ADXL345D[])
    {
        for (int i = 0; i < 3; i++) this->ADXL345A[i] = ADXL345A[i];
    }

    void RawADXL345BusData::setADXL345(
            const short ADXL345A[], const short ADXL345B[],
            const short ADXL345C[], const short ADXL345D[])
    {
        setADXL345A(ADXL345A);
        setADXL345B(ADXL345B);
        setADXL345C(ADXL345C);
        setADXL345D(ADXL345D);
    }

    CalADXL345BusData::CalADXL345BusData() : ma::Data()
    {
        float zeroArray[3] = {0, 0, 0};
        setADXL345(zeroArray, zeroArray, zeroArray, zeroArray);
    }

    CalADXL345BusData::CalADXL345BusData(const CalADXL345BusData& source) :
            ma::Data(source)
    {
        setADXL345A(source.ADXL345A);
        setADXL345B(source.ADXL345B);
        setADXL345C(source.ADXL345C);
        setADXL345D(source.ADXL345D);
    }

    CalADXL345BusData::CalADXL345BusData(
            const float ADXL345A[], const float ADXL345B[],
            const float ADXL345C[], const float ADXL345D[]) : ma::Data()
    {
        setADXL345(ADXL345A, ADXL345B, ADXL345C, ADXL345D);
    }

    CalADXL345BusData::~CalADXL345BusData()
    {
    }

    void swap(CalADXL345BusData& first, CalADXL345BusData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        std::swap(first.ADXL345A, second.ADXL345A);
        std::swap(first.ADXL345B, second.ADXL345B);
        std::swap(first.ADXL345C, second.ADXL345C);
        std::swap(first.ADXL345D, second.ADXL345D);
    }

    CalADXL345BusData& CalADXL345BusData::operator=(
            CalADXL345BusData source)
    {
        swap(*this, source);
        return *this;
    }

    void CalADXL345BusData::getADXL345A(float ADXL345A[]) const
    {
        for (int i = 0; i < 3; i++) ADXL345A[i] = this->ADXL345A[i];
    }

    void CalADXL345BusData::getADXL345B(float ADXL345B[]) const
    {
        for (int i = 0; i < 3; i++) ADXL345B[i] = this->ADXL345B[i];
    }

    void CalADXL345BusData::getADXL345C(float ADXL345C[]) const
    {
        for (int i = 0; i < 3; i++) ADXL345C[i] = this->ADXL345C[i];
    }

    void CalADXL345BusData::getADXL345D(float ADXL345D[]) const
    {
        for (int i = 0; i < 3; i++) ADXL345D[i] = this->ADXL345D[i];
    }

    void CalADXL345BusData::setADXL345A(const float ADXL345A[])
    {
        for (int i = 0; i < 3; i++) this->ADXL345A[i] = ADXL345A[i];
    }

    void CalADXL345BusData::setADXL345B(const float ADXL345B[])
    {
        for (int i = 0; i < 3; i++) this->ADXL345A[i] = ADXL345A[i];
    }

    void CalADXL345BusData::setADXL345C(const float ADXL345C[])
    {
        for (int i = 0; i < 3; i++) this->ADXL345A[i] = ADXL345A[i];
    }

    void CalADXL345BusData::setADXL345D(const float ADXL345D[])
    {
        for (int i = 0; i < 3; i++) this->ADXL345A[i] = ADXL345A[i];
    }

    void CalADXL345BusData::setADXL345(
            const float ADXL345A[], const float ADXL345B[],
            const float ADXL345C[], const float ADXL345D[])
    {
        setADXL345A(ADXL345A);
        setADXL345B(ADXL345B);
        setADXL345C(ADXL345C);
        setADXL345D(ADXL345D);
    }

    short floatToShort(const float floatValue, const QValue qValue)
    {
        int intValue = (int)(floatValue * (1 << (int)qValue));
        if (intValue > 32767) intValue = 32767;
        else if (intValue < -32768) intValue = -32768;
        return (short)intValue;
    }

    float shortToFloat(const short shortValue, const QValue qValue)
    {
        return ((float)shortValue) / (1 << (int)qValue);
    }
}
