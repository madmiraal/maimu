// (c) 2018 - 2019: Marcel Admiraal

#include "nufileapi.h"

namespace nu
{
    FileAPI::FileAPI(unsigned int imu) :
        API(), lastTime(std::chrono::steady_clock::now()), imu(imu),
        sendRegisterData(false)
    {
        wxMutexLocker fileSerialLock(fileSerialAccess);
        fileSerial = new ma::FileSerial();
        ma::Serial* old = serial;
        serial = fileSerial;
        delete old;
        if (!registerData.loadData(
                ma::Str("registers" + ma::toStr(imu) + ".dat")))
            setDefaultRegisters();
        setPeriod();
    }

    FileAPI::~FileAPI()
    {
    }

    bool FileAPI::setPort(const ma::Str& fileName)
    {
        resetFileSerialTimer();
        wxMutexLocker fileSerialLock(fileSerialAccess);
        return fileSerial->setPort(fileName);
    }

    ma::FileSerial* FileAPI::setFileSerial(ma::FileSerial* newFileSerial)
    {
        wxMutexLocker fileSerialLock(fileSerialAccess);
        ma::FileSerial* oldFileSerial = fileSerial;
        fileSerial = newFileSerial;
        serial = fileSerial;
        return oldFileSerial;
    }

    void FileAPI::resetFileSerialTimer()
    {
        lastTime = std::chrono::steady_clock::now();
    }

    bool FileAPI::getRegisters()
    {
        sendRegisterData = true;
        return true;
    }

    bool FileAPI::setRegisters(ma::Data* data)
    {
        ma::Vector vectorData = data->getVectorData();

        // Check for valid data.
        if (data->getDataType() != ma::DataType::RegisterData) return false;
        if (vectorData.size() != 21) return false;

        this->registerData = RegisterData(vectorData);
        setPeriod();
        return true;
    }

    bool FileAPI::setRegister(unsigned int registerNumber, short registerValue)
    {
        bool success = registerData.setRegister(registerNumber, registerValue);
        if (success)
        {
            if (registerNumber == 2 || registerNumber == 4) setPeriod();
            return true;
        }
        return false;
    }

    bool FileAPI::saveRegistersToFlash()
    {
        ma::Str filename("registers" + ma::toStr(imu) + ".dat");
        std::ofstream fileStream(filename.c_str(), std::ofstream::out);
        if (fileStream.is_open())
        {
            fileStream << registerData << std::endl;
            return true;
        }
        return false;
    }

    ma::Data* FileAPI::getNextData()
    {
        if (sendRegisterData)
        {
            sendRegisterData = false;
            return new RegisterData(registerData);
        }
        return 0;
    }

    unsigned long FileAPI::readData(unsigned char *buffer,
            const unsigned int bufferLength)
    {
        wxMutexLocker fileSerialLock(fileSerialAccess);
        return fileSerial->readData(buffer, bufferLength);
    }

    unsigned long FileAPI::readDoubleData(double *buffer,
            const unsigned int bufferLength)
    {
        wxMutexLocker fileSerialLock(fileSerialAccess);
        return fileSerial->readDoubleData(buffer, bufferLength);
    }

    void FileAPI::setDefaultRegisters()
    {
        short value[] = {
            8,                   //  1 - id
            30000,               //  3 - Timer period count.
            0,                   //  5 - Frequency: (f + 1) * 2^14
            255,                 //  7 - ADC channel bit-filter
            0,                   //  9 - Timer pre-Scaler: 0:1, 1:8, 2:64, 3:256
            G_SCALE_500DPS,      // 11 - Gyroscope scale
            G_ODR_760_BW_100,    // 13 - Gyroscope rate
            A_SCALE_2G,          // 15 - Accelerometer scale
            A_ODR_800,           // 17 - Accelerometer rate
            M_SCALE_2GS,         // 19 - Magnetometer scale
            M_ODR_100,           // 21 - Magnetometer rate
            0,                   // 23 - Magnetometer X offset
            0,                   // 25 - Magnetometer Y offset
            0,                   // 27 - Magnetometer Z offset
            0,                   // 29 - Accelerometer X offset
            0,                   // 31 - Accelerometer Y offset
            0,                   // 33 - Accelerometer Z offset
            0,                   // 35 - Gyroscope X offset
            0,                   // 37 - Gyroscope Y offset
            0,                   // 39 - Gyroscope Z offset
            firmwareVersion,     // 41 - Firmware version
            6                    // 43 - Alpha inverse log
        };
        registerData.setVectorData(ma::Vector(value, 21));
    }

    void FileAPI::setPeriod()
    {
        period = 1000.0 / registerData.getSampleFrequency();
    }
}
