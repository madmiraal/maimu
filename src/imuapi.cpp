// (c) 2016: Marcel Admiraal

#include "imuapi.h"

namespace ma
{
    IMUAPI::IMUAPI() : serial(new Serial())
    {
        CreateThread(wxTHREAD_JOINABLE);
        running = true;
        GetThread()->Run();
    }

    IMUAPI::~IMUAPI()
    {
        running = false;
        GetThread()->Wait();
        delete serial;
    }

    Str IMUAPI::getPort()
    {
        return serial->getPortString();
    }

	bool IMUAPI::setPort(const Str& portName)
	{
        return (
            serial->setPort(portName) &&
            setParameters()
        );
	}

	bool IMUAPI::setParameters(
		const int baud,
		const int bits,
		const int stopBits,
		const int parity,
		const bool block
		)
    {
        return serial->setParameters(baud, bits, stopBits, parity, block);
    }

    bool IMUAPI::isConnected()
    {
        return serial->isConnected();
    }

    unsigned char IMUAPI::readByte()
    {
        unsigned char byte = '\0';
        serial->readData(&byte, 1);
        return byte;
    }

    bool IMUAPI::readBytes(unsigned char *byteArray,
            const unsigned int arrayLength)
    {
        return (serial->readData(byteArray, arrayLength) == arrayLength);
    }

    bool IMUAPI::sendPacket(unsigned char *packet, unsigned int packetLength)
    {
        unsigned int bytesWritten = serial->writeData(packet, packetLength);
        return (bytesWritten == packetLength);
    }

    void IMUAPI::clearReadBuffer()
    {
        serial->clearReadBuffer();
    }

    void IMUAPI::clearWriteBuffer()
    {
        serial->clearWriteBuffer();
    }

    void IMUAPI::addListener(IMUListener* listener)
    {
        listenerCollection.add(listener);
    }

    void IMUAPI::removeListener(IMUListener* listener)
    {
        listenerCollection.remove(listener);
    }

    ma::Serial* IMUAPI::getSerial()
    {
        return serial;
    }

    wxThread::ExitCode IMUAPI::Entry()
    {
        while (running)
        {
            if (isConnected())
            {
                Data* data = getNextData();
                if (data)
                {
                    informListeners(data);
                    delete data;
                }
            }
        }
        return (wxThread::ExitCode)0;
    }

    void IMUAPI::informListeners(Data* data)
    {
        for (Iterator<IMUListener*> iter = listenerCollection.getIterator();
                iter.valid(); --iter)
        {
            (*iter)->dataReceived(data);
        }
    }
}
