// (c) 2018 - 2019: Marcel Admiraal

#include "nufilerawapi.h"

#include "fileserial.h"

namespace nu
{
    FileRawAPI::FileRawAPI(unsigned int imu) :
        FileAPI(imu), signalData(0), remainingTime(0)
    {
    }

    FileRawAPI::~FileRawAPI()
    {
    }

    ma::Data* FileRawAPI::getNextData()
    {
        ma::Data* data = 0;
        if ((data = FileAPI::getNextData())) return data;
        if ((data = getSignalData())) return data;

        unsigned int packetLength = 256;
        unsigned char packet[packetLength];
        getPacket(packet, packetLength);
        if (packetLength > 0)
        {
            if (packet[0] == imu)
            {
                data = extractIMUData(&packet[1], packetLength-2);
                adjustIMUData(data);
                adjustAnalogueData(data);
            }
            if (data->getDataType() != ma::DataType::MessageData)
            {
                ma::Vector signal(1);
                signal[0] = (unsigned int)packet[packetLength - 1];
                signalData = new ma::Data(DataType::NU_CUSTOM_START, signal);
            }
        }

        return data;
    }

    ma::Data* FileRawAPI::getSignalData()
    {
        if (signalData != 0)
        {
            ma::Data* data = signalData;
            signalData = 0;
            return data;
        }
        return 0;
    }

    void FileRawAPI::getPacket(unsigned char* packet, unsigned int& packetLength)
    {
        unsigned int currentPeriod = 1000.0 / registerData.getSampleFrequency();
        std::chrono::steady_clock::time_point thisTime =
                std::chrono::steady_clock::now();
        std::chrono::duration<double> timePassed =
                std::chrono::duration_cast<std::chrono::duration<double>>
                (thisTime - lastTime);
        if ((timePassed.count() + remainingTime) * 1000 > currentPeriod)
        {
            remainingTime = ((timePassed.count() + remainingTime) * 1000 -
                    currentPeriod) / 1000;
            if (remainingTime > currentPeriod) remainingTime = 0;
            lastTime = thisTime;
            packetLength = readData(packet, packetLength);
        }
        else
        {
            packetLength = 0;
        }
    }
}
