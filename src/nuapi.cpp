// (c) 2016 - 2017: Marcel Admiraal

#include "nuapi.h"

#include "nudata.h"
#include "strstream.h"
#include "utils.h"

namespace nu
{
    API::API() : ma::IMUAPI(), analogueBits(DEFAULT_ANALOG_BITS)
    {
    }

    API::~API()
    {
    }

    bool API::sendCommand(const ma::Str& command)
    {
        unsigned int commandLength = command.length();
        unsigned char packet[commandLength + 2];
        for (unsigned int i = 0; i < commandLength; ++i)
        {
            packet[i+1] = (unsigned char)command[i];
        }
        packet[0] = 0x07;
        packet[commandLength + 1] = 0x0B;
        return sendPacket(packet, commandLength + 2);
    }

    bool API::stopSending()
    {
        return sendCommand("M0dg");
    }

    bool API::startSendingIMUData()
    {
        return sendCommand("M4dg");
    }

    bool API::startSendingQuaternions()
    {
        return sendCommand("M7dg");
    }

    bool API::testDevice()
    {
        return sendCommand("Taag");
    }

    bool API::getRegisters()
    {
        return sendCommand("RegR");
    }

    bool API::setRegisters(ma::Data* data)
    {
        ma::Vector vectorData = data->getVectorData();

        // Check for valid data.
        if (data->getDataType() != ma::DataType::RegisterData) return false;
        if (vectorData.size() != 21) return false;

        // Keep a copy.
        registerData = RegisterData(vectorData);

        unsigned char packet[6];
        for (unsigned int i = 0; i < 21; ++i)
        {
            packet[0] = 0x07;
            packet[1] = (unsigned char)'C';
            packet[2] = (unsigned char)(i * 2 + 1);
            packet[3] = (unsigned char)(((short)(vectorData[i])) >> 8);
            packet[4] = (unsigned char)(((short)(vectorData[i])) & 255);
            packet[5] = 0x0B;
            if (!sendPacket(packet, 6)) return false;
        }
        return true;
    }

    bool API::setRegister(unsigned int registerNumber, short registerValue)
    {
        // Keep a copy.
        if (!registerData.setRegister(registerNumber, registerValue))
            return false;

        // Update local copy.
        ma::Vector registerVector = registerData.getVectorData();
        registerVector[registerNumber] = registerValue;
        registerData.setVectorData(registerVector);

        unsigned char packet[6];
        packet[0] = 0x07;
        packet[1] = 'C';
        packet[2] = (unsigned char)registerNumber;
        packet[3] = (unsigned char)(registerValue >> 8);
        packet[4] = (unsigned char)(registerValue & 255);
        packet[5] = 0x0B;
        return sendPacket(packet, 6);
    }

    bool API::saveRegistersToFlash()
    {
        unsigned char packet[6];
        packet[0] = 0x07;
        packet[1] = 'C';
        packet[2] = 0xDD;
        packet[3] = 0xDD;
        packet[4] = 0xDD;
        packet[5] = 0x0B;
        return sendPacket(packet, 6);
    }

    unsigned int API::getAnalogueBits() const
    {
        return analogueBits;
    }

    void API::setAnalogueBits(const unsigned int bits)
    {
        analogueBits = bits;
        if (analogueBits > 15) analogueBits = 15;
    }

    double API::getAccelerometerScale()
    {
        return accelerometerScale[registerData.getAccelerometerScale()];
    }

    double API::getGyroscopeScale()
    {
        return gyroscopeScale[registerData.getGyroscopeScale()];
    }

    double API::getMagnetometerScale()
    {
        return magnetometerScale[registerData.getMagnetometerScale()];
    }

    ma::Data* API::getNextData()
    {
        unsigned int packetLength = 256;
        unsigned char packet[packetLength];
        getPacket(packet, packetLength);
        if (packetLength > 0)
            return extractData(packet, packetLength);
        else
            return 0;
    }

    void API::getPacket(unsigned char* packet,
            unsigned int& packetLength)
    {
        packetLength = 0;
        packet[0] = '\0';
        packetLength = getSerial()->readData(packet, 1);

        // Check whether bytes available.
        if (packetLength == 0) return;

        // Drop byte if it is incorrect.
        if (packet[0] != 0xDD)
        {
            packetLength = 0;
            return;
        }

        // Retrieve full header and length byte.
        while (packetLength < 4)
        {
            unsigned int bytesRead = getSerial()->readData(
                    &packet[packetLength], 4 - packetLength);
            if (bytesRead == 0)
            {
                packetLength = 0;
                return;
            }
            packetLength += bytesRead;
        }
        if (packet[0] != 0xDD || packet[1] != 0xAA || packet[2] != 0x55)
        {
            packetLength = 0;
            return;
        }

        // Store length and reset packet length.
        unsigned int length = (unsigned int)packet[3];
        packetLength = 0;

        // Keep reading bytes until a full packet is received.
        while (packetLength != length)
        {
            unsigned int bytesRead = getSerial()->readData(
                    &packet[packetLength], length - packetLength);
            if (bytesRead == 0)
            {
                packetLength = 0;
                return;
            }
            packetLength += bytesRead;
        }
        // Check that packet length bytes match.
        if (packetLength == 0 || packet[packetLength-1] != length)
        {
            packetLength = 0;
            return;
        }
        // Remove tail byte.
        --packetLength;
    }

    ma::Data* API::extractData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        ma::Data* data;
        switch (packet[0])
        {
        case 0:
            data = extractMessageData(&packet[1], packetLength-1);
            break;
        case 1:
            data = extractNumberData(&packet[1], packetLength-1);
            break;
        case 4:
            data = receivedFinalData(&packet[1], packetLength-1);
            break;
        case 6:
            data = extractIMUData(&packet[1], packetLength-1);
            adjustIMUData(data);
            adjustAnalogueData(data);
            break;
        case 8:
            data = extractQuaternionData(&packet[1], packetLength-1);
            adjustAnalogueData(data);
            break;
        case 9:
            data = extractRegisterData(&packet[1], packetLength-1);
            copyRegisterData(data);
            break;
        default:
            ma::Str message = "Unrecognised data type: " +
                    ma::toStr((int)packet[0]);
            data = new ma::Data(ma::DataType::MessageData, message);
        }
        return data;
    }

    void API::copyRegisterData(ma::Data* data)
    {
        RegisterData* thisRegisterData = dynamic_cast<RegisterData*>(data);
        if (thisRegisterData)
        {
            registerData = *thisRegisterData;
        }
    }

    void API::adjustIMUData(ma::Data* data)
    {
        IMUData* thisIMUData = dynamic_cast<IMUData*>(data);
        if (thisIMUData)
        {
            thisIMUData->setAccelerometerData(
                    thisIMUData->getAccelerometerData() *
                    getAccelerometerScale());
            thisIMUData->setGyroscopeData(
                    thisIMUData->getGyroscopeData() *
                    getGyroscopeScale());
            thisIMUData->setMagnetometerData(
                    thisIMUData->getMagnetometerData() *
                    getMagnetometerScale());
        }
    }

    void API::adjustAnalogueData(ma::Data* data)
    {
        AnalogueData* thisAnalogueData = dynamic_cast<AnalogueData*>(data);
        if (thisAnalogueData)
        {
            thisAnalogueData->setAnalogueData(
                    thisAnalogueData->getAnalogueData() *
                    (1 << (15 - analogueBits)));
        }
    }

    ma::Data* extractMessageData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        ma::StrStream message;
        for (unsigned int i = 0; i < packetLength; ++i)
        {
            message << (char)packet[i];
        }
        ma::Data* data = new ma::Data(ma::DataType::MessageData, message.toStr());
        return data;
    }

    ma::Data* extractNumberData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        ma::StrStream message;
        message << (int)packet[0] << " ";
        message << (char)packet[0] << " ";
        message << ma::toHex((int)packet[0]) << " ";
        message << ma::toBinary((int)packet[0]);
        ma::Data* data = new ma::Data(ma::DataType::MessageData, message.toStr());
        return data;
    }

    ma::Data* extractIMUData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        // Analogue data only.
        if (packetLength <= 16)
        {
            // Convert byte array to short array.
            short analogueShort[8] = {0};
            byteToShort(&packet[0], analogueShort, packetLength / 2, true);

            // Create data object.
            AnalogueData* data = new AnalogueData(ma::Vector(analogueShort, 8));
            return data;
        }
        // IMU and analogue data.
        else if (packetLength >= 18 && packetLength <= 34)
        {
            // Convert byte arrays to short arrays.
            short gyroscopeShort[3] = {0};
            short magnetometerShort[3] = {0};
            short accelerometerShort[3] = {0};
            short analogueShort[8] = {0};
            byteToShort(&packet[0], gyroscopeShort, 3);
            byteToShort(&packet[6], magnetometerShort, 3);
            byteToShort(&packet[12], accelerometerShort, 3);
            byteToShort(&packet[18], analogueShort, (packetLength - 18) / 2, true);

            // Create data object.
            IMUAnalogueData* data = new IMUAnalogueData(
                    ma::Vector(accelerometerShort, 3) / (1 << 15),
                    ma::Vector(gyroscopeShort, 3) / (1 << 15),
                    ma::Vector(magnetometerShort, 3) / (1 << 15),
                    ma::Vector(analogueShort, 8) / (1 << 15));
            return data;
        }
        else
        {
            ma::Str message = "IMU data with unrecognised packet length: "
                    + ma::toStr(packetLength);
            ma::Data* data = new ma::Data(ma::DataType::MessageData, message);
            return data;
        }
    }

    ma::Data* extractQuaternionData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength >= 8 && packetLength <= 24)
        {
            // Convert byte arrays to short arrays.
            short quaternionShort[4] = {0};
            short analogueShort[8] = {0};
            byteToShort(&packet[0], quaternionShort, 4, true);
            byteToShort(&packet[8], analogueShort, (packetLength - 8) / 2, true);

            // Crate data object.
            QuaternionAnalogueData* data = new QuaternionAnalogueData(
                    ma::Vector(quaternionShort, 4).normalise(),
                    ma::Vector(analogueShort, 8) / (1 << 15));
            return data;
        }
        else
        {
            ma::Str message = "Quaternion data with unrecognised packet length: "
                    + ma::toStr(packetLength);
            ma::Data* data = new ma::Data(ma::DataType::MessageData, message);
            return data;
        }
    }

    ma::Data* extractRegisterData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        if (packetLength != 36)
        {
            ma::Str message = "Register data with unrecognised packet length: "
                    + ma::toStr(packetLength);
            ma::Data* data = new ma::Data(ma::DataType::MessageData, message);
            return data;
        }
        short registerShort[21];
        byteToShort(&packet[0], &registerShort[0], 5, true);
        // The next six values are single bytes only!
        for (unsigned int i = 0; i < 6; ++i) registerShort[5+i] = packet[10+i];
        byteToShort(&packet[16], &registerShort[11], 10, true);
        RegisterData* data = new RegisterData(ma::Vector(registerShort, 21));

        return data;
    }

    ma::Data* receivedFinalData(const unsigned char* packet,
            const unsigned int packetLength)
    {
        ma::Str message;
        if (packetLength == 1 && packet[0] == 0x55)
        {
            message = "Final data received.";
        }
        else
        {
            message = "Unknown final data packet received.";
        }
        ma::Data* data = new ma::Data(ma::DataType::MessageData, message);
        return data;
    }
}
