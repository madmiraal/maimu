// (c) 2018 - 2019: Marcel Admiraal

#include "nufiledataapi.h"

#include "fileserial.h"
#include "nudata.h"

#define MAXROWLENGTH 256

namespace nu
{
    FileDataAPI::FileDataAPI(unsigned int imu) :
        FileAPI(imu), waitPeriod(0), rowLength(0),
        rowValue(new double[MAXROWLENGTH]())
    {
    }

    FileDataAPI::~FileDataAPI()
    {
        delete rowValue;
    }

    ma::Data* FileDataAPI::getNextData()
    {
        ma::Data* data = 0;
        if ((data = FileAPI::getNextData())) return data;
        if (getRow())
            data = extractIMUAnalogueData(&rowValue[1], rowLength - 1);
        return data;
    }

    bool FileDataAPI::getRow()
    {
        std::chrono::steady_clock::time_point thisTime =
                std::chrono::steady_clock::now();
        std::chrono::milliseconds timePassed =
                std::chrono::duration_cast<std::chrono::milliseconds>
                (thisTime - lastTime);
        if (timePassed.count() > waitPeriod)
        {
            double lastTimeStamp = rowValue[0];
            rowLength = readDoubleData(rowValue, MAXROWLENGTH);
            if (rowLength == 0) return false;
            waitPeriod = ((rowValue[0] - lastTimeStamp) * 1e3)
                - (timePassed.count() - waitPeriod);
            //waitPeriod = 7;
            lastTime = thisTime;
            return true;
        }
        return false;
    }

    ma::Data* extractIMUAnalogueData(double* rowValue,
            const unsigned int rowLength)
    {
        ma::Vector accelerometerData;
        ma::Vector gyroscopeData;
        ma::Vector magnetometerData;
        ma::Vector analogueData;

        unsigned int rowIndex = 0;
        if (rowLength >= 3)
        {
            accelerometerData = ma::Vector(3);
            for (unsigned int accelerometerIndex = 0; accelerometerIndex < 3;
                    ++accelerometerIndex)
                accelerometerData[accelerometerIndex] = rowValue[rowIndex++];
        }
        if (rowLength >= 6)
        {
            gyroscopeData = ma::Vector(3);
            for (unsigned int gyroscopeIndex = 0; gyroscopeIndex < 3;
                    ++gyroscopeIndex)
                gyroscopeData[gyroscopeIndex] = rowValue[rowIndex++];
        }
        if (rowLength >= 9)
        {
            magnetometerData = ma::Vector(3);
            for (unsigned int magnetometerIndex = 0; magnetometerIndex < 3;
                    ++magnetometerIndex)
                magnetometerData[magnetometerIndex] = rowValue[rowIndex++];
        }
        if (rowLength > 9)
        {
            analogueData = ma::Vector(8);
            for (unsigned int analogueIndex = 0; analogueIndex < 8 &&
                    rowIndex < rowLength; ++analogueIndex)
                analogueData[analogueIndex] = rowValue[rowIndex++];
        }

        IMUAnalogueData* data = new IMUAnalogueData(accelerometerData,
                gyroscopeData, magnetometerData, analogueData);
        return data;
    }
}
