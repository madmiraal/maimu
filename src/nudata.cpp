// (c) 2016 - 2018: Marcel Admiraal

#include "nudata.h"

#include <algorithm>

namespace nu
{
    IMUData::IMUData() : ma::Data(DataType::IMUOnly)
    {
    }

    IMUData::IMUData(const IMUData& source) :
        ma::Data(source),
        accelerometerData(source.accelerometerData),
        gyroscopeData(source.gyroscopeData),
        magnetometerData(source.magnetometerData)
    {
    }

    IMUData::IMUData(
            const ma::Vector& accelerometerData,
            const ma::Vector& gyroscopeData,
            const ma::Vector& magnetometerData) :
            ma::Data(DataType::IMUOnly),
            accelerometerData(accelerometerData),
            gyroscopeData(gyroscopeData),
            magnetometerData(magnetometerData)
    {
    }

    IMUData::~IMUData()
    {
    }

    void swap(IMUData& first, IMUData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        swap(first.accelerometerData, second.accelerometerData);
        swap(first.gyroscopeData, second.gyroscopeData);
        swap(first.magnetometerData, second.magnetometerData);
    }

    IMUData& IMUData::operator=(IMUData source)
    {
        swap(*this, source);
        return *this;
    }

    ma::Vector IMUData::getAccelerometerData() const
    {
        return accelerometerData;
    }

    ma::Vector IMUData::getGyroscopeData() const
    {
        return gyroscopeData;
    }

    ma::Vector IMUData::getMagnetometerData() const
    {
        return magnetometerData;
    }

    void IMUData::setAccelerometerData(
            const ma::Vector& accelerometerData)
    {
        this->accelerometerData = accelerometerData;
    }

    void IMUData::setGyroscopeData(
            const ma::Vector& gyroscopeData)
    {
        this->gyroscopeData = gyroscopeData;
    }

    void IMUData::setMagnetometerData(
            const ma::Vector& magnetometerData)
    {
        this->magnetometerData = magnetometerData;
    }

    AnalogueData::AnalogueData() : ma::Data(DataType::AnalogueOnly)
    {
    }

    AnalogueData::AnalogueData(const ma::Vector& analogueData) :
            ma::Data(DataType::AnalogueOnly), analogueData(analogueData)
    {
    }

    AnalogueData::AnalogueData(const AnalogueData& source) :
        ma::Data(source), analogueData(source.analogueData)
    {
    }

    AnalogueData::~AnalogueData()
    {
    }

    void swap(AnalogueData& first, AnalogueData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        swap(first.analogueData, second.analogueData);
    }

    AnalogueData& AnalogueData::operator=(AnalogueData source)
    {
        swap(*this, source);
        return *this;
    }

    ma::Vector AnalogueData::getAnalogueData() const
    {
        return analogueData;
    }

    void AnalogueData::setAnalogueData(ma::Vector analogueData)
    {
        this->analogueData = analogueData;
    }

    QuaternionData::QuaternionData() : ma::Data(DataType::QuaternionOnly)
    {
    }

    QuaternionData::QuaternionData(const ma::Vector& quaternionData) :
        ma::Data(DataType::QuaternionOnly), quaternionData(quaternionData)
    {
    }

    QuaternionData::QuaternionData(const QuaternionData& source) :
        ma::Data(source), quaternionData(source.quaternionData)
    {
    }

    QuaternionData::~QuaternionData()
    {
    }

    void swap(QuaternionData& first, QuaternionData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
        swap(first.quaternionData, second.quaternionData);
    }

    QuaternionData& QuaternionData::operator=(QuaternionData source)
    {
        swap(*this, source);
        return *this;
    }

    ma::Vector QuaternionData::getQuaternionData() const
    {
        return quaternionData;
    }

    void QuaternionData::setQuaternionData(ma::Vector quaternionData)
    {
        this->quaternionData = quaternionData;
    }

    IMUAnalogueData::IMUAnalogueData() : IMUData(), AnalogueData()
    {
        setDataType(DataType::IMUAnalogue);
    }

    IMUAnalogueData::IMUAnalogueData(const IMUAnalogueData& source) :
        IMUData(source), AnalogueData(source)
    {
        setDataType(DataType::IMUAnalogue);
    }

    IMUAnalogueData::IMUAnalogueData(
            const ma::Vector& accelerometerData,
            const ma::Vector& gyroscopeData,
            const ma::Vector& magnetometerData,
            const ma::Vector& analogueData) :
        IMUData(accelerometerData, gyroscopeData, magnetometerData),
        AnalogueData(analogueData)
    {
        setDataType(DataType::IMUAnalogue);
    }

    IMUAnalogueData::~IMUAnalogueData()
    {
    }

    void swap(IMUAnalogueData& first, IMUAnalogueData& second)
    {
        swap(static_cast<IMUData&>(first),
                static_cast<IMUData&>(second));
        swap(static_cast<AnalogueData&>(first),
                static_cast<AnalogueData&>(second));
    }

    IMUAnalogueData& IMUAnalogueData::operator=(IMUAnalogueData source)
    {
        swap(*this, source);
        return *this;
    }

    QuaternionAnalogueData::QuaternionAnalogueData() :
        QuaternionData(), AnalogueData()
    {
        setDataType(DataType::QuaternionAnalogue);
    }

    QuaternionAnalogueData::QuaternionAnalogueData(
            const QuaternionAnalogueData& source) :
        QuaternionData(source), AnalogueData(source)
    {
        setDataType(DataType::QuaternionAnalogue);
    }

    QuaternionAnalogueData::QuaternionAnalogueData(
            const ma::Vector& quaternionData, const ma::Vector& analogueData) :
        QuaternionData(quaternionData), AnalogueData(analogueData)
    {
        setDataType(DataType::QuaternionAnalogue);
    }

    QuaternionAnalogueData::~QuaternionAnalogueData()
    {
    }

    void swap(QuaternionAnalogueData& first, QuaternionAnalogueData& second)
    {
        swap(static_cast<QuaternionData&>(first),
                static_cast<QuaternionData&>(second));
        swap(static_cast<AnalogueData&>(first),
                static_cast<AnalogueData&>(second));
    }

    QuaternionAnalogueData& QuaternionAnalogueData::operator=(
            QuaternionAnalogueData source)
    {
        swap(*this, source);
        return *this;
    }

    RegisterData::RegisterData() : ma::Data(ma::DataType::RegisterData,
            ma::Vector(21))
    {
    }

    RegisterData::RegisterData(const ma::Vector& registerData) :
        ma::Data(ma::DataType::RegisterData, registerData.subVector(0, 21))
    {
    }

    RegisterData::RegisterData(const RegisterData& source) :
        ma::Data(source)
    {
    }

    RegisterData::~RegisterData()
    {
    }

    void swap(RegisterData& first, RegisterData& second)
    {
        swap(static_cast<ma::Data&>(first), static_cast<ma::Data&>(second));
    }

    RegisterData& RegisterData::operator=(RegisterData source)
    {
        swap(*this, source);
        return *this;
    }

    bool RegisterData::setRegister(unsigned int registerNumber,
            short registerValue)
    {
        if (registerNumber > 43 || registerNumber % 2 == 0) return false;
        unsigned int vectorIndex = registerNumber / 2;
        vectorData[vectorIndex] = registerValue;
        return true;
    }

    short RegisterData::getPreScalerValue()
    {
        return getPreScalerValue(getPreScaler());
    }

    short RegisterData::getPreScalerValue(unsigned int index)
    {
        if (index > 3) return 0;
        return prescaler[index];
    }

    int RegisterData::getSampleFrequency()
    {
        // Oscillator frequency: Fosc = 32 MHz.
        // Instruction frequency: Fcy = Fosc/2 = 16 MHz
        // Sample frequency = Fcy / (PERIOD * PS)
        int sampleFrequency = FOSC / 2 /
            (getPeriod() * prescaler[getPreScaler()]);
        return sampleFrequency;
    }

    void RegisterData::setSampleFrequency(int sampleFrequency)
    {
        // Oscillator frequency: Fosc = 30 MHz.
        // Instruction frequency: Fcy = Fosc/2 = 15 MHz
        // For a timer frequency of tf Hz:
        // PR = Fcy / tf
        // If PR > 65535 user a pre-scaler PS (1, 8, 64 or 256)
        // PR = Fcy / (tf * PS)
        // Frequency (Hz) Period (ms)   PR	 PS
        //   100,000        0.01       160	  1
        //    50,000        0.02       320	  1
        //    20,000        0.05       800	  1
        //    10,000        0.1       1600	  1
        //     5,000        0.2       3200	  1
        //     2,000        0.5       8000	  1
        //     1,000        1        16000	  1
        //       500        2         4000 	  8
        //       200        5        10000	  8
        //       100       10        20000	  8
        //        50       20         5000	 64
        //        20       50        12500	 64
        //        10      100        25000	 64
        //         5      200        50000	 64
        //         2      500        31250	256
        //         1     1000        62500	256
        int Fcy = FOSC / 2;
        int PR = Fcy / sampleFrequency;
        int PSindex = 0;
        while (PR > 65535)
            PR = Fcy / sampleFrequency / getPreScalerValue(++PSindex);
        setPeriod(PR);
        setPreScaler(PSindex);
    }

    short RegisterData::getID()
    {
        return vectorData[0];
    }

    void RegisterData::setID(const short id)
    {
        vectorData[0] = id;
    }

    unsigned short RegisterData::getPeriod()
    {
        return vectorData[1];
    }

    void RegisterData::setPeriod(const unsigned short period)
    {
        vectorData[1] = period;
    }

    short RegisterData::getSkip()
    {
        return vectorData[2];
    }

    void RegisterData::setSkip(const short skip)
    {
        vectorData[2] = skip;
    }

    short RegisterData::getADCChannelFilter()
    {
        return vectorData[3];
    }

    void RegisterData::setADCChannelFilter(const short bitFilter)
    {
        vectorData[3] = bitFilter;
    }

    short RegisterData::getPreScaler()
    {
        return vectorData[4];
    }

    void RegisterData::setPreScaler(const short code)
    {
        vectorData[4] = code;
    }

    short RegisterData::getGyroscopeScale()
    {
        return vectorData[5];
    }

    void RegisterData::setGyroscopeScale(const short scale)
    {
        vectorData[5] = scale;
    }

    short RegisterData::getGyroscopeRate()
    {
        return vectorData[6];
    }

    void RegisterData::setGyroscopeRate(const short rate)
    {
        vectorData[6] = rate;
    }

    short RegisterData::getAccelerometerScale()
    {
        return vectorData[7];
    }

    void RegisterData::setAccelerometerScale(const short scale)
    {
        vectorData[7] = scale;
    }

    short RegisterData::getAccelerometerRate()
    {
        return vectorData[8];
    }

    void RegisterData::setAccelerometerRate(const short rate)
    {
        vectorData[8] = rate;
    }

    short RegisterData::getMagnetometerScale()
    {
        return vectorData[9];
    }

    void RegisterData::setMagnetometerScale(const short scale)
    {
        vectorData[9] = scale;
    }

    short RegisterData::getMagnetometerRate()
    {
        return vectorData[10];
    }

    void RegisterData::setMagnetometerRate(const short rate)
    {
        vectorData[10] = rate;
    }

    short RegisterData::getMagnetometerXOffset()
    {
        return vectorData[11];
    }

    void RegisterData::setMagnetometerXOffset(const short offset)
    {
        vectorData[11] = offset;
    }

    short RegisterData::getMagnetometerYOffset()
    {
        return vectorData[12];
    }

    void RegisterData::setMagnetometerYOffset(const short offset)
    {
        vectorData[12] = offset;
    }

    short RegisterData::getMagnetometerZOffset()
    {
        return vectorData[13];
    }

    void RegisterData::setMagnetometerZOffset(const short offset)
    {
        vectorData[13] = offset;
    }

    short RegisterData::getAccelerometerXOffset()
    {
        return vectorData[14];
    }

    void RegisterData::setAccelerometerXOffset(const short offset)
    {
        vectorData[14] = offset;
    }

    short RegisterData::getAccelerometerYOffset()
    {
        return vectorData[15];
    }

    void RegisterData::setAccelerometerYOffset(const short offset)
    {
        vectorData[15] = offset;
    }

    short RegisterData::getAccelerometerZOffset()
    {
        return vectorData[16];
    }

    void RegisterData::setAccelerometerZOffset(const short offset)
    {
        vectorData[16] = offset;
    }

    short RegisterData::getGyroscopeXOffset()
    {
        return vectorData[17];
    }

    void RegisterData::setGyroscopeXOffset(const short offset)
    {
        vectorData[17] = offset;
    }

    short RegisterData::getGyroscopeYOffset()
    {
        return vectorData[18];
    }

    void RegisterData::setGyroscopeYOffset(const short offset)
    {
        vectorData[18] = offset;
    }

    short RegisterData::getGyroscopeZOffset()
    {
        return vectorData[19];
    }

    void RegisterData::setGyroscopeZOffset(const short offset)
    {
        vectorData[19] = offset;
    }

    short RegisterData::getFirmwareVersion()
    {
        return vectorData[20];
    }

    void RegisterData::setFirmwareVersion(const short version)
    {
        vectorData[20] = version;
    }

    void byteToShort(const unsigned char byteArray[], short shortArray[],
            unsigned int shortArrayLength, bool bigEndian)
    {
        for (unsigned int i = 0; i < shortArrayLength; ++i)
        {
            shortArray[i] = 0;
            for (unsigned int j = 0; j < 2; ++j)
            {
                short part = (short)byteArray[i*2+j];
                if ((j == 0 && bigEndian) ||
                    (j == 1 && !bigEndian)) part <<= 8;
                shortArray[i] |= part;
            }
        }
    }
}
