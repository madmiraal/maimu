// (c) 2016: Marcel Admiraal

#include "nulistener.h"

namespace nu
{
    void Listener::dataReceived(ma::Data* data)
    {
        switch (data->getDataType())
        {
        case DataType::IMUOnly:
            imuDataReceived(data);
            break;
        case DataType::AnalogueOnly:
            analogueDataReceived(data);
            break;
        case DataType::QuaternionOnly:
            quaternionDataReceived(data);
            break;
        case DataType::IMUAnalogue:
            imuDataReceived(data);
            analogueDataReceived(data);
            break;
        case QuaternionAnalogue:
            quaternionDataReceived(data);
            analogueDataReceived(data);
            break;
        default:
            ma::IMUListener::dataReceived(data);
            break;
        }
    }

    void Listener::imuDataReceived(ma::Data* data)
    {
        IMUData* imuData =
            dynamic_cast<IMUData*>(data);
        if (imuData != 0)
        {
            // Inform accelerometer listeners.
            ma::Vector accelerometerVector = imuData->getAccelerometerData();
            if (accelerometerVector.size() == 3)
                onAccelerometerDataReceived(ma::Data (
                        ma::DataType::AccelerometerData,
                        accelerometerVector));
            // Inform gyroscope listeners.
            ma::Vector gyroscopeVector = imuData->getGyroscopeData();
            if (gyroscopeVector.size() == 3)
                onGyroscopeDataReceived(ma::Data (
                        ma::DataType::GyroscopeData,
                        gyroscopeVector));
            // Inform magnetometer listeners.
            ma::Vector magnetometerVector = imuData->getMagnetometerData();
            if (magnetometerVector.size() == 3)
                onMagnetometerDataReceived(ma::Data (
                        ma::DataType::MagnetometerData,
                        magnetometerVector));
        }
    }

    void Listener::analogueDataReceived(ma::Data* data)
    {
        AnalogueData* analogueData =
            dynamic_cast<AnalogueData*>(data);
        if (analogueData != 0)
        {
            // Inform analogue listeners.
            ma::Vector analogueVector = analogueData->getAnalogueData();
            if (analogueVector.size() == 8)
                onAnalogueDataReceived(ma::Data (
                        ma::DataType::AnalogueData,
                        analogueVector));
        }
    }

    void Listener::quaternionDataReceived(ma::Data* data)
    {
        QuaternionData* quaternionData =
            dynamic_cast<QuaternionData*>(data);
        if (quaternionData != 0)
        {
            // Inform quaternion listeners.
            ma::Data newData(ma::DataType::QuaternionData,
                    quaternionData->getQuaternionData());
            onQuaternionReceived(newData);
        }
    }
}
