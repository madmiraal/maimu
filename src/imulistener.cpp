// (c) 2016: Marcel Admiraal

#include "imulistener.h"

namespace ma
{
    void IMUListener::dataReceived(ma::Data* data)
    {
        switch (data->getDataType())
        {
        case DataType::MessageData:
            onMessageDataReceived(*data);
            break;
        case DataType::AccelerometerData:
            onAccelerometerDataReceived(*data);
            break;
        case DataType::GyroscopeData:
            onGyroscopeDataReceived(*data);
            break;
        case DataType::MagnetometerData:
            onMagnetometerDataReceived(*data);
            break;
        case DataType::QuaternionData:
            onQuaternionReceived(*data);
            break;
        case DataType::AnalogueData:
            onAnalogueDataReceived(*data);
            break;
        case DataType::RegisterData:
            onRegisterDataReceived(*data);
            break;
        }
    }
}
