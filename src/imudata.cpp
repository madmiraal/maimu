// (c) 2016: Marcel Admiraal

#include "imudata.h"

#include <algorithm>
#include <fstream>

namespace ma
{
    Data::Data() : type(DataType::NullData)
    {
    }

    Data::Data(const unsigned int type) : type(type)
    {
    }

    Data::Data(const unsigned int type, const Str& stringData) :
            type(type), stringData(stringData)
    {
    }

    Data::Data(const unsigned int type, const Vector& vectorData) :
            type(type), vectorData(vectorData)
    {
    }

    Data::Data(const Data& source) :
            type(source.type), vectorData(source.vectorData),
            stringData(source.stringData)
    {
    }

    Data::~Data()
    {
    }

    void swap(Data& first, Data& second)
    {
        std::swap(first.type, second.type);
        swap(first.vectorData, second.vectorData);
        swap(first.stringData, second.stringData);
    }

    Data& Data::operator=(Data source)
    {
        swap(*this, source);
        return *this;
    }

    unsigned int Data::getDataType() const
    {
        return type;
    }

    void Data::setDataType(const unsigned int type)
    {
        this->type = type;
    }

    Vector Data::getVectorData() const
    {
        return vectorData;
    }

    void Data::setVectorData(Vector data)
    {
        vectorData = data;
    }

    Str Data::getStringData() const
    {
        return stringData;
    }

    void Data::setStringData(Str data)
    {
        stringData = data;
    }

    bool Data::saveData(const Str& filename)
    {
        std::ofstream fileStream(filename.c_str(), std::ofstream::out);
        if (!fileStream.is_open()) return false;
        fileStream << *this << std::endl;
        return true;
    }

    bool Data::loadData(const Str& filename)
    {
        std::ifstream fileStream(filename.c_str(), std::ifstream::in);
        if (!fileStream.is_open()) return false;
        fileStream >> *this;
        if (!fileStream.good()) return false;
        return true;
    }

    std::istream& operator>>(std::istream& is, Data& destination)
    {
        unsigned int type;
        Vector vectorData;
        Str stringData;

        is >> type;
        is >> vectorData;
        is >> stringData;
        if (is.good())
        {
            destination.setDataType(type);
            destination.setVectorData(vectorData);
            destination.setStringData(stringData);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const Data& source)
    {

        os << source.getDataType();
        os << '\t' << source.getVectorData();
        os << '\t' << source.getStringData();
        return os;
    }
}
