# MAIMU #

Cross-platform C++ code for connecting to IMUs.

Currently two types of IMU are supported: [x-IMU](http://x-io.co.uk/) and the [nu IMU](https://bitbucket.org/account/user/biomechatronicslab/projects/NUIM).

## Dependencies ##
This code depends on:

* [wxWidgets](https://www.wxwidgets.org/)
* [MAUtils](https://bitbucket.org/madmiraal/mautils)
* [MAwxUtils](https://bitbucket.org/madmiraal/mawxutils)
