// Enumerations used by the x-IMU API.
//
// (c) 2013 - 2016: Marcel Admiraal

#ifndef XIMUENUM_H
#define XIMUENUM_H

namespace xIMU
{
    // Packet headers.
    enum class PacketHeader
    {
        Error,
        Command,
        ReadRegister,
        WriteRegister,
        ReadDateTime,
        WriteDateTime,
        RawBatteryAndTemperatureData,
        CalBatteryAndTemperatureData,
        RawInertialAndMagneticData,
        CalInertialAndMagneticData,
        QuaternionData,
        DigitalIOdata,
        RawAnalogueInputData,
        CalAnalogueInputData,
        PWMOutputData,
        RawADXL345BusData,
        CalADXL345BusData
    };

    // Error codes.
    enum class ErrorCode
    {
        NoError,
        FactoryResetFailed,
        LowBattery,
        USBreceiveBufferOverrun,
        USBtransmitBufferOverrun,
        BluetoothReceiveBufferOverrun,
        BluetoothTransmitBufferOverrun,
        SDcardWriteBufferOverrun,
        TooFewBytesInPacket,
        TooManyBytesInPacket,
        InvalidChecksum,
        UnknownHeader,
        InvalidNumBytesForPacketHeader,
        InvalidRegisterAddress,
        RegisterReadOnly,
        InvalidRegisterValue,
        InvalidCommand,
        GyroscopeAxisNotAt200dps,
        GyroscopeNotStationary,
        AcceleroemterAxisNotAt1g,
        MagnetometerSaturation,
        IncorrectAuxillaryPortMode,
        UARTreceiveBufferOverrun,
        UARTtransmitBufferOverrun
    };

    // Command codes.
    enum class CommandCode
    {
        NullCommand,
        FactoryReset,
        Reset,
        Sleep,
        ResetSleepTimer,
        SampleGyroscopeAxisAt200dps,
        CalculateGyroscopeSensitivity,
        SampleGyroscopeBiasTemp1,
        SampleGyroscopeBiasTemp2,
        CalculateGyroscopeBiasParameters,
        SampleAccelerometerAxisAt1g,
        CalculateAccelerometerBiasAndSensitivity,
        MeasureMagnetometerBiasAndSensitivity,
        AlgorithmInitialise,
        AlgorithmTare,
        AlgorithmClearTare,
        AlgorithmInitialiseThenTare
    };

    // Number of fractional bits used by fixed-point representations.
    enum class QValue
    {
        CalibratedBattery = 12,
        CalibratedThermometer = 8,
        CalibratedGyroscope = 4,
        CalibratedAccelerometer = 11,
        CalibratedMagnetometer = 11,
        Quaternion = 15,
        BatterySensitivity = 5,
        BatteryBias = 8,
        ThermometerSensitivity = 6,
        ThermometerBias = 0,
        GyroscopeSensitivity = 7,
        GyroscopeSampled200dps = 0,
        GyroscopeBiasAt25degC = 3,
        GyroscopeBiasTempSensitivity = 11,
        GyroscopeSampledBias = 3,
        AccelerometerSensitivity = 4,
        AccelerometerBias = 8,
        AccelerometerSampled1g = 4,
        MagnetometerSensitivity = 4,
        MagnetometerBias = 8,
        MagnetometerHardIronBias = 11,
        AlgorithmKp = 11,
        AlgorithmKi = 15,
        AlgorithmInitKp = 11,
        AlgorithmInitPeriod = 11,
        CalibratedAnalogueInput = 12,
        AnalogueInputSensitivity = 4,
        AnalogueInputBias = 8,
        CalibratedADXL345 = 10,
        ADXL345busSensitivity = 6,
        ADXL345busBias = 8
    };

    // Register addresses.
    enum class RegisterAddress
    {
        FirmwareVersionMajorNum,
        FirmwareVersionMinorNum,
        DeviceID,
        ButtonMode,
        BatterySensitivity,
        BatteryBias,
        ThermometerSensitivity,
        ThermometerBias,
        GyroscopeFullScale,
        GyroscopeSensitivityX,
        GyroscopeSensitivityY,
        GyroscopeSensitivityZ,
        GyroscopeSampledPlus200dpsX,
        GyroscopeSampledPlus200dpsY,
        GyroscopeSampledPlus200dpsZ,
        GyroscopeSampledMinus200dpsX,
        GyroscopeSampledMinus200dpsY,
        GyroscopeSampledMinus200dpsZ,
        GyroscopeBiasAt25degCX,
        GyroscopeBiasAt25degCY,
        GyroscopeBiasAt25degCZ,
        GyroscopeBiasTempSensitivityX,
        GyroscopeBiasTempSensitivityY,
        GyroscopeBiasTempSensitivityZ,
        GyroscopeSample1Temp,
        GyroscopeSample1BiasX,
        GyroscopeSample1BiasY,
        GyroscopeSample1BiasZ,
        GyroscopeSample2Temp,
        GyroscopeSample2BiasX,
        GyroscopeSample2BiasY,
        GyroscopeSample2BiasZ,
        AccelerometerFullScale,
        AccelerometerSensitivityX,
        AccelerometerSensitivityY,
        AccelerometerSensitivityZ,
        AccelerometerBiasX,
        AccelerometerBiasY,
        AccelerometerBiasZ,
        AccelerometerSampledPlus1gX,
        AccelerometerSampledPlus1gY,
        AccelerometerSampledPlus1gZ,
        AccelerometerSampledMinus1gX,
        AccelerometerSampledMinus1gY,
        AccelerometerSampledMinus1gZ,
        MagnetometerFullScale,
        MagnetometerSensitivityX,
        MagnetometerSensitivityY,
        MagnetometerSensitivityZ,
        MagnetometerBiasX,
        MagnetometerBiasY,
        MagnetometerBiasZ,
        MagnetometerHardIronBiasX,
        MagnetometerHardIronBiasY,
        MagnetometerHardIronBiasZ,
        AlgorithmMode,
        AlgorithmKp,
        AlgorithmKi,
        AlgorithmInitKp,
        AlgorithmInitPeriod,
        AlgorithmMinValidMag,
        AlgorithmMaxValidMag,
        AlgorithmTareQuat0,
        AlgorithmTareQuat1,
        AlgorithmTareQuat2,
        AlgorithmTareQuat3,
        SensorDataMode,
        DateTimeDataRate,
        BatteryAndTemperatureDataRate,
        InertialAndMagneticDataRate,
        QuaternionDataRate,
        SDcardNewFileName,
        BatteryShutdownVoltage,
        SleepTimer,
        MotionTrigWakeUp,
        BluetoothPower,
        AuxiliaryPortMode,
        DigitalIOdirection,
        DigitalIOdataRate,
        AnalogueInputDataMode,
        AnalogueInputDataRate,
        AnalogueInputSensitivity,
        AnalogueInputBias,
        PWMoutputFrequency,
        ADXL345busDataMode,
        ADXL345busDataRate,
        ADXL345AsensitivityX,
        ADXL345AsensitivityY,
        ADXL345AsensitivityZ,
        ADXL345AbiasX,
        ADXL345AbiasY,
        ADXL345AbiasZ,
        ADXL345BsensitivityX,
        ADXL345BsensitivityY,
        ADXL345BsensitivityZ,
        ADXL345BbiasX,
        ADXL345BbiasY,
        ADXL345BbiasZ,
        ADXL345CsensitivityX,
        ADXL345CsensitivityY,
        ADXL345CsensitivityZ,
        ADXL345CbiasX,
        ADXL345CbiasY,
        ADXL345CbiasZ,
        ADXL345DsensitivityX,
        ADXL345DsensitivityY,
        ADXL345DsensitivityZ,
        ADXL345DbiasX,
        ADXL345DbiasY,
        ADXL345DbiasZ,
        UARTbaudRate,
        UARThardwareFlowControl,
        NumRegisters
    };

    // Button mode register values.
    enum class ButtonMode
    {
        Disabled,
        Reset,
        SleepWake,
        AlgorithmInitialise,
        AlgorithmTare,
        AlgorithmInitialiseThenTare
    };

    // Accelerometer full-scale register values.
    enum class GyroscopeScale
    {
        FullScalePlusMinus250dps,
        FullScalePlusMinus500dps,
        FullScalePlusMinus1000dps,
        FullScalePlusMinus2000dps
    };

    // Accelerometer full-scale register values.
    enum class AccelerometerScale
    {
        PlusMinus2g,
        PlusMinus4g,
        PlusMinus8g
    };

    // Magnetometer full-scale register values.
    enum class MagnetometerScale
    {
        PlusMinus1p3G,
        PlusMinus1p9G,
        PlusMinus2p5G,
        PlusMinus4p0G,
        PlusMinus4p7G,
        PlusMinus5p6G,
        PlusMinus8p1G
    };

    // Algorithm mode register values.
    enum class AlgorithmMode
    {
        Disabled,
        IMU,
        AHRS
    };

    // Data mode register values.
    enum class SensorDataMode
    {
        Raw,
        Calibrated
    };

    // Data output rate register values.
    enum class DataOutputRate
    {
        Disabled,
        Rate1Hz,
        Rate2Hz,
        Rate4Hz,
        Rate8Hz,
        Rate16Hz,
        Rate32Hz,
        Rate64Hz,
        Rate128Hz,
        Rate256Hz,
        Rate512Hz
    };

    // Motion triggered wake up mode register values.
    enum class MotionTriggerWakeUpMode
    {
        Disabled,
        LowSensitivity,
        HighSensitivity
    };

    // Bluetooth power mode register values.
    enum class BluetoothPowerMode
    {
        Disabled,
        Enabled
    };

    // Auxiliary Port mode register values.
    enum class AuxiliaryPortMode
    {
        Disabled,
        DigitalIO,
        AnalogueInput,
        PWMoutput,
        ADXL345bus,
        SleepWake
    };

    // Digital I/O direction register values.
    enum class DigitalIOdirection
    {
        In01234567,
        In0123456Out7,
        In012345Out67,
        In01234Out567,
        In0123Out4567,
        In012Out34567,
        In01Out234567,
        In0Out1234567,
        Out01234567
    };

    // UART baud rate register values.
    enum class UARTBaudRate
    {
        UARTBaudRate2400,
        UARTBaudRate4800,
        UARTBaudRate7200,
        UARTBaudRate9600,
        UARTBaudRate14400,
        UARTBaudRate19200,
        UARTBaudRate38400,
        UARTBaudRate57600,
        UARTBaudRate115200,
        UARTBaudRate230400
    };

    // UART hardware flow control register values.
    enum class UARTHardwareFlowControl
    {
        Off,
        On
    };
}

#endif // XIMUENUM_H
