// Abstract class for listening to packets received from the x-IMU.
//
// (c) 2013 - 2016: Marcel Admiraal

#ifndef XIMULISTENER_H
#define XIMULISTENER_H

#include "imulistener.h"
#include "ximudata.h"

namespace xIMU
{
    class Listener : public virtual ma::IMUListener
    {
    public:
        /**
         * Default destructor.
         */
        virtual ~Listener() {}

        /**
         * Called by the API when a data packet is received from the IMU.
         * Uses the data type to call the appropriate data type received
         * function.
         *
         * @param data  Pointer to the data received.
         */
        void dataReceived(ma::Data* data);

        /**
         * Called when error data received from the IMU.
         *
         * @param data  The error data received.
         */
        virtual void onErrorDataReceived(ErrorData data) {}

        /**
         * Called when command data received from the IMU.
         *
         * @param data  The command data received.
         */
        virtual void onCommandDataReceived(CommandData data) {}

        /**
         * Called when register data received from the IMU.
         *
         * @param data  The register data received.
         */
        virtual void onRegisterDataReceived(RegisterData data) {}

        /**
         * Called when data and time data received from the IMU.
         *
         * @param data  The date and time data received.
         */
        virtual void onDateTimeDataReceived(DateTimeData data) {}

        /**
         * Called when raw battery and temperature data received from the IMU.
         *
         * @param data  The raw battery and temperature data received.
         */
        virtual void onRawBatteryAndTemperatureDataReceived(
            RawBatteryAndTemperatureData data) {}

        /**
         * Called when calibrated battery and temperature data received from the
         * IMU.
         *
         * @param data  The calibrated battery and temperature data received.
         */
        virtual void onCalBatteryAndTemperatureDataReceived(
            CalBatteryAndTemperatureData data) {}

        /**
         * Called when raw inertial and magnetometer data received from the IMU.
         *
         * @param data  The raw inertial and magnetometer data received.
         */
        virtual void onRawInertialAndMagneticDataReceived(
            RawInertialAndMagneticData data) {}

        /**
         * Called when calibrated inertial and magenetometer data received from
         * the IMU.
         *
         * @param data  The calibrated inertial and magenetometer data received.
         */
        virtual void onCalInertialAndMagneticDataReceived(
            CalInertialAndMagneticData data) {}

        /**
         * Called when quaternion data received from the IMU.
         *
         * @param data  The quaternion data received.
         */
        virtual void onQuaternionDataReceived(QuaternionData data) {}

        /**
         * Called when digital input-output data received from the IMU.
         *
         * @param data  The digital input-output data received.
         */
        virtual void onDigitalIODataReceived(DigitalIOData data) {}

        /**
         * Called when raw analouge input data received from the IMU.
         *
         * @param data  The raw analouge input data received.
         */
        virtual void onRawAnalogueInputDataReceived(RawAnalogueInputData data) {}

        /**
         * Called when calibrated analouge input data received from the IMU.
         *
         * @param data  The calibrated analouge input data received.
         */
        virtual void onCalAnalogueInputDataReceived(CalAnalogueInputData data) {}

        /**
         * Called when PWM output data received from the IMU.
         *
         * @param data  The PWM output data received.
         */
        virtual void onPWMOutputDataReceived(PWMOutputData data) {}

        /**
         * Called when raw ADXL345 data received from the IMU.
         *
         * @param data  The raw ADXL345 data received.
         */
        virtual void onRawADXL345BusDataReceived(RawADXL345BusData data) {}

        /**
         * Called when calibrated ADXL345 data received from the IMU.
         *
         * @param data  The calibrated ADXL345 data received.
         */
        virtual void onCalADXL345BusDataReceived(CalADXL345BusData data) {}
    };
}

#endif // XIMULISTENER_H
