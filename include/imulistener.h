// Base abstract class for listening to packets from an IMU.
//
// (c) 2016: Marcel Admiraal

#ifndef IMULISTENER_H
#define IMULISTENER_H

#include "imudata.h"

namespace ma
{
    class IMUListener
    {
    public:
        /**
         * Default destructor.
         */
        virtual ~IMUListener() {}

        /**
         * Called by the API when a data packet is received from the IMU.
         * Uses the data type to call the appropriate data type received
         * function.
         *
         * @param data  Pointer to the data received.
         */
        virtual void dataReceived(Data* data);

        /**
         * Called when message data received from the IMU.
         *
         * @param data  The message data received.
         */
        virtual void onMessageDataReceived(const Data& data) {};

        /**
         * Called when accelerometer data received from the IMU.
         *
         * @param data  The accelerometer data received.
         */
        virtual void onAccelerometerDataReceived(const Data& data) {};

        /**
         * Called when gyroscope data received from the IMU.
         *
         * @param data  The gyroscope data received.
         */
        virtual void onGyroscopeDataReceived(const Data& data) {};

        /**
         * Called when magnetometer data received from the IMU.
         *
         * @param data  The magnetometer data received.
         */
        virtual void onMagnetometerDataReceived(const Data& data) {};

        /**
         * Called when quaternion data received from the IMU.
         *
         * @param data  The quaternion data received.
         */
        virtual void onQuaternionReceived(const Data& data) {};

        /**
         * Called when analogue data received from the IMU.
         *
         * @param data  The analogue data received.
         */
        virtual void onAnalogueDataReceived(const Data& data) {};

        /**
         * Called when register data received from the IMU.
         *
         * @param data  The register data received.
         */
        virtual void onRegisterDataReceived(const Data& data) {};
    };
}

#endif //IMULISTENER_H
