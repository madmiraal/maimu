// nu Application Programming Interface for recorded raw data.
//
// (c) 2018 - 2019: Marcel Admiraal

#ifndef NUFILERAWAPI_H
#define NUFILERAWAPI_H

#include "nufileapi.h"

namespace nu
{
    class FileRawAPI : public FileAPI
    {
    public:
        /**
         * Default constructor.
         */
        FileRawAPI(unsigned int imu);

        /**
         * Default destructor.
         */
        virtual ~FileRawAPI();

    protected:
        /**
         * Return a pointer to the next data item if available.
         * Note: The API will take ownership of the data object and destroy it.
         *
         * @return A pointer to the next data item, or zero if no data available.
         */
        virtual ma::Data* getNextData();

    private:
        ma::Data* getSignalData();
        void getPacket(unsigned char* packet, unsigned int& packetLength);

        ma::Data* signalData;
        double remainingTime;
    };
}

#endif // NUFILERAWAPI_H
