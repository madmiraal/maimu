// x-IMU Application Programming Interface
//
// (c) 2013 - 2016: Marcel Admiraal

#ifndef XIMUAPI_H
#define XIMUAPI_H

#include "imuapi.h"
#include "ximuenum.h"

namespace xIMU
{
    class API : public ma::IMUAPI
    {
    public:
        /**
         * Default constructor.
         */
        API();

        /**
         * Default destructor.
         */
        virtual ~API();

        /**
         * Sends the commands to fetch the imu's register data.
         *
         * @return Whether the commands were successfully sent.
         */
        virtual bool getRegisters();

        /**
         * Sends the commands to set the imu's register data.
         *
         * @param registerData  The new register data.
         * @return              Whether the commands were successfully sent.
         */
        virtual bool setRegisters(ma::Data* registerData);

        /**
         * Sends a command code to the xIMU.
         * Returns whether or not the send was successful.
         *
         * @param commandCode   The command code to send.
         * @return              Whether or not the send was successful.
         */
        bool sendCommand(const CommandCode commandCode);

        /**
         * Sends a read register instruction to the xIMU.
         * Returns whether or not the send was successful.
         *
         * @param registerAddress   The address of the register to read.
         * @return                  Whether or not the send was successful.
         */
        bool sendReadRegister(const RegisterAddress registerAddress);

        /**
         * Sends a write register instruction to the xIMU.
         * Returns whether or not the send was successful.
         *
         * @param registerAddress   The address of the register to write to.
         * @param value             The short value to assign to the register.
         * @return                  Whether or not the send was successful.
         */
        bool sendWriteRegister(
            const RegisterAddress registerAddress,
            const unsigned short value
            );

        /**
         * Sends a read date and time instruction to the xIMU.
         * Returns whether or not the send was successful.
         *
         * @return Whether or not the send was successful.
         */
        bool sendReadDateTime();

        /**
         * Sends a write date and time instruction to the xIMU.
         * Returns whether or not the send was successful.
         *
         * @param year      The year of the date.
         * @param month     The month of the date.
         * @param day       The day of the date.
         * @param hour      The hours of the time.
         * @param minute    The minutes of the time.
         * @param second    The seconds of the time.
         * @return          Whether or not the send was successful.
         */
        bool sendWriteDateTime(
            const int year,
            const int month,
            const int day,
            const int hour,
            const int minute,
            const int second
            );

        /**
         * Sends a set and read I/O port instruction to the xIMU.
         * Returns whether or not the send was successful.
         *
         * @param directionByte The channels (bits) to set as
                                input (1) and output (0).
         * @param stateByte     The output channels to set
                                high (1) or low (0).
         * @return              Whether or not the send was successful.
         */
        bool sendDigitalIO(
            const unsigned char directionByte,
            const unsigned char valueByte
            );

        /**
         * Sends a set PWM duty cycles instruction to the xIMU.
         * Values are between 0 to 65535 representing 0% to 100%.
         * Returns whether or not the send was successful.
         *
         * @param channel1  The duty cycle of the first channel.
         * @param channel2  The duty cycle of the second channel.
         * @param channel3  The duty cycle of the third channel.
         * @param channel4  The duty cycle of the fourth channel.
         * @return          Whether or not the send was successful.
         */
        bool sendPWMOutput(
            unsigned short channel1,
            unsigned short channel2,
            unsigned short channel3,
            unsigned short channel4
            );

    protected:
        /**
         * Read bytes from the underlying serial port and then create and return
         * a pointer to the next data item.
         * Note: The API will take ownership of the data object and destroy it.
         * @return A pointer to the data received, or zero if no data available.
         */
        virtual ma::Data* getNextData();

    private:
        /**
         * Reads bytes into the packet array and populate the packetLength.
         *
         * @param packet        The array to populate with the packet bytes.
         * @param packetLength  The length of the packet read.
         */
        void getPacket(unsigned char* packet, unsigned int& packetLength);

        /**
         * Extracts the data from the packet populated with the getPacket
         * function, and returns a pointer to the data.
         *
         * @param packet        The array with the packet.
         * @param packetLength  The length of the packet.
         * @return              A pointer to the data received.
         */
        ma::Data* extractData(const unsigned char* packet,
                const unsigned int packetLength);

        unsigned int calcEncodedPacketLength(const unsigned int decodedLength);
        unsigned int calcDecodedPacketLength(const unsigned int encodedLength);
        void encodePacket(unsigned char* packet,
                const unsigned int decodedLength);
        void decodePacket(unsigned char* packet,
                const unsigned int encodedLength);
        unsigned char calcChecksum(const unsigned char* packet,
                const unsigned int packetLength);
        bool checkChecksum(const unsigned char* packet,
                const unsigned int packetLength);

        ma::Data* extractErrorData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractCommandData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractRegisterData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractDateTimeData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractRawBatteryAndTemperatureData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractCalBatteryAndTemperatureData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractRawInertialAndMagneticData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractCalInertialAndMagneticData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractQuaternionData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractDigitalIOData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractRawAnalogueInputData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractCalAnalogueInputData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractPWMOutputData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractRawADXL345BusData(const unsigned char* packet,
                const unsigned int packetLength);
        ma::Data* extractCalADXL345BusData(const unsigned char* packet,
                const unsigned int packetLength);
    };

    unsigned short concat(const unsigned char MSB, const unsigned char LSB);
}

#endif // XIMUDRIVER_H
