// Classes for storing nu IMU Data.
//
// (c) 2016 - 2018: Marcel Admiraal

#ifndef NUDATA_H
#define NUDATA_H

#include "imudata.h"

// External oscillator frequency.
#define FOSC 30000000

namespace nu
{
    enum DataType
    {
        IMUOnly = ma::DataType::CUSTOM_START,
        AnalogueOnly,
        QuaternionOnly,
        IMUAnalogue,
        QuaternionAnalogue,
        NU_CUSTOM_START
    };

    // Possible values for the gyroscope output data rate and bandwidth cutoff.
    enum gyroscopeODR
    {							// Bits  ODR (Hz) Cutoff
        G_ODR_95_BW_125  = 0x0, // 0000:  95       12.5
        G_ODR_95_BW_25   = 0x1, // 0001:  95       25
        // 0x2 and 0x3 define the same data rate and bandwidth
        G_ODR_190_BW_125 = 0x4, // 0100: 190       12.5
        G_ODR_190_BW_25  = 0x5, // 0101: 190       25
        G_ODR_190_BW_50  = 0x6, // 0110: 190       50
        G_ODR_190_BW_70  = 0x7, // 0111: 190       70
        G_ODR_380_BW_20  = 0x8, // 1000: 380       20
        G_ODR_380_BW_25  = 0x9, // 1001: 380       25
        G_ODR_380_BW_50  = 0xA, // 1010: 380       50
        G_ODR_380_BW_100 = 0xB, // 1011: 380      100
        G_ODR_760_BW_30  = 0xC, // 1100: 760       30
        G_ODR_760_BW_35  = 0xD, // 1101: 760       35
        G_ODR_760_BW_50  = 0xE, // 1110: 760       50
        G_ODR_760_BW_100 = 0xF, // 1111: 760      100
    };

    // Possible values for the gyroscope scale.
    enum gyroscopeScale
    {
        G_SCALE_245DPS,         // 00:  ḟ245 dps (degrees per second)
        G_SCALE_500DPS,         // 01:  ḟ500 dps
        G_SCALE_2000DPS         // 10: ḟ2000 dps
                                // 11: ḟ2000 dps
    };

    // Possible values for the accelerometer output data rate.
    enum accelerometerODR
    {
        A_POWER_DOWN,           // 0000: Power-down mode
        A_ODR_3125,             // 0001:    3.125 Hz
        A_ODR_625,              // 0010:    6.25 Hz
        A_ODR_125,              // 0011:   12.5 Hz
        A_ODR_25,               // 0100:   25 Hz
        A_ODR_50,               // 0101:   50 Hz
        A_ODR_100,              // 0110:  100 Hz
        A_ODR_200,              // 0111:  200 Hz
        A_ODR_400,              // 1000:  400 Hz
        A_ODR_800,              // 1001:  800 Hz
        A_ODR_1600              // 1010: 1600 Hz
    };

    // Possible values for the accelerometer scale.
    enum accelerometerScale
    {
        A_SCALE_2G,             // 000:  ḟ2 g
        A_SCALE_4G,             // 001:  ḟ4 g
        A_SCALE_6G,             // 010:  ḟ6 g
        A_SCALE_8G,             // 011:  ḟ8 g
        A_SCALE_16G             // 100: ḟ16 g
    };

    // Possible values for the magnetometer output data rate.
    enum magnetometerODR
    {
        M_ODR_3125,             // 000:   3.125 Hz
        M_ODR_625,              // 001:   6.25 Hz
        M_ODR_125,              // 010:  12.5 Hz
        M_ODR_25,               // 011:  25 Hz
        M_ODR_50,               // 100:  50 Hz
        M_ODR_100               // 101: 100 Hz
    };

    // Possible values for the magnetometer scale.
    enum magnetometerScale
    {
        M_SCALE_2GS,            // 00:  ḟ2 guass
        M_SCALE_4GS,            // 01:  ḟ4 guass
        M_SCALE_8GS,            // 10:  ḟ8 guass
        M_SCALE_12GS,           // 11: ḟ12 guass
    };

    const double accelerometerScale[] =
    {
        2,      // ±2 g
        4,      // ±4 g
        6,      // ±6 g
        8,      // ±8 g
        16      // ±16 g
    };

    const double gyroscopeScale[] =
    {
        245,    // ±245 degrees per second
        500,    // ±500 degrees per second
        2000,   // ±2000 degrees per second
        2000    // ±2000 degrees per second
    };

    const double magnetometerScale[] =
    {
        2,      // ±2 guass
        4,      // ±4 guass
        8,      // ±8 guass
        12      // ±12 guass
    };

    const int prescaler[4] = {1, 8, 64, 256};

    class IMUData : public virtual ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        IMUData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        IMUData(const IMUData& source);

        /**
         * Vector constructor.
         *
         * @param accelerometerData Vector with the accelerometer data.
         * @param gyroscopeData     Vector with the gyroscope data.
         * @param magnetometerData  Vector with the magnetometer data.
         */
        IMUData(
                const ma::Vector& accelerometerData,
                const ma::Vector& gyrosopeData,
                const ma::Vector& magnetometerData);

        /**
         * Default destructor.
         */
        virtual ~IMUData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(IMUData& first, IMUData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        IMUData& operator=(IMUData source);

        /**
         * Returns the accelerometer data.
         *
         * @return The accelerometer data.
         */
        ma::Vector getAccelerometerData() const;

        /**
         * Returns the gyroscope data.
         *
         * @return The gyroscope data.
         */
        ma::Vector getGyroscopeData() const;

        /**
         * Returns the magnetometer data.
         *
         * @return The magnetometer data.
         */
        ma::Vector getMagnetometerData() const;

        /**
         * Sets the accelerometer data.
         *
         * @param accelerometerData A vector with the accelerometer data.
         */
        void setAccelerometerData(const ma::Vector& accelerometerData);

        /**
         * Sets the gyroscope data.
         *
         * @param gyroscopeData     A vector with the gyroscope data.
         */
        void setGyroscopeData(const ma::Vector& gyroscopeData);

        /**
         * Sets the magnetometer data.
         *
         * @param magnetometerData  A vector with the magnetometer data.
         */
        void setMagnetometerData(const ma::Vector& magnetometerData);

    protected:
        ma::Vector accelerometerData;
        ma::Vector gyroscopeData;
        ma::Vector magnetometerData;
    };

    class AnalogueData : public virtual ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        AnalogueData();

        /**
         * Vector constructor.
         *
         * @param analogueData The analogue data.
         */
        AnalogueData(const ma::Vector& analogueData);

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        AnalogueData(const AnalogueData& source);

        /**
         * Default destructor.
         */
        virtual ~AnalogueData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(AnalogueData& first, AnalogueData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        AnalogueData& operator=(AnalogueData source);

        /**
         * Returns the analogue data.
         *
         * @return The analogue data.
         */
        ma::Vector getAnalogueData() const;

        /**
         * Sets the analogue data.
         *
         * @param analogueData  The analogue data.
         */
        void setAnalogueData(ma::Vector analogueData);

    protected:
        ma::Vector analogueData;
    };

    class QuaternionData : public virtual ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        QuaternionData();

        /**
         * Vector constructor.
         *
         * @param quaternionData    The quaternion data.
         */
        QuaternionData(const ma::Vector& quaternionData);

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        QuaternionData(const QuaternionData& source);

        /**
         * Default destructor.
         */
        virtual ~QuaternionData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(QuaternionData& first, QuaternionData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        QuaternionData& operator=(QuaternionData source);

        /**
         * Returns the quaternion data.
         *
         * @return The quaternion data.
         */
        ma::Vector getQuaternionData() const;

        /**
         * Sets the analogue data.
         *
         * @param quaternionData    The quaternion data.
         */
        void setQuaternionData(ma::Vector quaternionData);

    protected:
        ma::Vector quaternionData;
    };

    class IMUAnalogueData : public IMUData, public AnalogueData
    {
    public:
        /**
         * Default constructor.
         */
        IMUAnalogueData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        IMUAnalogueData(const IMUAnalogueData& source);

        /**
         * Vector constructor.
         *
         * @param accelerometerData Vector with the accelerometer data.
         * @param gyroscopeData     Vector with the gyroscope data.
         * @param magnetometerData  Vector with the magnetometer data.
         * @param analogueData      Vector with the analogue data.
         */
        IMUAnalogueData(
                const ma::Vector& accelerometerData,
                const ma::Vector& gyrosopeData,
                const ma::Vector& magnetometerData,
                const ma::Vector& analogueData);

        /**
         * Default destructor.
         */
        virtual ~IMUAnalogueData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(IMUAnalogueData& first, IMUAnalogueData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        IMUAnalogueData& operator=(IMUAnalogueData source);
    };

    class QuaternionAnalogueData : public QuaternionData, public AnalogueData
    {
    public:
        /**
         * Default constructor.
         */
        QuaternionAnalogueData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        QuaternionAnalogueData(const QuaternionAnalogueData& source);

        /**
         * Vector constructor.
         *
         * @param quaternionData    Vector with the quaternion data.
         * @param analogueData      Vector with the analogue data.
         */
        QuaternionAnalogueData(
                const ma::Vector& quaternionData,
                const ma::Vector& analogueData);

        /**
         * Default destructor.
         */
        virtual ~QuaternionAnalogueData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(QuaternionAnalogueData& first,
                QuaternionAnalogueData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        QuaternionAnalogueData& operator=(QuaternionAnalogueData source);
    };

    class RegisterData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        RegisterData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        RegisterData(const RegisterData& source);

        /**
         * Vector constructor.
         *
         * @param registerData  Vector with the register data values.
         */
        RegisterData(const ma::Vector& registerData);

        /**
         * Default destructor.
         */
        virtual ~RegisterData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(RegisterData& first, RegisterData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        RegisterData& operator=(RegisterData source);

        /**
         * Sets the register by register number.
         *
         * @param registerNumber    The register number to set.
         * @param registerValue     The new register value.
         * @return                  Whether it was a valid register number.
         */
        bool setRegister(unsigned int registerNumber, short registerValue);

        short getPreScalerValue();

        /**
         * Returns the pre-scaler value associated with the index.
         *
         * @param index The index of pre-scaler.
         * @return      The pre-scaler value.
         */
        short getPreScalerValue(unsigned int index);

        /**
         * Returns the sample frequency based on the period and pre-scaler
         * registers data.
         *
         * @return The sample frequency.
         */
        int getSampleFrequency();

        /**
         * Sets the period and prescale registers based on the frequency.
         *
         * @param sampleFrequency   The sample frequency in Hz.
        */
        void setSampleFrequency(int sampleFrequency);

        /**
         * Returns the id in the register data.
         *
         * @return The id in the register data.
         */
        short getID();

        /**
         * Sets the id in the register data.
         *
         * @param id    The id to set in the register data.
         */
        void setID(const short id);

        /**
         * Returns the sample period in the register data.
         *
         * @return The sample period in the register data.
         */
        unsigned short getPeriod();

        /**
         * Sets the sample period in the register data.
         *
         * @param period    The sample period to set in the register data.
         */
        void setPeriod(const unsigned short period);

        /**
         * Returns the quaternion sample skips in the register data.
         *
         * @return The quaternion sample skips in the register data.
         */
        short getSkip();

        /**
         * Sets the quaternion sample skips in the register data.
         *
         * @param skip The quaternion sample skips to set in the register data.
         */
        void setSkip(const short skip);

        /**
         * Returns the ADC channel bit filter in the register data.
         *
         * @return The ADC channel bit filter in the register data.
         */
        short getADCChannelFilter();

        /**
         * Sets the ADC channel bit filter in the register data.
         *
         * @param bitFilter The ADC channel bit filter to set in the register
         *                  data.
         */
        void setADCChannelFilter(const short bitFilter);

        /**
         * Returns the pre-scaler code in the register data.
         * 0 = 1, 1 = 8, 2 = 64, 3 = 256
         *
         * @return The pre-scaler code in the register data.
         */
        short getPreScaler();

        /**
         * Sets the pre-scaler code in the register data.
         * 0 = 1, 1 = 8, 2 = 64, 3 = 256
         *
         * @param code  The pre-scaler code to set in the register data.
         */
        void setPreScaler(const short code);

        /**
         * Returns the gyroscope scale in the register data.
         *
         * @return The gyroscope scale in the register data.
         */
        short getGyroscopeScale();

        /**
         * Sets the gyroscope scale in the register data.
         *
         * @param scale The gyroscope scale to set in the register data.
         */
        void setGyroscopeScale(const short scale);

        /**
         * Returns the gyroscope rate in the register data.
         *
         * @return The gyroscope rate in the register data.
         */
        short getGyroscopeRate();

        /**
         * Sets the gyroscope rate in the register data.
         *
         * @param rate  The gyroscope rate to set in the register data.
         */
        void setGyroscopeRate(const short rate);

        /**
         * Returns the accelerometer scale in the register data.
         *
         * @return The accelerometer scale in the register data.
         */
        short getAccelerometerScale();

        /**
         * Sets the accelerometer scale in the register data.
         *
         * @param scale The accelerometer scale to set in the register data.
         */
        void setAccelerometerScale(const short scale);

        /**
         * Returns the accelerometer rate in the register data.
         *
         * @return The accelerometer rate in the register data.
         */
        short getAccelerometerRate();

        /**
         * Sets the accelerometer rate in the register data.
         *
         * @param rate  The accelerometer rate to set in the register data.
         */
        void setAccelerometerRate(const short rate);

        /**
         * Returns the magnetometer scale in the register data.
         *
         * @return The magnetometer scale in the register data.
         */
        short getMagnetometerScale();

        /**
         * Sets the magnetometer scale in the register data.
         *
         * @param scale The magnetometer scale to set in the register data.
         */
        void setMagnetometerScale(const short scale);

        /**
         * Returns the magnetometer rate in the register data.
         *
         * @return The magnetometer rate in the register data.
         */
        short getMagnetometerRate();

        /**
         * Sets the magnetometer rate in the register data.
         *
         * @param rate  The magnetometer rate to set in the register data.
         */
        void setMagnetometerRate(const short rate);

        /**
         * Returns the magnetometer X-axis offset in the register data.
         *
         * @return The magnetometer X-axis offset in the register data.
         */
        short getMagnetometerXOffset();

        /**
         * Sets the magnetometer X-axis offset in the register data.
         *
         * @param offset    The magnetometer X-axis offset to set in the
         *                  register data.
         */
        void setMagnetometerXOffset(const short offset);

        /**
         * Returns the magnetometer Y-axis offset in the register data.
         *
         * @return The magnetometer Y-axis offset in the register data.
         */
        short getMagnetometerYOffset();

        /**
         * Sets the magnetometer Y-axis offset in the register data.
         *
         * @param offset    The magnetometer Y-axis offset to set in the
         *                  register data.
         */
        void setMagnetometerYOffset(const short offset);

        /**
         * Returns the magnetometer Z-axis offset in the register data.
         *
         * @return The magnetometer Z-axis offset in the register data.
         */
        short getMagnetometerZOffset();

        /**
         * Sets the magnetometer Z-axis offset in the register data.
         *
         * @param offset    The magnetometer Z-axis offset to set in the
         *                  register data.
         */
        void setMagnetometerZOffset(const short offset);

        /**
         * Returns the accelerometer X-axis offset in the register data.
         *
         * @return The accelerometer X-axis offset in the register data.
         */
        short getAccelerometerXOffset();

        /**
         * Sets the accelerometer X-axis offset in the register data.
         *
         * @param offset    The accelerometer X-axis offset to set in the
         *                  register data.
         */
        void setAccelerometerXOffset(const short offset);

        /**
         * Returns the accelerometer Y-axis offset in the register data.
         *
         * @return The accelerometer Y-axis offset in the register data.
         */
        short getAccelerometerYOffset();

        /**
         * Sets the accelerometer Y-axis offset in the register data.
         *
         * @param offset    The accelerometer Y-axis offset to set in the
         *                  register data.
         */
        void setAccelerometerYOffset(const short offset);

        /**
         * Returns the accelerometer Z-axis offset in the register data.
         *
         * @return The accelerometer Z-axis offset in the register data.
         */
        short getAccelerometerZOffset();

        /**
         * Sets the accelerometer Z-axis offset in the register data.
         *
         * @param offset    The accelerometer Z-axis offset to set in the
         *                  register data.
         */
        void setAccelerometerZOffset(const short offset);

        /**
         * Returns the gyroscope X-axis offset in the register data.
         *
         * @return The gyroscope X-axis offset in the register data.
         */
        short getGyroscopeXOffset();

        /**
         * Sets the gyroscope X-axis offset in the register data.
         *
         * @param offset    The gyroscope X-axis offset to set in the
         *                  register data.
         */
        void setGyroscopeXOffset(const short offset);

        /**
         * Returns the gyroscope Y-axis offset in the register data.
         *
         * @return The gyroscope Y-axis offset in the register data.
         */
        short getGyroscopeYOffset();

        /**
         * Sets the gyroscope Y-axis offset in the register data.
         *
         * @param offset    The gyroscope Y-axis offset to set in the
         *                  register data.
         */
        void setGyroscopeYOffset(const short offset);

        /**
         * Returns the gyroscope Z-axis offset in the register data.
         *
         * @return The gyroscope Z-axis offset in the register data.
         */
        short getGyroscopeZOffset();

        /**
         * Sets the gyroscope Z-axis offset in the register data.
         *
         * @param offset    The gyroscope Z-axis offset to set in the
         *                  register data.
         */
        void setGyroscopeZOffset(const short offset);

        /**
         * Returns the firmware version in the register data.
         *
         * @return The firmware version in the register data.
         */
        short getFirmwareVersion();

        /**
         * Sets the firmware version in the register data.
         *
         * @param version   The firmware version to set in the register data.
         */
        void setFirmwareVersion(const short version);
    };

    /**
     * Converts an array of bytes into an array of shorts.
     * Each short is built from two bytes either in small or big endian format.
     * Note: the length of the byte array should be twice the short array.
     *
     * @param byteArray         The array of bytes.
     * @param shortArray        The array of shorts to populate.
     * @param shortArrayLength  The length of the short array.
     * @param bigEndian         Whether or not the bytes are in big or little
     *                          endian format. Defaults to little endian i.e.
     *                          the least significant byte is first.
     */
    void byteToShort(const unsigned char byteArray[], short shortArray[],
            unsigned int shortArrayLength, bool bigEndian = false);
}

#endif // NUDATA_H
