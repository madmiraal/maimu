// nu Application Programming Interface for recorded data.
//
// (c) 2018 - 2019: Marcel Admiraal

#ifndef NUFILEAPI_H
#define NUFILEAPI_H

#include "nuapi.h"
#include "fileserial.h"

#include <chrono>

#define firmwareVersion 6

namespace nu
{
    class FileAPI : public API
    {
    public:
        /**
         * Default constructor.
         */
        FileAPI(unsigned int imu);

        /**
         * Default destructor.
         */
        virtual ~FileAPI();

        /**
         * Sets the file that contains the recorded serial data.
         * Returns whether or not the connection was successful.
         *
         * @param fileName The filename that contains the recorded data.
         * @return         Whehter or not the connection was successful.
         */
        virtual bool setPort(const ma::Str& fileName);

        /**
         * Swaps the FileSerial that this IMU uses.
         * Returns the FileSerial that was being used.
         *
         * @param fileSerial    The new FileSerial to use.
         * @return              The FileSerail that was being used.
         */
        ma::FileSerial* setFileSerial(ma::FileSerial* fileSerial);

        /**
         * Resets the FileSerial timer.
         */
        void resetFileSerialTimer();

        /**
         * Sends the commands to fetch the imu's register data.
         *
         * @return Whether the commands were successfully sent.
         */
        virtual bool getRegisters();

        /**
         * Sends the commands to set the imu's register data.
         *
         * @param registerData  The new register data.
         * @return              Whether the commands were successfully sent.
         */
        virtual bool setRegisters(ma::Data* registerData);

        /**
         * Sets a register value.
         * Returns true.
         *
         * @param registerNumber    The number of the register to set.
         * @param registerValue     The new value of the register.
         * @return                  Whether or not the send was successful.
         */
        virtual bool setRegister(unsigned int registerNumber,
                short registerValue);

        /**
         * Saves register data to be used.
         * Returns whether or not the save was successful.
         *
         * @return  Whether or not the save was successful.
         */
        virtual bool saveRegistersToFlash();

    protected:
        /**
         * Return a pointer to the next data item if available.
         * Note: The API will take ownership of the data object and destroy it.
         *
         * @return A pointer to the next data item, or zero if no data available.
         */
        virtual ma::Data* getNextData();

        /**
         * Reads up to a line of comma separated byte integers
         * from the file and writes them to the byte array passed.
         * Returns the number of byte integers read.
         *
         * @param buffer       An unsigned char array of bytes to
         *                     write the data read to.
         * @param bufferLength The length of the array
         *                     (the maximum number of bytes to read).
         * @return             The number of bytes in the line read.
         */
        unsigned long readData(unsigned char *buffer,
                const unsigned int bufferLength);

        /**
         * Reads up to a line of comma separated floating point numbers
         * from the file and writes them to the double array passed.
         * Returns the number of floating point numbers read.
         *
         * @param buffer       A double array to write the data read to.
         * @param bufferLength The length of the array
         *                     (the maximum number of bytes to read).
         * @return             The number of bytes in the line read.
         */
        unsigned long readDoubleData(double *buffer,
                const unsigned int bufferLength);

        std::chrono::steady_clock::time_point lastTime;
        unsigned int imu;

    private:
        void setDefaultRegisters();
        void setPeriod();

        ma::FileSerial* fileSerial;
        bool sendRegisterData;
        int period;

        wxMutex fileSerialAccess;
    };
}

#endif // NUFILEAPI_H
