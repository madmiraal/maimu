// Abstract class for listening to packets received from the nu IMU.
//
// (c) 2016: Marcel Admiraal

#ifndef NULISTENER_H
#define NULISTENER_H

#include "imulistener.h"
#include "nudata.h"

namespace nu
{
    class Listener : public virtual ma::IMUListener
    {
    public:
        /**
         * Default destructor.
         */
        virtual ~Listener() {}

        /**
         * Called by the API when a data packet is received from the IMU.
         * Uses the data type to call the appropriate data type received
         * function.
         *
         * @param data  Pointer to the data received.
         */
        virtual void dataReceived(ma::Data* data);

    private:
        void imuDataReceived(ma::Data* data);
        void analogueDataReceived(ma::Data* data);
        void quaternionDataReceived(ma::Data* data);
    };
}

#endif // NULISTENER_H
