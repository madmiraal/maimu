// Abstract base class for sending and receiving packets to and from an IMU.
//
// Each IMU type has a class that extends this class and overrides the two
// virtual functions getPacket and extractData.
// This base class allows calls to be made to an IMU irrespective of the type.
//
// (c) 2016: Marcel Admiraal

#ifndef IMUAPI_H
#define IMUAPI_H

#include <wx/thread.h>

#include "serial.h"
#include "collection.h"
#include "imulistener.h"
#include "imudata.h"

namespace ma
{
    class IMUAPI : public wxThreadHelper
    {
    public:
        /**
         * Default constructor.
         */
        IMUAPI();

        /**
         * Default destructor.
         */
        virtual ~IMUAPI();

        /**
         * Returns the name of the serial port that the IMU is currently
         * connected to.
         *
         * @return The name of the serial port that the IMU is currently
         * connected to.
         */
        virtual Str getPort();

        /**
         * Sets the serial port that the IMU is connected to
         * e.g. COM1 or /dev/tty1.
         * and sets the default port parameters.
         * Returns whether or not the connection was successful.
         *
         * @param portName A string literal of the portname.
         * @return         Whehter or not the connection was successful.
         */
        virtual bool setPort(const Str& portName);

        /**
         * Sets the serial port parameters.
         * Returns whether or not setting the serial port parameters was
         * successful.
         *
         * @param baud     Default: 9600       - The baud rate in bps.
         * @param bits     Default: 8          - The number of bits in a packet.
         * @param parity   Default: NOPARITY   - The number of parity bits.
         * @param stopbits Default: ONESTOPBIT - The number of stop bits.
         * @param block    Default: false      - Whether or not to block until
         *                                       bytes available when reading.
         * @return         Whether or not setting the serial port parameters was
         *                 successful.
         */
        bool setParameters(
            const int baud = 9600,
            const int bits = 8,
            const int stopBits = ONESTOPBIT,
            const int parity = NOPARITY,
            const bool block = false
            );

        /**
         * Returns the connection status.
         * Note: If a connection was attempted it simply returns whether or not
         * the last connection was successful so it may not reflect the acutal
         * status of the serial port.
         *
         * @return  The connection status.
         */
        virtual bool isConnected();

        /**
         * Returns the next byte from the underlying serial port.
         *
         * @return      The next byte from the underlying serial port.
         */
        virtual unsigned char readByte();

        /**
         * Reads bytes from the underlying serial port.
         * Returns whether the required number of bytes were read.
         *
         * @param byteArray     An unsigned char array of bytes to
         *                      write the data read to.
         * @param arrayLength   The length of the byte array.
         *                      The number of bytes to read.
         * @return              Whether or not the bytes were read.
         */
        virtual bool readBytes(unsigned char *byteArray,
                const unsigned int arrayLength);

        /**
         * Sends the bytes in the packet to the underlying serial port.
         * Returns whether or not the packet was successfully sent.
         *
         * @param packet        An unsigned char array of bytes to send.
         * @param packetLength  The length of the packet to send.
         * @return              Whether or not the packet was successfuly sent.
         */
        bool sendPacket(unsigned char* packet, unsigned int packetLength);

        /**
         * Clears the serial port's read buffer.
         */
        void clearReadBuffer();

        /**
         * Clears the serial port's write buffer.
         */
        void clearWriteBuffer();

        /**
         * Adds a listener that responds to received packets.
         *
         * @param listener The listener to add.
         */
        void addListener(IMUListener*);

        /**
         * Removes a listener.
         *
         * @param listener The listener to remove.
         */
        void removeListener(IMUListener*);

        /**
         * Sends the commands to fetch the imu's register data.
         *
         * @return Whether the commands were successfully sent.
         */
        virtual bool getRegisters() = 0;

        /**
         * Sends the commands to set the imu's register data.
         *
         * @param registerData  The new register data.
         * @return              Whether the commands were successfully sent.
         */
        virtual bool setRegisters(ma::Data* registerData) = 0;

        /**
         * Returns the pointer to the underlying Serial object.
         *
         * @return The pointer to the underlying Serial object.
         */
        ma::Serial* getSerial();

    protected:
        /**
         * Read bytes from the underlying serial port and then create and return
         * a pointer to the next data item.
         * Note: The API will take ownership of the data object and destroy it.
         * @return A pointer to the data received, or zero if no data available.
         */
        virtual Data* getNextData() = 0;

    protected:
        Serial* serial;

    private:
        wxThread::ExitCode Entry();
        void informListeners(Data* data);

        Collection<IMUListener*> listenerCollection;

        bool running;
    };
}

#endif // IMUAPI_H
