// Base class for storing IMU Data.
//
// (c) 2016: Marcel Admiraal

#ifndef IMUDATA_H
#define IMUDATA_H

#include "vector.h"
#include "str.h"

#include <iostream>

namespace ma
{
    enum DataType
    {
        NullData,
        MessageData,
        AccelerometerData,
        GyroscopeData,
        MagnetometerData,
        QuaternionData,
        AnalogueData,
        RegisterData,
        CUSTOM_START
    };

    class Data
    {
    public:
        /**
         * Default constructor.
         */
        Data();

        /**
         * Type constructor.
         *
         * @param type  The data type.
         */
        Data(const unsigned int type);

        /**
         * String constructor.
         *
         * @param type  The data type.
         * @param stringData The string data.
         */
        Data(const unsigned int type, const Str& stringData);

        /**
         * Vector constructor.
         *
         * @param type  The data type.
         * @param vectorData The vector data.
         */
        Data(const unsigned int type, const Vector& vectorData);

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        Data(const Data& source);

        /**
         * Default destructor.
         */
        virtual ~Data();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(Data& first, Data& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        Data& operator=(Data source);

        /**
         * Returns the data type.
         *
         * @return The data type.
         */
        unsigned int getDataType() const;

        /**
         * Sets the data type.
         *
         * @param type  The data type.
         */
        void setDataType(const unsigned int type);

        /**
         * Returns the vector data.
         *
         * @return The vector data.
         */
        Vector getVectorData() const;

        /**
         * Sets the vector data.
         *
         * @param data  The vector data.
         */
        void setVectorData(Vector data);

        /**
         * Returns the string data.
         *
         * @return The string data.
         */
        Str getStringData() const;

        /**
         * Sets the string data.
         *
         * @param data  The string data.
         */
        void setStringData(Str data);

        /**
         * Saves the data in the file specified.
         *
         * @param filename  The filename of the file.
         * @return          Whether or not the save was successful.
         */
        bool saveData(const Str& filename);

        /**
         * Loads the data from the file specified.
         *
         * @param filename  The filename of the file.
         * @return          Whether or not the load was successful.
         */
        bool loadData(const Str& filename);

    protected:
        unsigned int type;
        Vector vectorData;
        Str stringData;
    };

    /**
     * Input stream operator.
     * Builds data from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The data to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, Data& destination);

    /**
     * Output stream operator.
     * Appends the data to the output stream and returns the output stream.
     *
     * @param os     The output stream.
     * @param source The data to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Data& source);
}

#endif // IMUDATA_H
