// nu Application Programming Interface for recorded processed data.
//
// (c) 2018 - 2019: Marcel Admiraal

#ifndef NUFILEDATAAPI_H
#define NUFILEDATAAPI_H

#include "nufileapi.h"

namespace nu
{
    class FileDataAPI : public FileAPI
    {
    public:
        /**
         * Default constructor.
         */
        FileDataAPI(unsigned int imu);

        /**
         * Default destructor.
         */
        virtual ~FileDataAPI();

    protected:
        /**
         * Return a pointer to the next data item if available.
         * Note: The API will take ownership of the data object and destroy it.
         *
         * @return A pointer to the next data item, or zero if no data available.
         */
        virtual ma::Data* getNextData();

    private:
        bool getRow();

        double waitPeriod;
        unsigned int rowLength;
        double* rowValue;
    };

    ma::Data* extractIMUAnalogueData(double* rowValue,
            const unsigned int rowLength);
}

#endif // NUFILEDATAAPI_H
