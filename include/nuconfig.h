// wxWidgets Dialog for configuring nu IMU's register values.
//
// (c) 2016 - 2018: Marcel Admiraal

#ifndef NUCONFIG_H
#define NUCONFIG_H

#include <wx/dialog.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/checkbox.h>
#include <wx/combobox.h>

#include "nudata.h"

namespace nu
{
    class Config : public wxDialog
    {
    public:
        /**
         * Constructor.
         *
         * @param parent        The window that owns this dialog.
         * @param registerData  A pointer to the register data.
         */
        Config(wxWindow* parent, RegisterData* registerData);

        /**
         * Default destructor.
         */
        virtual ~Config();

    private:
        void populateValues();
        void textEntered(wxCommandEvent& event);
        void frequencyChanged(wxCommandEvent& event);
        void adcCheckboxChanged(wxCommandEvent& event);
        void comboChanged(wxCommandEvent& event);
        void saveRegistersToFlash(wxCommandEvent& event);
        void loadRegisterSettings(wxCommandEvent& event);
        void saveRegisterSettings(wxCommandEvent& event);

        RegisterData* registerData;

        wxStaticText* firmwareVersionText;
        wxTextCtrl* idText;
        wxTextCtrl* sampleFrequencyText;
        wxStaticText* periodText;
        wxStaticText* prescalerText;
        wxTextCtrl* skipText;
        wxStaticText* adcFilterText;
        wxCheckBox** adcCheckboxArray;

        wxComboBox* accelerometerScaleCombo;
        wxComboBox* accelerometerRateCombo;
        wxComboBox* gyroscopeScaleCombo;
        wxComboBox* gyroscopeRateCombo;
        wxComboBox* magnetometerScaleCombo;
        wxComboBox* magnetometerRateCombo;

        wxTextCtrl* accelerometerXOffsetText;
        wxTextCtrl* accelerometerYOffsetText;
        wxTextCtrl* accelerometerZOffsetText;
        wxTextCtrl* gyroscopeXOffsetText;
        wxTextCtrl* gyroscopeYOffsetText;
        wxTextCtrl* gyroscopeZOffsetText;
        wxTextCtrl* magnetometerXOffsetText;
        wxTextCtrl* magnetometerYOffsetText;
        wxTextCtrl* magnetometerZOffsetText;
    };

    void populateByteCheckboxes(wxCheckBox** checkBoxArray, short value);
    short getByteValue(wxCheckBox** checkBoxArray);
    wxString getPreScalerString(unsigned int index);
}

#endif // NUCONFIG_H
