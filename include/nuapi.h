// nu Application Programming Interface.
//
// (c) 2016 - 2018: Marcel Admiraal

#ifndef NUAPI_H
#define NUAPI_H

#include "imuapi.h"
#include "nudata.h"
#include "str.h"

#define DEFAULT_ANALOG_BITS 6

namespace nu
{
    class API : public ma::IMUAPI
    {
    public:
        /**
         * Default constructor.
         */
        API();

        /**
         * Default destructor.
         */
        virtual ~API();

        /**
         * Sends a command to the nu.
         * Returns whether or not the send was successful.
         *
         * @param command   The command to send.
         * @return          Whether or not the send was successful.
         */
        bool sendCommand(const ma::Str& command);

        /**
         * Sends the command to the nu to stop sending.
         * Returns whether or not the send was successful.
         *
         * @return  Whether or not the send was successful.
         */
        bool stopSending();

        /**
         * Sends the command to the nu to start sending IMU data.
         * Returns whether or not the send was successful.
         *
         * @return  Whether or not the send was successful.
         */
        bool startSendingIMUData();

        /**
         * Sends the command to the nu to start sending quaternions.
         * Returns whether or not the send was successful.
         *
         * @return  Whether or not the send was successful.
         */
        bool startSendingQuaternions();

        /**
         * Sends the command to the nu to test the accelerometer and gyroscope.
         * Returns whether or not the send was successful.
         *
         * @return  Whether or not the send was successful.
         */
        bool testDevice();

        /**
         * Sends the commands to fetch the imu's register data.
         *
         * @return Whether the commands were successfully sent.
         */
        virtual bool getRegisters();

        /**
         * Sends the commands to set the imu's register data.
         *
         * @param registerData  The new register data.
         * @return              Whether the commands were successfully sent.
         */
        virtual bool setRegisters(ma::Data* registerData);

        /**
         * Sends the command to set a register value.
         * Returns whether or not the send was successful.
         *
         * @param registerNumber    The number of the register to set.
         * @param registerValue     The new value of the register.
         * @return                  Whether or not the send was successful.
         */
        virtual bool setRegister(unsigned int registerNumber, short registerValue);

        /**
         * Sends the command to the nu to save registers to flash.
         * Returns whether or not the send was successful.
         *
         * @return  Whether or not the send was successful.
         */
        virtual bool saveRegistersToFlash();

        /**
         * Returns the number of bits used by the analogue signal.
         *
         * @return The number of bits used by the analogue signal.
         */
        unsigned int getAnalogueBits() const;

        /**
         * Sets the number of bits used by the analogue signal.
         *
         * @param bits  The number of bits to be used by the analogue signal.
         */
        void setAnalogueBits(const unsigned int bits);

        /**
         * Returns the maximum accelerometer value - the scale setting in g.
         *
         * @return The maximum accelerometer value.
         */
        double getAccelerometerScale();

        /**
         * Returns the maximum gyroscope value - the scale setting in degrees
         * per second.
         *
         * @return The maximum gryoscope value.
         */
        double getGyroscopeScale();

        /**
         * Returns the maximum magnetometer value - the scale setting in gauss.
         *
         * @return The maximum magnetometer value.
         */
        double getMagnetometerScale();

    protected:
        /**
         * Read bytes from the underlying serial port and then create and return
         * a pointer to the next data item.
         * Note: The API will take ownership of the data object and destroy it.
         * @return A pointer to the data received, or zero if no data available.
         */
        virtual ma::Data* getNextData();

        void adjustIMUData(ma::Data* data);
        void adjustAnalogueData(ma::Data* data);

        RegisterData registerData;
        unsigned int analogueBits;

    private:
        /**
         * Reads bytes into the packet array and populates the packetLength.
         *
         * @param packet        The array to populate with the packet bytes.
         * @param packetLength  The length of the packet read.
         */
        void getPacket(unsigned char* packet, unsigned int& packetLength);

        /**
         * Extracts the data from the packet populated with the getPacket
         * function, and returns a pointer to the data.
         *
         * @param packet        The array with the packet.
         * @param packetLength  The length of the packet.
         * @return              A pointer to the data received.
         */
        ma::Data* extractData(const unsigned char* packet,
                const unsigned int packetLength);

        void copyRegisterData(ma::Data* data);
    };

    ma::Data* extractMessageData(const unsigned char* packet,
            const unsigned int packetLength);
    ma::Data* extractNumberData(const unsigned char* packet,
            const unsigned int packetLength);
    ma::Data* extractIMUData(const unsigned char* packet,
            const unsigned int packetLength);
    ma::Data* extractQuaternionData(const unsigned char* packet,
            const unsigned int packetLength);
    ma::Data* extractRegisterData(const unsigned char* packet,
            const unsigned int packetLength);
    ma::Data* receivedFinalData(const unsigned char* packet,
            const unsigned int packetLength);
}

#endif // NUAPI_H
