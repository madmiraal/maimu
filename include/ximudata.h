// Classes for storing x-IMU Data.
//
// (c) 2013 - 2016: Marcel Admiraal

#ifndef XIMUDATA_H
#define XIMUDATA_H

#include "ximuenum.h"

#include "imudata.h"

#include <string>

namespace xIMU
{
    class ErrorData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        ErrorData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        ErrorData(const ErrorData& source);

        /**
         * Error code constructor.
         *
         * @param code The error code.
         */
        ErrorData(const ErrorCode errorCode);

        /**
         * Short constructor.
         *
         * @param code The short value of the error code.
         */
        ErrorData(const unsigned short errorCode);

        /**
         * Default destructor.
         */
        virtual ~ErrorData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(ErrorData& first, ErrorData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        ErrorData& operator=(ErrorData source);

        /**
         * Returns the error code.
         *
         * @return The error code.
         */
        ErrorCode getErrorCode() const;

        /**
         * Sets the error code.
         *
         * @param errorCode The error code to set to.
         */
        void setErrorCode(const ErrorCode errorCode);

        /**
         * Sets the error code.
         *
         * @param errorCode The short value of the error code.
         */
        void setErrorCode(const unsigned short errorCode);

        /**
         * Returns an explanation of the error.
         *
         * @return An explanation of the error.
         */
        std::string getMessage() const;

    private:
        ErrorCode errorCode;
    };

    class CommandData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        CommandData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        CommandData(const CommandData& source);

        /**
         * Command code constructor.
         *
         * @param code The command code.
         */
        CommandData(const CommandCode commandCode);

        /**
         * Short constructor.
         *
         * @param code The short value of the command code.
         */
        CommandData(const unsigned short commandCode);

        /**
         * Default destructor.
         */
        virtual ~CommandData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(CommandData& first, CommandData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        CommandData& operator=(CommandData source);

        /**
         * Returns the command code.
         *
         * @return The command code.
         */
        CommandCode getCommandCode() const;

        /**
         * Sets the command code.
         *
         * @param commandCode The command code to set to.
         */
        void setCommandCode(const CommandCode commandCode);

        /**
         * Sets the command code.
         *
         * @param commandCode The short value of the command code to set to.
         */
        void setCommandCode(const unsigned short commandCode);

        /**
         * Returns an explanation of the command.
         *
         * @return An explanation of the command.
         */
        std::string getMessage() const;

    private:
        CommandCode commandCode;
    };

    class RegisterData : public ma::Data
    {
    public:
        /**
         * Copy constructor.
         *
         * @param source The matrix to copy.
         */
        RegisterData(const RegisterData& source);

        /**
         * Zero register data constructor.
         *
         * @param address The register's address.
         */
        RegisterData(const RegisterAddress address);

        /**
         * Float value register data constructor.
         *
         * @param address The register's address.
         * @param value   The float value to assign to the register.
         */
        RegisterData(const RegisterAddress address, const float value);

        /**
         * Short value register data constructor.
         *
         * @param address The register's address.
         * @param value   The short value to assign to the register.
         */
        RegisterData(const unsigned short address, const unsigned short value);

        /**
         * Default destructor.
         */
        virtual ~RegisterData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(RegisterData& first, RegisterData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        RegisterData& operator=(RegisterData source);

        /**
         * Returns the register address.
         *
         * @return The register address.
         */
        RegisterAddress getAddress() const;

        /**
         * Returns the register value.
         *
         * @return The register value.
         */
        unsigned short getValue() const;

        /**
         * Returns the float interpretation of the register value.
         *
         * @return The float interpretation of the register value.
         */
        float getFloatValue() const;

        /**
         * Sets the register address.
         *
         * @param address The register address.
         */
        void setAddress(const RegisterAddress address);

        /**
         * Sets the value.
         *
         * @param value The value.
         */
        void setValue(const unsigned short value);

        /**
         * Sets the value to an encoded version of the float.
         *
         * @param value The float value.
         */
        void setFloatValue(const float value);

        /**
         * Sets both the short value of the register address and the value.
         *
         * @param address The register address.
         * @param value   The value.
         */
        void setData(const unsigned short address, const unsigned short value);

    private:
        // Return the qValue associated with this register.
        QValue getQValue(const RegisterAddress) const;
        RegisterAddress address;
        unsigned short value;
    };

    class DateTimeData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        DateTimeData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        DateTimeData(const DateTimeData& source);

        /**
         * Data and time values constructor.
         *
         * @param year   The year of the date.
         * @param month  The month of the date.
         * @param day    The day of the date.
         * @param hour   The hours of the time.
         * @param minute The minutes of the time.
         * @param second The seconds of the time.
         */
        DateTimeData(const int year, const int month, const int day,
                const int hour, const int minute, const int second);

        /**
         * Default destructor.
         */
        virtual ~DateTimeData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(DateTimeData& first, DateTimeData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        DateTimeData& operator=(DateTimeData source);

        /**
         * Returns the year.
         *
         * @return The year.
         */
        int getYear() const;

        /**
         * Returns the month.
         *
         * @return The month.
         */
        int getMonth() const;

        /**
         * Returns the day.
         *
         * @return The day.
         */
        int getDay() const;

        /**
         * Returns the hour.
         *
         * @return The hour.
         */
        int getHour() const;

        /**
         * Returns the minute.
         *
         * @return The minute.
         */
        int getMinute() const;

        /**
         * Returns the second.
         *
         * @return The second.
         */
        int getSecond() const;

        /**
         * Sets the year.
         *
         * @param year The year.
         */
        void setYear(const int year);

        /**
         * Sets the month.
         *
         * @param month The month.
         */
        void setMonth(const int month);

        /**
         * Sets the day.
         *
         * @param day The day.
         */
        void setDay(const int day);

        /**
         * Sets the hour.
         *
         * @param hour The hour.
         */
        void setHour(const int hour);

        /**
         * Sets the minute.
         *
         * @param minute The minute.
         */
        void setMinute(const int minute);

        /**
         * Sets the second.
         *
         * @param second The second.
         */
        void setSecond(const int second);

        /**
         * Sets the date and time.
         *
         * @param year The year.
         * @param month The month.
         * @param day The day.
         * @param hour The hour.
         * @param minute The minute.
         * @param second The second.
         */
        void setDateTime(const int year, const int month, const int day,
                const int hour, const int minute, const int second);

    private:
        int year;
        int month;
        int day;
        int hour;
        int minute;
        int second;
    };

    class RawBatteryAndTemperatureData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        RawBatteryAndTemperatureData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        RawBatteryAndTemperatureData(
            const RawBatteryAndTemperatureData& source);

        /**
         * Short constructor.
         *
         * @param batteryVoltage The raw battery voltage value.
         * @param temperature    The raw temperature value.
         */
        RawBatteryAndTemperatureData(
            const short batteryVoltage, const short temperature);

        /**
         * Default destructor.
         */
        virtual ~RawBatteryAndTemperatureData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(RawBatteryAndTemperatureData& first,
            RawBatteryAndTemperatureData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        RawBatteryAndTemperatureData& operator=(
            RawBatteryAndTemperatureData source);

        /**
         * Returns the raw battery voltage value.
         *
         * @return The raw battery voltage value.
         */
        short getBatteryVoltage() const;

        /**
         * Returns the raw temperature value.
         *
         * @return The raw temperature value.
         */
        short getTemperature() const;

        /**
         * Sets the raw battery voltage value.
         *
         * @param batteryVoltage The raw battery voltage value.
         */
        void setBatteryVoltage(const short batteryVoltage);

        /**
         * Sets the raw temperature value.
         *
         * @param temperature The raw temperature value.
         */
        void setTemperature(const short temperature);

    private:
        short batteryVoltage;
        short temperature;
    };

    class CalBatteryAndTemperatureData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        CalBatteryAndTemperatureData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        CalBatteryAndTemperatureData(
            const CalBatteryAndTemperatureData& source);

        /**
         * Float constructor.
         *
         * @param batteryVoltage The calibrated battery voltage value.
         * @param temperature    The calibrated temperature value.
         */
        CalBatteryAndTemperatureData(const float batteryVoltage,
                const float temperature);

        /**
         * Default destructor.
         */
        virtual ~CalBatteryAndTemperatureData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(CalBatteryAndTemperatureData& first,
            CalBatteryAndTemperatureData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        CalBatteryAndTemperatureData& operator=(
            CalBatteryAndTemperatureData source);

        /**
         * Returns the calibrated battery voltage value.
         *
         * @return The calibrated battery voltage value.
         */
        float getBatteryVoltage() const;

        /**
         * Returns the calibrated temperature value.
         *
         * @return The calibrated temperature value.
         */
        float getTemperature() const;

        /**
         * Sets the calibrated battery voltage value.
         *
         * @param batteryVoltage The calibrated battery voltage value.
         */
        void setBatteryVoltage(const float batteryVoltage);

        /**
         * Sets the calibrated temperature value.
         *
         * @param temperature The calibrated temperature value.
         */
        void setTemperature(const float temperature);

    private:
        float batteryVoltage;
        float temperature;
    };

    class RawInertialAndMagneticData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        RawInertialAndMagneticData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        RawInertialAndMagneticData(const RawInertialAndMagneticData& source);

        /**
         * Short constructor.
         *
         * @param gyroscope     A three dimensional array of
         *                      the raw gyroscope values.
         * @param accelerometer A three dimensional array of
         *                      the raw accelerometer values.
         * @param magnetometer  A three dimensional array of
         *                      the raw magnetometer values.
         */
        RawInertialAndMagneticData(
            const short gyroscope[],
            const short accelerometer[],
            const short magnetometer[]
        );

        /**
         * Default destructor.
         */
        virtual ~RawInertialAndMagneticData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(RawInertialAndMagneticData& first,
            RawInertialAndMagneticData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        RawInertialAndMagneticData& operator=(
            RawInertialAndMagneticData source);

        /**
         * Populates the three dimensional array with the raw gyroscope data.
         *
         * @param gyroscope The three dimensional array to populate.
         */
        void getGyroscope(short gyroscope[]) const;

        /**
         * Populates the three dimensional array with the raw accelerometer data.
         *
         * @param gyroscope The three dimensional array to populate.
         */
        void getAccelerometer(short accelerometer[]) const;

        /**
         * Populates the three dimensional array with the raw magnetometer data.
         *
         * @param gyroscope The three dimensional array to populate.
         */
        void getMagnetometer(short magnetometer[]) const;

        /**
         * Sets the raw gyroscope data.
         *
         * @param gyroscope The three dimensional array of raw gyroscope data.
         */
        void setGyroscope(const short gyroscope[]);

        /**
         * Sets the raw accelerometer data.
         *
         * @param accelerometer The three dimensional array of
         *                      raw accelerometer data.
         */
        void setAccelerometer(const short accelerometer[]);

        /**
         * Sets the raw magnetometer data.
         *
         * @param magnetometer The three dimensional array of
         *                     raw magnetometer data.
         */
        void setMagnetometer(const short magnetometer[]);

    private:
        short gyroscope[3];
        short accelerometer[3];
        short magnetometer[3];
    };

    class CalInertialAndMagneticData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        CalInertialAndMagneticData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        CalInertialAndMagneticData(const CalInertialAndMagneticData& source);

        /**
         * Float constructor.
         *
         * @param gyroscope     A three dimensional array of
         *                      calibrated gyroscope values.
         * @param accelerometer A three dimensional array of
         *                      calibrated accelerometer values.
         * @param magnetometer  A three dimensional array of
         *                      calibrated magnetometer values.
         */
        CalInertialAndMagneticData(
            const float gyroscope[],
            const float accelerometer[],
            const float magnetometer[]
        );

        /**
         * Default destructor.
         */
        virtual ~CalInertialAndMagneticData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(CalInertialAndMagneticData& first,
            CalInertialAndMagneticData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        CalInertialAndMagneticData& operator=(
            CalInertialAndMagneticData source);

        /**
         * Populates the three dimensional array with
         * the calibrated gyroscope data.
         *
         * @param gyroscope The three dimensional array to populate.
         */
        void getGyroscope(float gyroscope[]) const;

        /**
         * Populates the three dimensional array with
         * the calibrated accelerometer data.
         *
         * @param accelerometer The three dimensional array to populate.
         */
        void getAccelerometer(float accelerometer[]) const;

        /**
         * Populates the three dimensional array with
         * the calibrated magnetometer data.
         *
         * @param magnetometer  The three dimensional array to populate.
         */
        void getMagnetometer(float magnetometer[]) const;

        /**
         * Sets the calibrated gyroscope data.
         *
         * @param gyroscope The three dimensional array of
         *                  calibrated gyroscope data.
         */
        void setGyroscope(const float gyroscope[]);

        /**
         * Sets the calibrated accelerometer data.
         *
         * @param accelerometer The three dimensional array of
         *                      calibrated accelerometer data.
         */
        void setAccelerometer(const float accelerometer[]);

        /**
         * Sets the calibrated magnetometer data.
         *
         * @param magnetometer The three dimensional array of
         *                     calibrated magnetometer data.
         */
        void setMagnetometer(const float magnetometer[]);

    private:
        float gyroscope[3];
        float accelerometer[3];
        float magnetometer[3];
    };

    class QuaternionData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        QuaternionData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        QuaternionData(const QuaternionData& source);

        /**
         * Float constructor.
         *
         * @param quaternion A four dimensional array representing
         *                   the quaterion values.
         */
        QuaternionData(const float quaternion[]);

        /**
         * Default destructor.
         */
        virtual ~QuaternionData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(QuaternionData& first, QuaternionData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        QuaternionData& operator=(QuaternionData source);

        /**
         * Populates the four dimensional array with the quaternion data.
         *
         * @param gyroscope The four dimensional array to populate.
         */
        void getQuaternion(float quaternion[]) const;

        /**
         * Sets the quaternion data.
         *
         * @param gyroscope The four dimensional array of quaternion data.
         */
        void setQuaternion(const float quaternion[]);

    private:
        float quaternion[4];
    };

    class DigitalIOData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        DigitalIOData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        DigitalIOData(const DigitalIOData& source);

        /**
         * Direction and state constructor.
         *
         * @param direction The channels (bits) to set as
         *                  input (1) and output (0).
         * @param state     The output channels to set
         *                  high (1) or low (0).
         */
        DigitalIOData(const unsigned char direction, const unsigned char state);

        /**
         * Default destructor.
         */
        virtual ~DigitalIOData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(DigitalIOData& first, DigitalIOData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        DigitalIOData& operator=(DigitalIOData source);

        /**
         * Returns the direction byte.
         *
         * @return The direction byte.
         */
        unsigned char getDirection() const;

        /**
         * Returns the state byte.
         *
         * @return The state byte.
         */
        unsigned char getState() const;

        /**
         * Sets the direction byte.
         *
         * @param direction The direction byte.
         */
        void setDirection(const unsigned char);

        /**
         * Sets the state byte.
         *
         * @param state The state byte.
         */
        void setState(const unsigned char);

    private:
        unsigned char direction;
        unsigned char state;
    };

    class RawAnalogueInputData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        RawAnalogueInputData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        RawAnalogueInputData(const RawAnalogueInputData& source);

        /**
         * Short constructor.
         *
         * @param AX0 The raw value of the first channel.
         * @param AX1 The raw value of the second channel.
         * @param AX2 The raw value of the third channel.
         * @param AX3 The raw value of the fourth channel.
         * @param AX4 The raw value of the fifth channel.
         * @param AX5 The raw value of the sixth channel.
         * @param AX6 The raw value of the seventh channel.
         * @param AX7 The raw value of the eighth channel.
         */
        RawAnalogueInputData(
            const short AX0, const short AX1, const short AX2, const short AX3,
            const short AX4, const short AX5, const short AX6, const short AX7);

        /**
         * Default destructor.
         */
        virtual ~RawAnalogueInputData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(
            RawAnalogueInputData& first, RawAnalogueInputData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        RawAnalogueInputData& operator=(RawAnalogueInputData source);

        /**
         * Returns the raw value of the first channel.
         *
         * @return The raw value of the first channel.
         */
        short getAX0() const;

        /**
         * Returns the raw value of the second channel.
         *
         * @return The raw value of the second channel.
         */
        short getAX1() const;

        /**
         * Returns the raw value of the third channel.
         *
         * @return The raw value of the third channel.
         */
        short getAX2() const;

        /**
         * Returns the raw value of the fourth channel.
         *
         * @return The raw value of the fourth channel.
         */
        short getAX3() const;

        /**
         * Returns the raw value of the fifth channel.
         *
         * @return The raw value of the fifth channel.
         */
        short getAX4() const;

        /**
         * Returns the raw value of the sixth channel.
         *
         * @return The raw value of the sixth channel.
         */
        short getAX5() const;

        /**
         * Returns the raw value of the seventh channel.
         *
         * @return The raw value of the seventh channel.
         */
        short getAX6() const;

        /**
         * Returns the raw value of the eighth channel.
         *
         * @return The raw value of the eighth channel.
         */
        short getAX7() const;

        /**
         * Populates the array with the channel data.
         *
         * @param The array to populate.
         */
        void getAX(short axData[]) const;

        /**
         * Sets the raw value of the first channel.
         *
         * @param AX0 The raw value of the first channel.
         */
        void setAX0(const short AX0);

        /**
         * Sets the raw value of the second channel.
         *
         * @param AX1 The raw value of the second channel.
         */
        void setAX1(const short AX1);

        /**
         * Sets the raw value of the third channel.
         *
         * @param AX2 The raw value of the third channel.
         */
        void setAX2(const short AX2);

        /**
         * Sets the raw value of the fourth channel.
         *
         * @param AX3 The raw value of the fourth channel.
         */
        void setAX3(const short AX3);

        /**
         * Sets the raw value of the fifth channel.
         *
         * @param AX4 The raw value of the fifth channel.
         */
        void setAX4(const short AX4);

        /**
         * Sets the raw value of the sixth channel.
         *
         * @param AX5 The raw value of the sixth channel.
         */
        void setAX5(const short AX5);

        /**
         * Sets the raw value of the seventh channel.
         *
         * @param AX6 The raw value of the seventh channel.
         */
        void setAX6(const short AX6);

        /**
         * Sets the raw value of the eighth channel.
         *
         * @param AX7 The raw value of the eighth channel.
         */
        void setAX7(const short AX7);

        /**
         * Sets the raw value of all the channel.
         *
         * @param AX0 The raw value of the first channel.
         * @param AX1 The raw value of the second channel.
         * @param AX2 The raw value of the third channel.
         * @param AX3 The raw value of the fourth channel.
         * @param AX4 The raw value of the fifth channel.
         * @param AX5 The raw value of the sixth channel.
         * @param AX6 The raw value of the seventh channel.
         * @param AX7 The raw value of the eighth channel.
         */
        void setAX(
            const short AX0, const short AX1, const short AX2, const short AX3,
            const short AX4, const short AX5, const short AX6, const short AX7);

    private:
        short AX0;
        short AX1;
        short AX2;
        short AX3;
        short AX4;
        short AX5;
        short AX6;
        short AX7;
    };

    class CalAnalogueInputData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        CalAnalogueInputData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        CalAnalogueInputData(const CalAnalogueInputData& source);

        /**
         * Float constructor.
         *
         * @param AX0 The calibrated value of the first channel.
         * @param AX1 The calibrated value of the second channel.
         * @param AX2 The calibrated value of the third channel.
         * @param AX3 The calibrated value of the fourth channel.
         * @param AX4 The calibrated value of the fifth channel.
         * @param AX5 The calibrated value of the sixth channel.
         * @param AX6 The calibrated value of the seventh channel.
         * @param AX7 The calibrated value of the eighth channel.
         */
        CalAnalogueInputData(
            const float AX0, const float AX1, const float AX2, const float AX3,
            const float AX4, const float AX5, const float AX6, const float AX7);

        /**
         * Default destructor.
         */
        virtual ~CalAnalogueInputData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(CalAnalogueInputData& first,
            CalAnalogueInputData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        CalAnalogueInputData& operator=(CalAnalogueInputData source);

        /**
         * Returns the calibrated value of the first channel.
         *
         * @return The calibrated value of the first channel.
         */
        float getAX0() const;

        /**
         * Returns the calibrated value of the second channel.
         *
         * @return The calibrated value of the second channel.
         */
        float getAX1() const;

        /**
         * Returns the calibrated value of the third channel.
         *
         * @return The calibrated value of the third channel.
         */
        float getAX2() const;

        /**
         * Returns the calibrated value of the fourth channel.
         *
         * @return The calibrated value of the fourth channel.
         */
        float getAX3() const;

        /**
         * Returns the calibrated value of the fifth channel.
         *
         * @return The calibrated value of the fifth channel.
         */
        float getAX4() const;

        /**
         * Returns the calibrated value of the sixth channel.
         *
         * @return The calibrated value of the sixth channel.
         */
        float getAX5() const;

        /**
         * Returns the calibrated value of the seventh channel.
         *
         * @return The calibrated value of the seventh channel.
         */
        float getAX6() const;

        /**
         * Returns the calibrated value of the eighth channel.
         *
         * @return The calibrated value of the eighth channel.
         */
        float getAX7() const;

        /**
         * Populates the array with the channel data.
         *
         * @param The array to populate.
         */
        void getAX(float axData[]) const;

        /**
         * Sets the calibrated value of the first channel.
         *
         * @param AX0 The calibrated value of the first channel.
         */
        void setAX0(const float AX0);

        /**
         * Sets the calibrated value of the second channel.
         *
         * @param AX1 The calibrated value of the second channel.
         */
        void setAX1(const float AX1);

        /**
         * Sets the calibrated value of the third channel.
         *
         * @param AX2 The calibrated value of the third channel.
         */
        void setAX2(const float AX2);

        /**
         * Sets the calibrated value of the fourth channel.
         *
         * @param AX3 The calibrated value of the fourth channel.
         */
        void setAX3(const float AX3);

        /**
         * Sets the calibrated value of the fifth channel.
         *
         * @param AX4 The calibrated value of the fifth channel.
         */
        void setAX4(const float AX4);

        /**
         * Sets the calibrated value of the sixth channel.
         *
         * @param AX5 The calibrated value of the sixth channel.
         */
        void setAX5(const float AX5);

        /**
         * Sets the calibrated value of the seventh channel.
         *
         * @param AX6 The calibrated value of the seventh channel.
         */
        void setAX6(const float AX6);

        /**
         * Sets the calibrated value of the eighth channel.
         *
         * @param AX7 The calibrated value of the eighth channel.
         */
        void setAX7(const float AX7);

        /**
         * Sets the calibrated value of all the channel.
         *
         * @param AX0 The calibrated value of the first channel.
         * @param AX1 The calibrated value of the second channel.
         * @param AX2 The calibrated value of the third channel.
         * @param AX3 The calibrated value of the fourth channel.
         * @param AX4 The calibrated value of the fifth channel.
         * @param AX5 The calibrated value of the sixth channel.
         * @param AX6 The calibrated value of the seventh channel.
         * @param AX7 The calibrated value of the eighth channel.
         */
        void setAX(
            const float AX0, const float AX1, const float AX2, const float AX3,
            const float AX4, const float AX5, const float AX6, const float AX7);

    private:
        float AX0;
        float AX1;
        float AX2;
        float AX3;
        float AX4;
        float AX5;
        float AX6;
        float AX7;
    };

    class PWMOutputData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        PWMOutputData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        PWMOutputData(const PWMOutputData& source);

        /**
         * Short constructor.
         *
         * @param AX0 The value of the first channel.
         * @param AX2 The value of the second channel.
         * @param AX4 The value of the third channel.
         * @param AX6 The value of the fourth channel.
         */
        PWMOutputData(
            const unsigned short AX0, const unsigned short AX2,
            const unsigned short AX4, const unsigned short AX6);

        /**
         * Default destructor.
         */
        virtual ~PWMOutputData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(PWMOutputData& first, PWMOutputData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        PWMOutputData& operator=(PWMOutputData source);

        /**
         * Returns the value of the first channel.
         *
         * @return The value of the first channel.
         */
        unsigned short getAX0() const;

        /**
         * Returns the value of the second channel.
         *
         * @return The value of the second channel.
         */
        unsigned short getAX2() const;

        /**
         * Returns the value of the third channel.
         *
         * @return The value of the third channel.
         */
        unsigned short getAX4() const;

        /**
         * Returns the value of the fourth channel.
         *
         * @return The value of the fourth channel.
         */
        unsigned short getAX6() const;

        /**
         * Sets the value of the first channel.
         *
         * @param AX0 The value of the first channel.
         */
        void setAX0(const unsigned short AX0);

        /**
         * Sets the value of the second channel.
         *
         * @param AX2 The value of the second channel.
         */
        void setAX2(const unsigned short AX2);

        /**
         * Sets the value of the third channel.
         *
         * @param AX4 The value of the third channel.
         */
        void setAX4(const unsigned short AX4);

        /**
         * Sets the value of the fourth channel.
         *
         * @param AX6 The value of the fourth channel.
         */
        void setAX6(const unsigned short AX6);

        /**
         * Sets the value of all the channels.
         *
         * @param AX0 The value of the first channel.
         * @param AX2 The value of the first channel.
         * @param AX4 The value of the first channel.
         * @param AX6 The value of the first channel.
         */
        void setAX(
            const unsigned short AX0, const unsigned short AX2,
            const unsigned short AX4, const unsigned short AX6);

    private:
        unsigned short AX0;
        unsigned short AX2;
        unsigned short AX4;
        unsigned short AX6;
    };

    class RawADXL345BusData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        RawADXL345BusData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        RawADXL345BusData(const RawADXL345BusData& source);

        /**
         * Short array constructor.
         *
         * @param ADXL345A The three dimensional array of
         *                 raw values for the first channel.
         * @param ADXL345B The three dimensional array of
         *                 raw values for the second channel.
         * @param ADXL345C The three dimensional array of
         *                 raw values for the third channel.
         * @param ADXL345D The three dimensional array of
         *                 raw values for the fourth channel.
         */
        RawADXL345BusData(
            // Arrays of size 3
            const short ADXL345A[], const short ADXL345B[],
            const short ADXL345C[], const short ADXL345D[]);

        /**
         * Default destructor.
         */
        virtual ~RawADXL345BusData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(RawADXL345BusData& first, RawADXL345BusData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        RawADXL345BusData& operator=(RawADXL345BusData source);

        /**
         * Populates the three dimensional array with
         * the first channel's raw values.
         *
         * @param ADXL345A The three dimensional array to populate.
         */
        void getADXL345A(short ADXL345A[]) const;

        /**
         * Populates the three dimensional array with
         * the second channel's raw values.
         *
         * @param ADXL345B The three dimensional array to populate.
         */
        void getADXL345B(short ADXL345B[]) const;

        /**
         * Populates the three dimensional array with
         * the third channel's raw values.
         *
         * @param ADXL345C The three dimensional array to populate.
         */
        void getADXL345C(short ADXL345C[]) const;

        /**
         * Populates the three dimensional array with
         * the fourth channel's raw values.
         *
         * @param ADXL345D The three dimensional array to populate.
         */
        void getADXL345D(short ADXL345D[]) const;

        /**
         * Sets the first channel's raw values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the first channel's raw values.
         */
        void setADXL345A(const short ADXL345A[]);

        /**
         * Sets the second channel's raw values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the second channel's raw values.
         */
        void setADXL345B(const short ADXL345B[]);

        /**
         * Sets the third channel's raw values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the third channel's raw values.
         */
        void setADXL345C(const short ADXL345C[]);

        /**
         * Sets the fourth channel's raw values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the fourth channel's raw values.
         */
        void setADXL345D(const short ADXL345D[]);

        /**
         * Sets all the channels' raw values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the first channel's raw values.
         * @param ADXL345B The three dimensional array of
         *                 the second channel's raw values.
         * @param ADXL345C The three dimensional array of
         *                 the third channel's raw values.
         * @param ADXL345D The three dimensional array of
         *                 the fourth channel's raw values.
         */
        void setADXL345(
            const short ADXL345A[], const short ADXL345B[],
            const short ADXL345C[], const short ADXL345D[]);

    private:
        short ADXL345A[3];
        short ADXL345B[3];
        short ADXL345C[3];
        short ADXL345D[3];
    };

    class CalADXL345BusData : public ma::Data
    {
    public:
        /**
         * Default constructor.
         */
        CalADXL345BusData();

        /**
         * Copy constructor.
         *
         * @param source The data to copy.
         */
        CalADXL345BusData(const CalADXL345BusData& source);

        /**
         * Float array constructor.
         *
         * @param ADXL345A The three dimensional array of
         *                 calibrated values for the first channel.
         * @param ADXL345B The three dimensional array of
         *                 calibrated values for the second channel.
         * @param ADXL345C The three dimensional array of
         *                 calibrated values for the third channel.
         * @param ADXL345D The three dimensional array of
         *                 calibrated values for the fourth channel.
         */
        CalADXL345BusData(
            const float ADXL345A[], const float ADXL345B[],
            const float ADXL345C[], const float ADXL345D[]);

        /**
         * Default destructor.
         */
        virtual ~CalADXL345BusData();

        /**
         * Swap function.
         *
         * @param first  The first data to swap with.
         * @param second The second data to swap with.
         */
        friend void swap(CalADXL345BusData& first,
            CalADXL345BusData& second);

        /**
         * Assignment operator.
         * Returns a copy of the data.
         *
         * @param source The data to copy.
         * @return       A copy of the data.
         */
        CalADXL345BusData& operator=(CalADXL345BusData source);

        /**
         * Populates the three dimensional array with
         * the first channel's calibrated values.
         *
         * @param ADXL345A The three dimensional array to populate.
         */
        void getADXL345A(float ADXL345A[]) const;

        /**
         * Populates the three dimensional array with
         * the second channel's calibrated values.
         *
         * @param ADXL345B The three dimensional array to populate.
         */
        void getADXL345B(float ADXL345B[]) const;

        /**
         * Populates the three dimensional array with
         * the third channel's calibrated values.
         *
         * @param ADXL345C The three dimensional array to populate.
         */
        void getADXL345C(float ADXL345C[]) const;

        /**
         * Populates the three dimensional array with
         * the fourth channel's calibrated values.
         *
         * @param ADXL345D The three dimensional array to populate.
         */
        void getADXL345D(float ADXL345D[]) const;

        /**
         * Sets the first channel's calibrated values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the first channel's calibrated values.
         */
        void setADXL345A(const float ADXL345A[]);

        /**
         * Sets the second channel's calibrated values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the second channel's calibrated values.
         */
        void setADXL345B(const float ADXL345B[]);

        /**
         * Sets the third channel's calibrated values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the third channel's calibrated values.
         */
        void setADXL345C(const float ADXL345C[]);

        /**
         * Sets the fourth channel's calibrated values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the fourth channel's calibrated values.
         */
        void setADXL345D(const float ADXL345D[]);

        /**
         * Sets all the channels' calibrated values.
         *
         * @param ADXL345A The three dimensional array of
         *                 the first channel's calibrated values.
         * @param ADXL345B The three dimensional array of
         *                 the second channel's calibrated values.
         * @param ADXL345C The three dimensional array of
         *                 the third channel's calibrated values.
         * @param ADXL345D The three dimensional array of
         *                 the fourth channel's calibrated values.
         */
        void setADXL345(
            const float ADXL345A[], const float ADXL345B[],
            const float ADXL345C[], const float ADXL345D[]);

    private:
        float ADXL345A[3];
        float ADXL345B[3];
        float ADXL345C[3];
        float ADXL345D[3];
    };

    /**
     * Returns a short q-Value encoding of a float.
     * The q-Value is the number of decimal digits.
     * The number is multiplied by 2^q-Value and stored as a short.
     *
     * @param floatValue The float value to convert.
     * @param qValue     The q-Value to use.
     * @return           The short value encoding.
     */
    short floatToShort(const float floatValue, const QValue qValue);

    /**
     * Returns a float q-Value decoding of a short.
     * The q-Value is the number of decimal digits.
     * The short is divided by 2^q-Value.
     *
     * @param shortValue The short value to convert.
     * @param qValue     The q-Value to use.
     * @return           The float value.
     */
    float shortToFloat(const short shortValue, const QValue qValue);
}

#endif // XIMUDATA_H
