// Abstract class for listening to packets received from the nu File IMU.
//
// (c) 2018: Marcel Admiraal

#ifndef NUFILELISTENER_H
#define NUFILELISTENER_H

#include "nulistener.h"

namespace nu
{
    class FileListener : public Listener
    {
    public:
        /**
         * Default destructor.
         */
        virtual ~FileListener() {}

        /**
         * Called by the API when a data packet is received from the IMU.
         * Uses the data type to call the appropriate data type received
         * function.
         *
         * @param data  Pointer to the data received.
         */
        void dataReceived(ma::Data* data);

        /**
         * Called when signal data received.
         *
         * @param data  Pointer to the register data received.
         */
        virtual void onSignalDataReceived(const ma::Data& data) {}
    };
}

#endif // NUFILELISTENER_H
